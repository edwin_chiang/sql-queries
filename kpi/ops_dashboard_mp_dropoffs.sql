with mp_dropoff AS (
  SELECT *
  FROM fpon_cdm.fpon_cdm_sales_line a
  JOIN fpon_cdm.calendar_full b 
  ON a.date_of_delivery = b.date
  WHERE is_marketplace = 1
  AND date_of_delivery >= '2020-01-01'
  AND order_status <> 'CANCELLED'
  -- AND ordered_qty > delivered_qty
--   AND order_item_status <> 'CANCELLED'
  ORDER BY date_of_delivery
),

items_with_tickets AS (
SELECT ticket_id, customer_id, order_ref_number, feedback_category_new, ticket_created_date,
       negligent_party, damage_cause, product_sku, product_name, 
FROM fpon_cdm.zendesk_sku_daily
WHERE is_marketplace = 1
)

SELECT *, AVG(delivered_qty_with_normalised_weighted_items) OVER(PARTITION BY month,year,week,product_sku) as total_delivered_qty
       , cast(concat(cast(year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week

FROM (
  SELECT a.*, ticket_id, feedback_category_new, ticket_created_date, negligent_party,damage_cause
  FROM mp_dropoff a
  LEFT JOIN items_with_tickets b
  ON a.order_ref_number = b.order_ref_number 
  AND a.product_sku = b.product_sku
)
# WHERE week <= extract(week from (DATE_ADD(CURRENT_DATE(),INTERVAL 3 DAY))) 
WHERE date_of_order < CURRENT_DATE()
