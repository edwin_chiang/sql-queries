  with tickets AS (
    SELECT *
    FROM (
    SELECT DISTINCT order_reference_number AS order_ref_number, ticket_id, ticket_status, feedback_category_new, ticket_created_date, snapshot_date
    , cal.natural_year as ticket_created_year
    , cal.month as ticket_created_month,
    ROW_NUMBER() OVER (PARTITION BY zd.ticket_id ORDER BY zd.snapshot_date DESC) AS rnk
    FROM fpon_cdm.zendesk_tickets_weekly_snapshot as zd
    left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
    -- WHERE ticket_id = '331965'
    where snapshot_date = (SELECT DISTINCT snapshot_date
                           FROM fpon_cdm.zendesk_tickets_weekly_snapshot 
                           WHERE EXTRACT(month FROM snapshot_date) = EXTRACT(month FROM DATE_SUB(current_date(), interval 1 week) )
                          #  WHERE EXTRACT(month FROM snapshot_date) = 1
                           ORDER BY snapshot_date DESC
                           LIMIT 1)
    AND lower(ticket_status) in ('solved', 'hold', 'open', 'pending', 'new')
    AND feedback_category_new IS NOT NULL
    AND lower(ticket_tags) NOT LIKE '%closed_by_merge%'
    AND brand_id = 114094936051 
    ORDER BY snapshot_date, ticket_id
    )
  WHERE ticket_created_year >= 2020
  AND ticket_created_month = EXTRACT(month FROM snapshot_date)
  AND rnk = 1
),

tickets_with_grouping as (
  SELECT store_code, store_name,a.*, grouping_new
  FROM tickets a
  LEFT JOIN fpon_ods.zendesk_feedback_category_grouping b
  ON a.feedback_category_new = b.feedback_category_new 
  LEFT JOIN fpon_cdm.fpon_cdm_sales_head c
  ON a.order_ref_number = c.order_ref_number
  ORDER BY snapshot_date,ticket_id
),

total_orders AS (
    SELECT
    wk.natural_year, wk.month
    , count(distinct sh.order_ref_number) as total_orders
    FROM fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    WHERE order_status not in ('CANCELLED')
    AND order_type in ('DELIVERY','B2B')
    AND wk.year >= 2020
    and order_ship_mode in ('PFC_DELIVERY','FFS')
    AND date_of_delivery <= (SELECT DISTINCT snapshot_date
                           FROM fpon_cdm.zendesk_tickets_weekly_snapshot 
                           WHERE EXTRACT(month FROM snapshot_date) = EXTRACT(month FROM DATE_SUB(current_date(), interval 1 week) )
                          #  WHERE EXTRACT(month FROM snapshot_date) = 1
                           ORDER BY snapshot_date DESC
                           LIMIT 1)
    GROUP BY wk.natural_year, wk.month
),

ticket_count AS (
  SELECT ticket_created_year AS year, ticket_created_month AS month, feedback_category_new,grouping_new, 
         CASE WHEN feedback_category_new IN ('order_issues__damaged_items','order_issues__late','order_issues__missing_items','order_issues__no_show','order_issues__wrong/additional_items',
         'changes_to_orders__delivery_address_amendment','fairprice_on__changes_to_orders__amendment_enquiry') THEN count(distinct order_ref_number) 
         ELSE count(distinct ticket_id) END AS ticket_count
  FROM tickets_with_grouping
  GROUP BY ticket_created_year, ticket_created_month,grouping_new, feedback_category_new
)

SELECT *
FROM (
  SELECT cast(concat(cast(a.year as string),case when a.month < 10 then '0' else '' end ,cast(a.month as string)) as int64) as year_month,a.year, a.month,feedback_category_new, grouping_new,
          total_orders, ticket_count, ticket_count / total_orders AS contact_ratio
          ,c.date AS start_of_month, d.date AS end_of_month
  FROM ticket_count a
  LEFT JOIN total_orders b 
  ON a.month = b.month AND a.year = b.natural_year
  JOIN (SELECT * FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY year, month ORDER BY day ASC) AS rnk FROM fpon_cdm.calendar_full) WHERE rnk = 1) c
  ON a.year = c.natural_year
  AND a.month = c.month 
  JOIN (SELECT * FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY year, month ORDER BY day DESC) AS rnk FROM fpon_cdm.calendar_full) WHERE rnk = 1) d
  ON a.year = d.natural_year
  AND a.month = d.month 
)
-- WHERE month = EXTRACT(month FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))