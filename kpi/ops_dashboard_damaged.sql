WITH damaged_items AS (
  SELECT cs_year, year, week, month,date_of_delivery,negligent_party, damage_cause, order_ship_mode, order_type, store_code ,dept_name, class_name, subclass_name, product_sku, product_name, count(*) AS no_of_products_damaged
  FROM (
  SELECT cs_year,year,week,month,date_of_delivery,ticket_id, ticket_created_date, negligent_party, damage_cause, pdt_type, store_code, order_ship_mode, order_type, product_sku,
         department_desc AS dept_name, class_desc AS class_name, subclass_desc AS subclass_name, a.product_name, is_marketplace, delivered_qty
  FROM fpon_cdm.zendesk_sku_daily a
  JOIN fpon_cdm.calendar_full b 
  ON a.date_of_delivery = b.date
  WHERE LOWER(feedback_category_new) LIKE '%damaged%'
  AND ticket_created_date >= '2020-01-01'
  ORDER BY ticket_id
  )
  WHERE product_name IS NOT NULL AND store_code IS NOT NULL
  GROUP BY product_name, dept_name, class_name, subclass_name, order_ship_mode, order_type, store_code,negligent_party, damage_cause, year, week,product_sku,month,date_of_delivery,cs_year
  ORDER BY no_of_products_damaged DESC
)

SELECT *
FROM (
  SELECT cs_year,year,week,month,date_of_delivery,store_code, product_sku,name  
        , cast(concat(cast(cs_year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week
        , cast(concat(cast(cs_year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64) as year_month,
         CASE 
          WHEN UPPER(name) LIKE '%DS%' THEN 'FFS Darkstore'
          WHEN UPPER(name) LIKE '%PFC%' THEN 'PFC'
          WHEN UPPER(name) LIKE '%FFS%' THEN 'FFS Normal'
         END AS fulfillment
        , order_type, dept_name, class_name, subclass_name, product_name, negligent_party, damage_cause, no_of_products_damaged
  FROM damaged_items a
  JOIN fpon_cdm.dim_fpon_store_meta b
  ON a.store_code = b.client_store_id
  AND name NOT IN ('VivoCity','PUNGGOL DRIVE OASIS','FairPrice Xtra JEM')
)
-- WHERE damage_cause LIKE '%,%'
# WHERE week <= extract(week from (DATE_ADD(CURRENT_DATE(),INTERVAL 3 DAY))) 
WHERE date_of_delivery <= CURRENT_DATE()
ORDER BY year, week, no_of_products_damaged DESC
