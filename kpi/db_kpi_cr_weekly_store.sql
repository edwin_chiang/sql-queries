  with tickets AS (
    SELECT *
    FROM (
    SELECT DISTINCT order_reference_number AS order_ref_number, ticket_id, ticket_status, feedback_category_new, ticket_created_date, snapshot_date
    , cal.year as ticket_created_year
    , cal.week as ticket_created_week,
    ROW_NUMBER() OVER (PARTITION BY zd.ticket_id ORDER BY zd.snapshot_date DESC) AS rnk
    FROM fpon_cdm.zendesk_tickets_weekly_snapshot as zd
    left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
    -- WHERE ticket_id = '331965'
    WHERE 1=1
    AND snapshot_date = DATE_SUB(CURRENT_DATE,INTERVAL 2 DAY)
--     AND snapshot_date = '2021-01-06'
    AND lower(ticket_status) in ('solved', 'hold', 'open', 'pending', 'new')
    AND feedback_category_new IS NOT NULL
    AND brand_id = 114094936051 
    ORDER BY snapshot_date, ticket_id
    )
  WHERE ticket_created_year = EXTRACT(year FROM DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY))
  AND ticket_created_week = (SELECT week FROM fpon_cdm.calendar_full WHERE date = DATE_SUB(CURRENT_DATE, INTERVAL 2 DAY))
  AND rnk = 1
),

tickets_with_grouping as (
  SELECT store_code, store_name,a.*, grouping_new
  FROM tickets a
  LEFT JOIN fpon_ods.zendesk_feedback_category_grouping b
  ON a.feedback_category_new = b.feedback_category_new 
  LEFT JOIN fpon_cdm.fpon_cdm_sales_head c
  ON a.order_ref_number = c.order_ref_number
  ORDER BY snapshot_date,ticket_id
),

total_orders AS (
    SELECT
    wk.year, wk.week, store_code, store_name
    , count(distinct sh.order_ref_number) as total_orders
    FROM fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    WHERE order_status not in ('CANCELLED')
    AND order_type in ('DELIVERY','B2B')
    AND wk.year >= 2020
    and order_ship_mode in ('PFC_DELIVERY','FFS')
    GROUP BY wk.year, wk.week, store_code, store_name
),

ticket_count AS (
  SELECT ticket_created_year AS year, ticket_created_week AS week, store_code, store_name, feedback_category_new,grouping_new, 
         CASE WHEN feedback_category_new IN ('order_issues__damaged_items','order_issues__late','order_issues__missing_items','order_issues__no_show','order_issues__wrong/additional_items',
         'changes_to_orders__delivery_address_amendment','fairprice_on__changes_to_orders__amendment_enquiry') THEN count(distinct order_ref_number) 
         ELSE count(distinct ticket_id) END AS ticket_count
  FROM tickets_with_grouping
  GROUP BY ticket_created_year, ticket_created_week,grouping_new, feedback_category_new, store_code, store_name
)

SELECT *
FROM (
  SELECT cast(concat(cast(a.year as string),case when a.week < 10 then '0' else '' end ,cast(a.week as string)) as int64) as year_week,a.year,a.week, a.store_code, UPPER(a.store_name) AS store_name,feedback_category_new, grouping_new,ticket_count, total_orders, ticket_count / total_orders AS contact_ratio,
  c.date AS start_of_week, d.date AS end_of_week, e.date AS start_of_week_monday
  FROM ticket_count a
  LEFT JOIN total_orders b 
  ON a.week = b.week AND a.year = b.year AND a.store_code = b.store_code AND a.store_name = b.store_name
  JOIN (SELECT * FROM fpon_cdm.calendar_full WHERE weekday_num = 3) c
  ON a.year = c.year
  AND a.week = c.week 
  JOIN (SELECT * FROM fpon_cdm.calendar_full WHERE weekday_num = 2) d
  ON a.year = d.year
  AND a.week = d.week 
  JOIN (SELECT * FROM fpon_cdm.calendar_full WHERE weekday_num = 0) e
  ON a.year = e.year
  AND a.week = e.week 
  ORDER BY year DESC,week DESC
)
WHERE store_name IS NOT NULL