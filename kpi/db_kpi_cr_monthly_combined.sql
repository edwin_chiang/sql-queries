CREATE OR REPLACE TABLE
  fpon_cdm.db_kpi_cr_monthly_combined AS
(
with tbl AS (
    SELECT CONCAT('Actuals',year,month,'TOTAL_ONLINE') AS col_id, year_month,year, month, feedback_category_new, 'TOTAL_ONLINE' AS group_name,grouping_new,total_orders, ticket_count, contact_ratio, start_of_month, end_of_month	
    FROM `fairprice-bigquery.fpon_cdm.db_kpi_cr_monthly`
    UNION ALL 
       SELECT col_id, year_month, year, month, feedback_category_new, group_name, grouping_new, total_group_orders AS total_orders, SUM(ticket_count) AS ticket_count, SUM(ticket_count) / total_group_orders AS contact_ratio, start_of_month, end_of_month
       FROM (
       SELECT CONCAT('Actuals',year,month,group_name) AS col_id, *
       FROM (
              SELECT a.year_month, year, month, feedback_category_new, CASE WHEN a.fulfillment = 'PFC_DELIVERY' THEN 'PFC' WHEN a.fulfillment IN ('FFS Normal','FFS Darkstore') THEN 'FFS' ELSE a.fulfillment END AS group_name,
              grouping_new, total_group_orders,
              ticket_count, contact_ratio, start_of_month, end_of_month	
              FROM `fairprice-bigquery.fpon_cdm.db_kpi_cr_monthly_fulfillment` a
              LEFT JOIN (SELECT *, SUM(total_orders) OVER (PARTITION BY year_month,group_name) AS total_group_orders
              FROM (
                     SELECT DISTINCT year_month, 
                     fulfillment, 
                     CASE WHEN fulfillment = 'PFC_DELIVERY' THEN 'PFC' WHEN fulfillment IN ('FFS Normal','FFS Darkstore') THEN 'FFS' ELSE fulfillment END AS group_name,
                     total_orders
                     FROM `fairprice-bigquery.fpon_cdm.db_kpi_cr_monthly_fulfillment`
                     ORDER BY year_month
              ) 
              ) b
              ON a.year_month = b.year_month AND a.fulfillment = b.fulfillment 
       )
       )
       GROUP BY col_id, year_month,year, month, feedback_category_new, group_name, grouping_new, total_group_orders, start_of_month, end_of_month
    UNION ALL 
    SELECT CONCAT('Actuals',year,month,store_code) AS col_id,year_month, year, month, feedback_category_new, store_code AS group_name,grouping_new,total_orders, ticket_count, contact_ratio, start_of_month, end_of_month	
    FROM `fairprice-bigquery.fpon_cdm.db_kpi_cr_monthly_store`
)


SELECT *, 
       wrongadd_tickets + damaged_tickets + late_tickets + missing_tickets + noshow_tickets + driver_attitude_tickets AS customer_tickets,
       wrongadd_cr + damaged_cr + late_cr + missing_cr + noshow_cr + driver_attitude_cr AS customer_cr,
       ticket_count - (wrongadd_tickets + damaged_tickets + late_tickets + missing_tickets + noshow_tickets + driver_attitude_tickets) AS other_tickets,
       contact_ratio - (wrongadd_cr + damaged_cr + late_cr + missing_cr + noshow_cr + driver_attitude_cr) AS other_cr,
FROM (SELECT col_id, year, month, group_name, MAX(total_orders) AS orders_delivered, SUM(ticket_count) AS ticket_count, SUM(contact_ratio) AS contact_ratio, 
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__wrong/additional_items' THEN ticket_count END),0) AS wrongadd_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__wrong/additional_items' THEN contact_ratio END),0) AS wrongadd_cr,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__damaged_items' THEN ticket_count END),0) AS damaged_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__damaged_items' THEN contact_ratio END),0) AS damaged_cr,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__late' THEN ticket_count END),0) AS late_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__late' THEN contact_ratio END),0) AS late_cr,  
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__missing_items' THEN ticket_count END),0) AS missing_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__missing_items' THEN contact_ratio END),0) AS missing_cr, 
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__no_show' THEN ticket_count END),0) AS noshow_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'order_issues__no_show' THEN contact_ratio END),0) AS noshow_cr,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'feedback/suggestion__driver_attitude' THEN ticket_count END),0) AS driver_attitude_tickets,
       COALESCE(MAX(CASE WHEN feedback_category_new = 'feedback/suggestion__driver_attitude' THEN contact_ratio END),0) AS driver_attitude_cr       
FROM tbl 
-- WHERE group_name = '355'
-- AND year = 2021
-- AND month = 4
GROUP BY col_id, year, month, group_name)
)
ORDER BY year ASC, month ASC, group_name