create temp table run_date
(
current_date date,
year int64,
week int64
)
;
INSERT INTO run_date
select current_date,
case when wk.week=1 then wk.year-1 else wk.year end as year,
case when wk.week=1 then 52 else wk.week-1 end as week --- change to 53 if leap year
from fpon_cdm.calendar as wk -- to obtain FP weeks
where current_date = wk.date
;

INSERT INTO fpon_cdm.weeklykpi_business
select
concat('Actuals',cast(year as STRING), cast(week as STRING),'TOTAL_ONLINE') as col_id
, 'Actuals' as actuals_budget
 , year
  ,week
  ,'TOTAL_ONLINE' as group_name
  ,overall_total_sales as total_sales
  ,overall_total_orders as total_orders
  ,overall_daily_orders as daily_orders
  ,overall_aov as aov
  ,overall_units as units
  ,null as asp
  ,overall_nps as nps
  ,null as skucount
  ,overall_fresh_pen_order as fresh_pen_order
  ,overall_fresh_pen_qty as fresh_pen_qty
  ,overall_fresh_pen_sales as fresh_pen_sales
  ,overall_superperf as perc_superperf_orders
  ,100-overall_amendments as amendments
  ,cast((pfc_slots_availability+ffs_slots_availability) as INT64) as slots_avail
  ,null as perc_late_picked
  , overall_perc_late_delivered as perc_late_delivered_tms
  , overall_perc_ontime_delivered as perc_ontime_delivered_tms
  , null as sellers
  , overall_extra_fees as extra_fees
  , overall_daily_delivery as daily_delivery
  , null as slots_utilization
  , overall_perfect as perc_perfect_orders
  , null as mp_gmv_penetration
  , null as mp_order_penetration
  , null as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

union all

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'PFC') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'PFC' as group_name
  ,pfc_total_sales as total_sales
  ,pfc_total_orders as total_orders
  ,pfc_daily_orders as daily_orders
  ,pfc_aov as aov
  ,pfc_units as units
  ,null as asp
  ,pfc_nps as nps
  ,pfc_skucount as skucount
  ,pfc_fresh_pen_order as fresh_pen_order 
  ,pfc_fresh_pen_qty as fresh_pen_qty
  ,pfc_fresh_pen_sales as fresh_pen_sales 
  ,pfc_superperf as perc_superperf_orders
  ,100-pfc_amendments as amendments
  ,cast(pfc_slots_availability as INT64) as slots_avail
  ,null as perc_late_picked
  ,pfc_perc_late_delivered as perc_late_delivered_tms
  ,pfc_perc_ontime_delivered as perc_ontime_delivered_tms
  ,null as sellers 
  , pfc_extra_fees as extra_fees
  , pfc_daily_delivery as daily_delivery
  , pfc_daily_delivery * 100.00 / pfc_slots_availability as slots_utilization
  , pfc_perfect as perc_perfect_orders
  , null as mp_gmv_penetration
  , null as mp_order_penetration
  , null as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

UNION ALL

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'B2B') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'B2B' as group_name
  ,b2b_total_sales as total_sales
  ,b2b_total_orders as total_orders
  ,b2b_daily_orders as daily_orders
  ,b2b_aov as aov
  ,b2b_units as units
  ,null as asp
  ,b2b_nps as nps
  ,null as skucount
  ,b2b_fresh_pen_order as fresh_pen_order 
  ,b2b_fresh_pen_qty as fresh_pen_qty
  ,b2b_fresh_pen_sales as fresh_pen_sales 
  ,null as perc_superperf_orders
  ,null as amendments
  ,null as slots_avail
  ,null as perc_late_picked
  ,null as perc_late_delivered_tms
  ,null as perc_ontime_delivered_tms
  ,null as sellers 
  ,null as extra_fees
  , null as daily_delivery
  , null as slots_utilization
  , null as perc_perfect_orders
  , null as mp_gmv_penetration
  , null as mp_order_penetration
  , null as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

union all

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'FFS') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'FFS' as group_name
  ,ffs_total_sales as total_sales
  ,ffs_total_orders as total_orders
  ,ffs_daily_orders as daily_orders
  ,ffs_aov as aov
  ,ffs_units as units
  ,null as asp
  ,ffs_nps as nps
  ,null as skucount
  ,ffs_fresh_pen_order as fresh_pen_order
  ,ffs_fresh_pen_qty as fresh_pen_qty
  ,ffs_fresh_pen_sales as fresh_pen_sales 
  ,ffs_superperf as perc_superperf_orders
  ,100-ffs_amendments as amendments
  ,cast(ffs_slots_availability as INT64) as slots_avail
  ,ffs_perc_picked_late as perc_late_picked
  ,ffs_perc_late_delivered as perc_late_delivered_tms
  ,ffs_perc_ontime_delivered as perc_ontime_delivered_tms
  ,null as sellers 
  ,ffs_extra_fees as extra_fees
  , ffs_daily_delivery as daily_delivery
  , ffs_daily_delivery * 100.00 / ffs_slots_availability as slots_utilization
  , ffs_perfect as perc_perfect_orders
  , null as mp_gmv_penetration
  , null as mp_order_penetration
  , null as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

union all

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'MARKETPLACE') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'MARKETPLACE' as group_name
  ,overall_marketplace_total_sales as total_sales
  ,overall_marketplace_total_orders as total_orders
  ,overall_marketplace_daily_orders as daily_orders
  ,overall_marketplace_aov as aov
  ,overall_marketplace_units as units
  ,overall_marketplace_asp as asp
  ,null as nps
  ,overall_marketplace_skucount as skucount
  ,null as fresh_pen_order 
  ,null as fresh_pen_qty
  ,null as fresh_pen_sales 
  ,null as perc_superperf_orders
  ,null as amendments
  ,null as slots_avail
  ,null as perc_late_picked
  ,null as perc_late_delivered_tms
  ,null as perc_ontime_delivered_tms
  , overall_marketplace_sellers as sellers
  , null as extra_fees
  , null as daily_delivery
  , null as slots_utilization
  , null as perc_perfect_orders
  , overall_mp_gmv_penetration as mp_gmv_penetration
  , overall_mp_order_penetration as mp_order_penetration
  , overall_marketplace_instock_perc as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

UNION ALL

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'PFC_MARKETPLACE') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'PFC_MARKETPLACE' as group_name
  ,pfc_marketplace_total_sales as total_sales
  ,pfc_marketplace_total_orders as total_orders
  ,pfc_marketplace_daily_orders as daily_orders
  ,pfc_marketplace_aov as aov
  ,pfc_marketplace_units as units
  ,pfc_marketplace_asp as asp
  ,null as nps
  ,pfc_marketplace_skucount as skucount
  ,null as fresh_pen_order 
  ,null as fresh_pen_qty
  ,null as fresh_pen_sales 
  ,null as perc_superperf_orders
  ,null as amendments
  ,null as slots_avail
  ,null as perc_late_picked
  ,null as perc_late_delivered_tms
  ,null as perc_ontime_delivered_tms
  , pfc_marketplace_sellers as sellers
  , null as extra_fees
  , null as daily_delivery
  , null as slots_utilization
  , null as perc_perfect_orders
  , pfc_mp_gmv_penetration as mp_gmv_penetration
  , pfc_mp_order_penetration as mp_order_penetration
  , pfc_marketplace_instock_perc as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

UNION ALL

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'FFS_MARKETPLACE') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'FFS_MARKETPLACE' as group_name
  ,ffs_marketplace_total_sales as total_sales
  ,ffs_marketplace_total_orders as total_orders
  ,ffs_marketplace_daily_orders as daily_orders
  ,ffs_marketplace_aov as aov
  ,ffs_marketplace_units as units
  ,ffs_marketplace_asp as asp
  ,null as nps
  ,ffs_marketplace_skucount as skucount
  ,null as fresh_pen_order 
  ,null as fresh_pen_qty
  ,null as fresh_pen_sales 
  ,null as perc_superperf_orders
  ,null as amendments
  ,null as slots_avail
  ,null as perc_late_picked
  ,null as perc_late_delivered_tms
  ,null as perc_ontime_delivered_tms
  , ffs_marketplace_sellers as sellers
  , null as extra_fees
  , null as daily_delivery
  , null as slots_utilization
  , null as perc_perfect_orders
  , ffs_mp_gmv_penetration as mp_gmv_penetration
  , ffs_mp_order_penetration as mp_order_penetration
  , ffs_marketplace_instock_perc as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)

UNION ALL

select
concat('Actuals',cast(year as STRING), cast(week as STRING),'DS_MARKETPLACE') as col_id
, 'Actuals' as actuals_budget
, year
  ,week
  ,'DS_MARKETPLACE' as group_name
  ,ds_marketplace_total_sales as total_sales
  ,ds_marketplace_total_orders as total_orders
  ,ds_marketplace_daily_orders as daily_orders
  ,ds_marketplace_aov as aov
  ,ds_marketplace_units as units
  ,ds_marketplace_asp as asp
  ,null as nps
  ,ds_marketplace_skucount as skucount
  ,null as fresh_pen_order 
  ,null as fresh_pen_qty
  ,null as fresh_pen_sales 
  ,null as perc_superperf_orders
  ,null as amendments
  ,null as slots_avail
  ,null as perc_late_picked
  ,null as perc_late_delivered_tms
  ,null as perc_ontime_delivered_tms
  , ds_marketplace_sellers as sellers
  , null as extra_fees
  , null as daily_delivery
  , null as slots_utilization
  , null as perc_perfect_orders
  , ds_mp_gmv_penetration as mp_gmv_penetration
  , ds_mp_order_penetration as mp_order_penetration
  , ds_marketplace_instock_perc as mp_instock_perc
from fpon_cdm.weeklykpi_raw_snapshot as kpi
where year in (select year from run_date) and week in (select week from run_date)
;
drop table run_date;