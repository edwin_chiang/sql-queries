-- updated marketplace sellers with product_brand_group
-- updated addmissingwrong, damaged CR with count order instead of count ticket
-- removed all CS metrics (reading from fpon_cdm.zendesk_tickets_features_snapshot)
-- update amendments ordered_qty and delivered_qty fields
-- updated with new nps query 20201023
-- changed dim_ffs_logistics_driver_trips to ffs_logistics_driver_trips_opm 20210412
-- 20210507 change: For fresh calculations, changed delivered_qty to delivered_qty_with_normalised_weighted_items
-- 20210601 change: Included mktplace impressions
-- 20210618 switched ffs_slotcapacity to ffs_slotcapacity_bq
-- declare dummy string ;

--weeklykpi_raw_snapshot_FRI0835
CREATE OR REPLACE TABLE fpon_cdm.weeklykpi_raw_snapshot AS (
with nps AS (
  select * from fpon_cdm.dim_fpon_cx_nps_detail
),
------------------------------------------------
------------- TOTAL SALES ----------------------
------------------------------------------------
total_sales as
(
select
wk.year, wk.week,
sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response

from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join -- to obtain gp
  (select order_id, sum(gp_cost) as gp_cost
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
  */
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
  (
   select
  cal.year
  ,cal.week
  ,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count(order_ref_number ) as nps_score
from
(
  SELECT * FROM nps
) as t3
 left join fpon_cdm.calendar_full as cal on cal.date = date(t3.end_date)
group by cal.year
  ,cal.week
  ) as nps on nps.year = wk.year and nps.week = wk.week
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
group by wk.year, wk.week, nps.nps_score
)
,
overall_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY','FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('B2B','DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
overall_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select overall_delivery.year, overall_delivery.week
, perf.count * 100.0 / overall_delivery.total_orders as perc_amendments
from
overall_delivery
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('PFC_DELIVERY', 'FFS')
    and order_type in ('B2B', 'DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff =1
    group by p1.year, p1.week
    ) as perf on perf.year = overall_delivery.year and perf.week = overall_delivery.week
 )
,
------------------------------------------------
------------ PFC SALES (B2C+B2B) ---------------
------------------------------------------------
pfc_sales as
(
select
wk.year
, wk.week
,sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
-- , sum(sl.gp_cost) as total_cost
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
, (
  select count(distinct sku)
  from
  (
  select *,case when sku like '9_____%' then 1 else 0 end as is_marketplace
  from fpon_cdm.dim_fpon_store_product_df
  where product_status_store<>'HIDDEN' and product_status_global in ('ENABLED')
  and store_code='004'
  ) as t
  where is_marketplace=0 -- exclude marketplace
  and refresh_date = CURRENT_DATE()
  group by refresh_date
  )
  as skucount
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --, orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
  */
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  cal.year
  ,cal.week
--   ,new_delivery_type
,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count( order_ref_number ) as nps_score
from
(
select * from nps where business_type IN ('PFC','B2B')
) as t3
join fpon_cdm.calendar_full as cal
on date(t3.end_date) = cal.date
-- where new_delivery_type = 'ORD'

group by year
  ,week
--   ,new_delivery_type
  ) as nps on nps.year = wk.year and nps.week = wk.week

where
  sh.order_status not in ('CANCELLED')
  and order_ship_mode in ('PFC_DELIVERY')
  and wk.year >= 2020
group by
wk.year, wk.week, nps.nps_score
)
,
pfc_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY','B2B')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
pfc_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select total.year, total.week
, perf.count * 100.0 / total.total_orders as perc_amendments
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY','B2B')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where
    order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
  and order_type in ('DELIVERY','B2B')
   and wk.year >= 2020
    ) as p1
  where no_dropoff =1
  group by p1.year, p1.week
  ) as perf on perf.year = total.year and perf.week = total.week
)
,
-- pfc_slots_availability
pfc_slots_availability as
(
select
year, week
, round(sum(capacity)/7,0) as Allocated
from fpon_cdm.dim_pfc_operation_slot_capacity_df as pfc_slots
LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = pfc_slots.ds
group by wk.year, wk.week
)
,
------------------------------------------------
------------ PFC SALES (B2B) -------------------
------------------------------------------------
b2b_sales as
(
select
wk.year
, wk.week
,sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --, orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
 */
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  wk.year
  ,wk.week
--   ,new_delivery_type
,safe_divide((count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0,count( order_ref_number )) as nps_score
from
(
  select * from nps where business_type = 'B2B'
) as t3
JOIN fpon_cdm.calendar_full as wk on wk.date = date(t3.end_date)
-- where new_delivery_type = 'B2B'
where business_type = 'B2B'
group by year
  ,week
--   ,new_delivery_type
  ) as nps on nps.year = wk.year and nps.week = wk.week
where
  sh.order_status not in ('CANCELLED')
  and order_type in ('B2B')
  and wk.year >= 2020
group by
wk.year, wk.week, nps.nps_score
)
,
------------------------------------------------
------------- FFS SALES - 1 --------------------
------------------------------------------------
ffs_sales as
(
select
wk.year
, wk.week
,sum((sh.order_amount - sh.order_discount)/1.07) as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders -- without cancel
-- for calculation of fresh pen
, sum(sh.order_amount) as order_amount
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount/1.07)) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --,orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id
  ) as sl
  on sh.order_id = sl.order_id
  */
LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  cal.year
  ,cal.week
--   ,new_delivery_type
  ,count(case when nps_response_category in ('promoters') then order_ref_number end) as promoter_cnt
  ,count(case when nps_response_category in ('detractors') then order_ref_number end) as detractor_cnt
  ,count(distinct respondent_id ) as total_cnt
,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count( order_ref_number ) as nps_score
from
(
 select * from nps where business_type IN ('FFS Darkstore','FFS Normal')
) as t3
join fpon_cdm.calendar_full as cal
on date(t3.end_date)= cal.date
-- where new_delivery_type = 'FFS'
group by year
  ,week
--   ,new_delivery_type
) as nps on nps.year = wk.year and nps.week = wk.week
where
sh.order_status not in ('CANCELLED')
and order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
and wk.year >= 2020
group by
wk.year
, wk.week
, nps.nps_score
)
,
ffs_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
)
,
------------------------------------------------
------------- FFS SALES - 2 --------------------
------------------------------------------------
/*
perc amendments - no dropoff
daily orders w cancellation
*/
ffs_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select total.year, total.week
, perf.count * 100.0 / total.total_orders as perc_amendments
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where
    order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
--     group by wk.year, wk.week, order_id ,ordered_qty ,delivered_qty
    ) as p1
  where no_dropoff =1
  group by p1.year, p1.week
  ) as perf on perf.year = total.year and perf.week = total.week
)
,
-- ffs slots
ffs_slots_availability as
(
select
year, week
, round(sum(cast(allocated as int64))/7,0) as Allocated
from fpon_analytics.ffs_slotcapacity_bq as ffs_slots
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = cast(ffs_slots.date as date)
    where year = 2020
      group by wk.year, wk.week
      order by week
)
,
ffs_perc_orders_picked_late as
(
-- % of orders picked late FOR FFS
-- Use calculation based on packing late formula from picker report
select
year, week, (Late_Packed / Total_Orders *100) as Perc_Late_Picked
FROM  (
  SELECT wk.year, wk.week,
      COUNT(DISTINCT ORD.id) Total_Orders
      ,SUM(CASE WHEN DATETIME_diff(DATETIME(OPM.end_time,'UTC'), DATETIME(ORD.preferred_date,PARSE_TIME("%T",ORD.slot_Start_time)), SECOND)>1 THEN 1 ELSE 0 END) as Late_Packed
    FROM fpon_ods.zs_order_service_orders ORD

    LEFT JOIN fpon_ods.zs_order_service_order_types ORDTYP ON ORD.type_id = ORDTYP.id
    LEFT JOIN fpon_ods.zs_order_service_order_process_mapping OPM ON ORD.id = OPM.order_id
    LEFT JOIN fpon_cdm.calendar_full as wk ON DATE(OPM.Start_Time)=wk.date
    WHERE ORD.store_id IN
            (SELECT STR.id FROM fpon_ods.zs_organization_service_stores STR
             WHERE STR.Deleted_At is null AND STR.id not in (165)
                AND STR.has_delivery_hub = 1 AND STR.STATUS = 'ENABLED' ) -- FFS Stores only
            AND ORD.Type_ID = 2 -- HD orders only
            AND OPM.process_name =  'PACKING' AND OPM.status = 'SUCCESS'
            AND wk.year >= 2020
    GROUP BY wk.year, wk.week
   ))
,
ffs_perc_orders_delivery as
(-- % of orders out late / on time for delivery FOR FFS
  select b.year,b.week
  ,count(case when new_delivery_status in ('On Time') then 1 end) *100.0/count(*) as ontime_perc
  ,count(case when new_delivery_status in ('Late') then 1 end) *100.0/count(*) as late_perc
  ,count(case when new_delivery_status in ('Early') then 1 end)*100.0/count(*) as early_perc
  from fpon_cdm.ffs_logistics_driver_trips_opm as driver
  join fpon_cdm.calendar_full as b on driver.date_of_delivery = b.date
  where driver.order_status in ('COMPLETED') and trip_status in ('SUCCESS')
  group by b.year,b.week
  order by b.year,b.week
)
,
------------------------------------------------
------------- FRESH ----------------------------
------------------------------------------------
ffs_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , sl.order_id
  , product_sku
  , sl.delivered_qty_with_normalised_weighted_items as delivered_qty
  , sl.order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
  --left join fpon_cdm.fpon_cdm_sales_head sh on sl.order_id = sh.order_id
  LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  sl.order_ship_mode in ('FFS')
  and sl.order_status not in ('CANCELLED')
  and sl.order_type in ('DELIVERY')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
ffs_fresh_sales as
(
select
ffs_sales.year
, ffs_sales.week
, fresh.total_orders*100.0/ ffs_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ ffs_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ ffs_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / ffs_sales.total_orders as fresh_aov
from ffs_sales
left join ffs_freshtable as fresh
on fresh.week = ffs_sales.week and fresh.year = ffs_sales.year
)
,
pfc_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items as delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('PFC_DELIVERY')
  and order_type in ('DELIVERY','B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
pfc_fresh_sales as
(
select
pfc_sales.year
, pfc_sales.week
, fresh.total_orders*100.0/ pfc_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ pfc_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ pfc_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / pfc_sales.total_orders as fresh_aov
from pfc_sales
left join pfc_freshtable as fresh
on fresh.week = pfc_sales.week and fresh.year = pfc_sales.year
)
,
overall_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items AS delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('FFS','PFC_DELIVERY')
  and order_type in ('DELIVERY','B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
overall_fresh_sales as
(
select
total_sales.year
, total_sales.week
, fresh.total_orders*100.0/ total_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ total_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ total_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / total_sales.total_orders as fresh_aov
from total_sales as total_sales
left join overall_freshtable as fresh
on fresh.week = total_sales.week and fresh.year = total_sales.year
)
,
b2b_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items AS delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('PFC_DELIVERY')
  and order_type in ('B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
            -- and wk.week >= 40
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
b2b_fresh_sales as
(
select
b2b_sales.year
, b2b_sales.week
, fresh.total_orders*100.0/ b2b_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ b2b_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ b2b_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / b2b_sales.total_orders as fresh_aov
from b2b_sales
left join b2b_freshtable as fresh
on fresh.week = b2b_sales.week and fresh.year = b2b_sales.year
)
,

/* PFC PERC ONTIME */
-- % of orders out late / on time for delivery FOR PFC
order_deliver_history as
(
select orderid
,wk.year
,wk.week
, delivery_date
, delivery_time
, delivery_window
, DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)) as delivery_datetime
, TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC') as delivery_datetime2
, TIMESTAMP_ADD(TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC'), interval 15 minute) as calculated_15mins_ear
, DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])) as delivery_datewindow_start
, DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))) as delivery_datewindow_end
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])),'UTC') as delivery_datewindow_startx
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))),'UTC') as delivery_datewindow_endx
, distance_metres
, latitude
, longitude
, status
from
# inventory_ods.fp_gls_tms_order_delivery_history
(SELECT * FROM `cds-sc-data-cloud-prod.fpg_sc_ods.fp_gls_tms_order_delivery_history` where delivery_time is not null AND REGEXP_CONTAINS(delivery_time,'[a-zA-Z0-9]') = TRUE)
 as tms
left join fpon_cdm.fpon_cdm_sales_head as sh on sh.order_ref_number = tms.orderid
LEFT JOIN fpon_cdm.calendar_full as wk -- to obtain FP weeks
  ON (tms.delivery_date) = wk.date
--sh.order_status in ('COMPLETED','DISPATCHED')
group by orderid
, year, week
, order_ship_mode, order_type
, tms.delivery_time, tms.delivery_date, tms.delivery_window, distance_metres, latitude, longitude, status
order by delivery_date desc
)
,
filter1 as
(
select
year
, week
, orderid
, distance_metres
, latitude
, longitude
, delivery_date
, delivery_datetime2
,TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)
, delivery_datewindow_startx
, delivery_datewindow_endx
,status
,case when upper(status) = 'ONTIME' and delivery_datewindow_startx >= TIMESTAMP_ADD(delivery_datetime2, INTERVAL 15 MINUTE) or (distance_metres > 500) then 'LATE'
when upper(status) = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE) then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE_EARLY
,case when status = 'ONTIME' and (distance_metres > 500) then 'LATE'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)  then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE
from order_deliver_history
)
,
filter2 as
(
select
year
, week
,count(*) as total_orders
,sum(case when LATE_EARLY = 'LATE' then 1 else 0 end) as LATE_EARLY_COUNT
,sum(case when LATE = 'LATE' then 1 else 0 end) as LATE_COUNT
from filter1
group by year, week
)
,
pfc_perc_orders_delivery as
(
select
year
, week
,total_orders
,LATE_COUNT * 100.0 / total_orders as Perc_Late_Delivered
, 100.00 - (LATE_COUNT * 100.0 / total_orders) as Perc_OnTime_Delivered
,LATE_EARLY_COUNT * 100.0 /total_orders as late_perc_applying_tooearly
from filter2
order by week
)
,
----- SUPER PERFECT DEFINITION
super_perfect_def as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty_with_normalised_weighted_items - sh.delivered_qty_with_normalised_weighted_items as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when pfc_ontime.LATE in ('ONTIME') THEN 1
  when pfc_ontime.LATE_EARLY IN ('ONTIME') then 1
  when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join fpon_cdm.zendesk_full_table_snapshot as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join filter1 as pfc_ontime on pfc_ontime.orderid = sh.order_ref_number
left join fpon_cdm.ffs_logistics_driver_trips_opm as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
--select * from perfect_def

,

---PERFECT DEFINITION
perfect_def as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty_with_normalised_weighted_items - sh.delivered_qty_with_normalised_weighted_items as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when pfc_ontime.LATE in ('ONTIME') THEN 1
  when pfc_ontime.LATE_EARLY IN ('ONTIME') then 1
  when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join (SELECT * FROM fpon_cdm.zendesk_full_table_snapshot WHERE feedback_category_new IN ('order_issues__damaged_items','feedback/suggestion__driver_attitude','order_issues__late','order_issues__no_show','order_issues__missing_items','order_issues__wrong/additional_items')) as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join filter1 as pfc_ontime on pfc_ontime.orderid = sh.order_ref_number
left join fpon_cdm.ffs_logistics_driver_trips_opm as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
--select * from perfect_def
,
overall_perf as
(
select
perfect_def.year, perfect_def.week
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ overall_delivery.total_orders as perfect
from perfect_def
left join overall_delivery
on overall_delivery.year = perfect_def.year and overall_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY','FFS')
and order_type in ('B2B','DELIVERY')
group by perfect_def.year, perfect_def.week, overall_delivery.total_orders
order by perfect_def.week asc
)
,
pfc_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ pfc_delivery.total_orders as perfect
from perfect_def
left join pfc_delivery
on pfc_delivery.year = perfect_def.year and pfc_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY')
and order_type in ('DELIVERY','B2B')
group by perfect_def.year, perfect_def.week, pfc_delivery.total_orders
,order_ship_mode
order by perfect_def.week asc
)
,
ffs_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ ffs_delivery.total_orders as perfect
from perfect_def
left join ffs_delivery
on ffs_delivery.year = perfect_def.year and ffs_delivery.week = perfect_def.week
where order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
group by perfect_def.year, perfect_def.week, order_ship_mode, ffs_delivery.total_orders
order by perfect_def.week asc
)
,
------------------------------------------------
----------- MARKETPLACE IMPRESSIONS-------------
------------------------------------------------
overall_marketplace_impressions as (
  select year
      , week
      , SUM(ttl_imp) AS total_imp
      , SUM(instock_imp) AS instock_imp
      , case when SUM(ttl_imp) = 0 then 0 else SUM(instock_imp) / SUM(ttl_imp) END AS instock_perc
  from fpon_analytics.fpon_b2c_prod_imp_by_cat
  where sku like '9_____%'
  and store_type IN ('FFS','DS','PFC')
  GROUP BY year, week
  ORDER BY year, week
)
,

pfc_marketplace_impressions as (
  select year
      , week
      , SUM(ttl_imp) AS total_imp
      , SUM(instock_imp) AS instock_imp
      , case when SUM(ttl_imp) = 0 then 0 else SUM(instock_imp) / SUM(ttl_imp) END AS instock_perc
  from fpon_analytics.fpon_b2c_prod_imp_by_cat
  where sku like '9_____%'
  and store_type = 'PFC'
  GROUP BY year, week
  ORDER BY year, week
)
,

ffs_marketplace_impressions as (
  select year
      , week
      , SUM(ttl_imp) AS total_imp
      , SUM(instock_imp) AS instock_imp
      , case when SUM(ttl_imp) = 0 then 0 else SUM(instock_imp) / SUM(ttl_imp) END AS instock_perc
  from fpon_analytics.fpon_b2c_prod_imp_by_cat
  where sku like '9_____%'
  and store_type = 'FFS'
  GROUP BY year, week
  ORDER BY year, week
)
,

ds_marketplace_impressions as (
  select year
      , week
      , SUM(ttl_imp) AS total_imp
      , SUM(instock_imp) AS instock_imp
      , case when SUM(ttl_imp) = 0 then 0 else SUM(instock_imp) / SUM(ttl_imp) END AS instock_perc
  from fpon_analytics.fpon_b2c_prod_imp_by_cat
  where sku like '9_____%'
  and store_type = 'DS'
  GROUP BY year, week
  ORDER BY year, week
)


------------------------------------------------
------------ CREATE RAW TABLE ------------------
------------------------------------------------
/* INSERT into fpon_cdm.weeklykpi_raw*/
select
total_sales.year
, total_sales.week

/* OVERALL */
, total_sales.total_sales as overall_total_sales
, total_sales.total_orders as overall_total_orders
, total_sales.daily_orders as overall_daily_orders
, total_sales.aov as overall_aov
, total_sales.delivered_qty as overall_units
-- , total_sales.scanned_gp_perc as overall_gp
, total_sales.nps_response as overall_nps
,overall_amendments.perc_amendments as overall_amendments
, ((ffs_perc_orders_delivery.late_perc*ffs_delivery.total_orders) + (pfc_perc_orders_delivery.Perc_Late_Delivered*pfc_delivery.total_orders))/overall_delivery.total_orders as overall_perc_late_delivered
, ((ffs_perc_orders_delivery.ontime_perc*ffs_delivery.total_orders) + (pfc_perc_orders_delivery.Perc_OnTime_Delivered*pfc_delivery.total_orders))/overall_delivery.total_orders as overall_perc_ontime_delivered
, overall_perf.perfect as overall_perfect
, overall_delivery.extra_fees as overall_extra_fees
, overall_delivery.total_orders/7 as overall_daily_delivery

/*PFC SALES (B2C+B2B)*/
, pfc_sales.total_sales as pfc_total_sales
, pfc_sales.total_orders as pfc_total_orders
, pfc_sales.daily_orders as pfc_daily_orders
, pfc_sales.aov as pfc_aov
, pfc_sales.delivered_qty as pfc_units
, pfc_slots_availability.Allocated as pfc_slots_availability
-- , pfc_sales.scanned_gp as pfc_gp
, pfc_sales.nps_response as pfc_nps
, pfc_sales.skucount as pfc_skucount
, pfc_amendments.perc_amendments as pfc_amendments
, pfc_perf.perfect as pfc_perfect
, pfc_delivery.extra_fees as pfc_extra_fees
, pfc_delivery.total_orders/7 as pfc_daily_delivery

/* B2B */
, b2b_sales.total_sales as b2b_total_sales
, b2b_sales.total_orders as b2b_total_orders
, b2b_sales.daily_orders as b2b_daily_orders
, b2b_sales.aov as b2b_aov
, b2b_sales.delivered_qty as b2b_units
-- , b2b_sales.scanned_gp as b2b_gp
, b2b_sales.nps_response as b2b_nps

/* FFS */
, ffs_sales.total_sales as ffs_total_sales
, ffs_sales.total_orders as ffs_total_orders
, ffs_sales.daily_orders as ffs_daily_orders
, ffs_sales.aov as ffs_aov
, ffs_sales.delivered_qty as ffs_units
-- , ffs_sales.scanned_gp as ffs_gp
, ffs_sales.nps_response as ffs_nps

, ffs_slots_availability.Allocated as ffs_slots_availability
, ffs_perc_orders_picked_late.Perc_Late_Picked as ffs_perc_picked_late
, ffs_perc_orders_delivery.late_perc as ffs_perc_late_delivered
, ffs_perc_orders_delivery.ontime_perc as ffs_perc_ontime_delivered

, ffs_amendments.perc_amendments as ffs_amendments
, ffs_perf.perfect as ffs_perfect
, ffs_delivery.extra_fees as ffs_extra_fees
, ffs_delivery.total_orders/7 as ffs_daily_delivery

/* FRESH */
, ffs_fresh_sales.fresh_pen_order as ffs_fresh_pen_order
, ffs_fresh_sales.fresh_pen_qty as ffs_fresh_pen_qty
, ffs_fresh_sales.fresh_pen_sales as ffs_fresh_pen_sales
, ffs_fresh_sales.fresh_aov as ffs_fresh_aov
, pfc_fresh_sales.fresh_pen_order as pfc_fresh_pen_order
, pfc_fresh_sales.fresh_pen_qty as pfc_fresh_pen_qty
, pfc_fresh_sales.fresh_pen_sales as pfc_fresh_pen_sales
, pfc_fresh_sales.fresh_aov as pfc_fresh_aov
, overall_fresh_sales.fresh_pen_order as overall_fresh_pen_order
, overall_fresh_sales.fresh_pen_qty as overall_fresh_pen_qty
, overall_fresh_sales.fresh_pen_sales as overall_fresh_pen_sales
, overall_fresh_sales.fresh_aov as overall_fresh_aov
, b2b_fresh_sales.fresh_pen_order as b2b_fresh_pen_order
, b2b_fresh_sales.fresh_pen_qty as b2b_fresh_pen_qty
, b2b_fresh_sales.fresh_pen_sales as b2b_fresh_pen_sales
, b2b_fresh_sales.fresh_aov as b2b_fresh_aov

/* MARKETPLACE IMPRESSION */
, overall_marketplace_impressions.instock_perc AS overall_marketplace_instock_perc
, pfc_marketplace_impressions.instock_perc AS pfc_marketplace_instock_perc
, ffs_marketplace_impressions.instock_perc AS ffs_marketplace_instock_perc
, ds_marketplace_impressions.instock_perc AS ds_marketplace_instock_perc

-- PFC Logistics
, pfc_perc_orders_delivery.perc_late_delivered as pfc_perc_late_delivered
, pfc_perc_orders_delivery.perc_ontime_delivered as pfc_perc_ontime_delivered
from total_sales
left join overall_delivery on overall_delivery.year = total_sales.year and overall_delivery.week = total_sales.week
left join overall_amendments on overall_amendments.year = total_sales.year and overall_amendments.week = total_sales.week
left join overall_perf on overall_perf.year = total_sales.year and overall_perf.week = total_sales.week
--pfc
left join pfc_sales on pfc_sales.year = total_sales.year and pfc_sales.week = total_sales.week
left join pfc_delivery on pfc_delivery.year = total_sales.year and pfc_delivery.week = total_sales.week
left join pfc_amendments on pfc_amendments.year = total_sales.year and pfc_amendments.week = total_sales.week
left join pfc_perf on pfc_perf.year = total_sales.year and pfc_perf.week = total_sales.week
left join pfc_slots_availability on pfc_slots_availability.year = total_sales.year and pfc_slots_availability.week = total_sales.week
--b2b
left join b2b_sales on b2b_sales.year = total_sales.year and b2b_sales.week = total_sales.week
-- ffs
left join ffs_sales on ffs_sales.year = total_sales.year and ffs_sales.week = total_sales.week
left join ffs_delivery on ffs_delivery.year = total_sales.year and ffs_delivery.week = total_sales.week
left join ffs_amendments on ffs_amendments.year = total_sales.year and ffs_amendments.week = total_sales.week
left join ffs_perf on ffs_perf.year = total_sales.year and ffs_perf.week = total_sales.week
left join ffs_slots_availability on ffs_slots_availability.year = total_sales.year and ffs_slots_availability.week = total_sales.week
left join ffs_perc_orders_picked_late on ffs_perc_orders_picked_late.year = total_sales.year and ffs_perc_orders_picked_late.week = total_sales.week
left join ffs_perc_orders_delivery on ffs_perc_orders_delivery.year = total_sales.year and ffs_perc_orders_delivery.week = total_sales.week
--fresh
left join ffs_fresh_sales on ffs_fresh_sales.year = total_sales.year and ffs_fresh_sales.week = total_sales.week
left join pfc_fresh_sales on pfc_fresh_sales.year = total_sales.year and pfc_fresh_sales.week = total_sales.week
left join overall_fresh_sales on overall_fresh_sales.year = total_sales.year and overall_fresh_sales.week = total_sales.week
left join b2b_fresh_sales on b2b_fresh_sales.year = total_sales.year and b2b_fresh_sales.week = total_sales.week
--marketplace
left join overall_marketplace_impressions on overall_marketplace_impressions.year = total_sales.year and overall_marketplace_impressions.week = total_sales.week
left join pfc_marketplace_impressions on pfc_marketplace_impressions.year = total_sales.year and pfc_marketplace_impressions.week = total_sales.week
left join ffs_marketplace_impressions on ffs_marketplace_impressions.year = total_sales.year and ffs_marketplace_impressions.week = total_sales.week
left join ds_marketplace_impressions on ds_marketplace_impressions.year = total_sales.year and ds_marketplace_impressions.week = total_sales.week
left join pfc_perc_orders_delivery on pfc_perc_orders_delivery.year = total_sales.year and pfc_perc_orders_delivery.week = total_sales.week
order by year, week asc
)
;
