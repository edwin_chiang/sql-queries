-- updated marketplace sellers with product_brand_group
-- updated addmissingwrong, damaged CR with count order instead of count ticket
-- removed all CS metrics (reading from fpon_cdm.zendesk_tickets_features_snapshot)
-- update amendments ordered_qty and delivered_qty fields
-- updated with new nps query 20201023
-- changed dim_ffs_logistics_driver_trips to ffs_logistics_driver_trips_opm 20210412
-- 20210507 change: For fresh calculations, changed delivered_qty to delivered_qty_with_normalised_weighted_items
-- 20210528 change: Split Marketplace into PFC, FFS, DS
CREATE OR REPLACE TABLE fpon_cdm.weeklykpi_raw_snapshot AS (
with nps AS (
  select * from fpon_cdm.dim_fpon_cx_nps_detail
),
------------------------------------------------
------------- TOTAL SALES ----------------------
------------------------------------------------
total_sales as
(
select
wk.year, wk.week,
sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response

from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join -- to obtain gp
  (select order_id, sum(gp_cost) as gp_cost
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
  */
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
  (
   select
  cal.year
  ,cal.week
  ,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count(order_ref_number ) as nps_score
from
(
  SELECT * FROM nps
) as t3
 left join fpon_cdm.calendar_full as cal on cal.date = date(t3.end_date)
group by cal.year
  ,cal.week
  ) as nps on nps.year = wk.year and nps.week = wk.week
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
group by wk.year, wk.week, nps.nps_score
)
,
overall_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY','FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('B2B','DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
overall_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select overall_delivery.year, overall_delivery.week
, perf.count * 100.0 / overall_delivery.total_orders as perc_amendments
from
overall_delivery
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('PFC_DELIVERY', 'FFS')
    and order_type in ('B2B', 'DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff =1
    group by p1.year, p1.week
    ) as perf on perf.year = overall_delivery.year and perf.week = overall_delivery.week
 )
,
------------------------------------------------
------------ PFC SALES (B2C+B2B) ---------------
------------------------------------------------
pfc_sales as
(
select
wk.year
, wk.week
,sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
-- , sum(sl.gp_cost) as total_cost
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
, (
  select count(distinct sku)
  from
  (
  select *,case when sku like '9_____%' then 1 else 0 end as is_marketplace
  from fpon_cdm.dim_fpon_store_product_df
  where product_status_store<>'HIDDEN' and product_status_global in ('ENABLED')
  and store_code='004'
  ) as t
  where is_marketplace=0 -- exclude marketplace
  and refresh_date = CURRENT_DATE()
  group by refresh_date
  )
  as skucount
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --, orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
  */
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  cal.year
  ,cal.week
--   ,new_delivery_type
,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count( order_ref_number ) as nps_score
from
(
select * from nps where business_type IN ('PFC','B2B')
) as t3
join fpon_cdm.calendar_full as cal
on date(t3.end_date) = cal.date
-- where new_delivery_type = 'ORD'

group by year
  ,week
--   ,new_delivery_type
  ) as nps on nps.year = wk.year and nps.week = wk.week

where
  sh.order_status not in ('CANCELLED')
  and order_ship_mode in ('PFC_DELIVERY')
  and wk.year >= 2020
group by
wk.year, wk.week, nps.nps_score
)
,
pfc_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY','B2B')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
pfc_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select total.year, total.week
, perf.count * 100.0 / total.total_orders as perc_amendments
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY','B2B')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where
    order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
  and order_type in ('DELIVERY','B2B')
   and wk.year >= 2020
    ) as p1
  where no_dropoff =1
  group by p1.year, p1.week
  ) as perf on perf.year = total.year and perf.week = total.week
)
,
-- pfc_slots_availability
pfc_slots_availability as
(
select
year, week
, round(sum(capacity)/7,0) as Allocated
from fpon_cdm.dim_pfc_operation_slot_capacity_df as pfc_slots
LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = pfc_slots.ds
group by wk.year, wk.week
)
,
------------------------------------------------
------------ PFC SALES (B2B) -------------------
------------------------------------------------
b2b_sales as
(
select
wk.year
, wk.week
,sum(sh.order_amount - sh.order_discount)/1.07 as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum(sh.order_amount) as order_amount
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount)/1.07) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --, orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id) as sl
  on sh.order_id = sl.order_id
 */
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  wk.year
  ,wk.week
--   ,new_delivery_type
,safe_divide((count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0,count( order_ref_number )) as nps_score
from
(
  select * from nps where business_type = 'B2B'
) as t3
JOIN fpon_cdm.calendar_full as wk on wk.date = date(t3.end_date)
-- where new_delivery_type = 'B2B'
where business_type = 'B2B'
group by year
  ,week
--   ,new_delivery_type
  ) as nps on nps.year = wk.year and nps.week = wk.week
where
  sh.order_status not in ('CANCELLED')
  and order_type in ('B2B')
  and wk.year >= 2020
group by
wk.year, wk.week, nps.nps_score
)
,
------------------------------------------------
------------- FFS SALES - 1 --------------------
------------------------------------------------
ffs_sales as
(
select
wk.year
, wk.week
,sum((sh.order_amount - sh.order_discount)/1.07) as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders -- without cancel
-- for calculation of fresh pen
, sum(sh.order_amount) as order_amount
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount/1.07)) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --,orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id
  ) as sl
  on sh.order_id = sl.order_id
  */
LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  cal.year
  ,cal.week
--   ,new_delivery_type
  ,count(case when nps_response_category in ('promoters') then order_ref_number end) as promoter_cnt
  ,count(case when nps_response_category in ('detractors') then order_ref_number end) as detractor_cnt
  ,count(distinct respondent_id ) as total_cnt
,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count( order_ref_number ) as nps_score
from
(
 select * from nps where business_type IN ('FFS Darkstore','FFS Normal')
) as t3
join fpon_cdm.calendar_full as cal
on date(t3.end_date)= cal.date
-- where new_delivery_type = 'FFS'
group by year
  ,week
--   ,new_delivery_type
) as nps on nps.year = wk.year and nps.week = wk.week
where
sh.order_status not in ('CANCELLED')
and order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
and wk.year >= 2020
group by
wk.year
, wk.week
, nps.nps_score
)
,
ffs_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, (sum(order_shipping) + sum(surcharge))/1.07 as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
)
,
------------------------------------------------
------------- FFS SALES - 2 --------------------
------------------------------------------------
/*
perc amendments - no dropoff
daily orders w cancellation
*/
ffs_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select total.year, total.week
, perf.count * 100.0 / total.total_orders as perc_amendments
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty_with_normalised_weighted_items - delivered_qty_with_normalised_weighted_items) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where
    order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
--     group by wk.year, wk.week, order_id ,ordered_qty ,delivered_qty
    ) as p1
  where no_dropoff =1
  group by p1.year, p1.week
  ) as perf on perf.year = total.year and perf.week = total.week
)
,
-- ffs slots
ffs_slots_availability as
(
select
year, week
, round(sum(allocated)/7,0) as Allocated
from fpon_analytics.ffs_slotcapacity_bq as ffs_slots
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = CAST(ffs_slots.date AS date)
    where year = 2020
      group by wk.year, wk.week
      order by week
)
,
ffs_perc_orders_picked_late as
(
-- % of orders picked late FOR FFS
-- Use calculation based on packing late formula from picker report
select
year, week, (Late_Packed / Total_Orders *100) as Perc_Late_Picked
FROM  (
  SELECT wk.year, wk.week,
      COUNT(DISTINCT ORD.id) Total_Orders
      ,SUM(CASE WHEN DATETIME_diff(DATETIME(OPM.end_time,'UTC'), DATETIME(ORD.preferred_date,PARSE_TIME("%T",ORD.slot_Start_time)), SECOND)>1 THEN 1 ELSE 0 END) as Late_Packed
    FROM fpon_ods.zs_order_service_orders ORD

    LEFT JOIN fpon_ods.zs_order_service_order_types ORDTYP ON ORD.type_id = ORDTYP.id
    LEFT JOIN fpon_ods.zs_order_service_order_process_mapping OPM ON ORD.id = OPM.order_id
    LEFT JOIN fpon_cdm.calendar_full as wk ON DATE(OPM.Start_Time)=wk.date
    WHERE ORD.store_id IN
            (SELECT STR.id FROM fpon_ods.zs_organization_service_stores STR
             WHERE STR.Deleted_At is null AND STR.id not in (165)
                AND STR.has_delivery_hub = 1 AND STR.STATUS = 'ENABLED' ) -- FFS Stores only
            AND ORD.Type_ID = 2 -- HD orders only
            AND OPM.process_name =  'PACKING' AND OPM.status = 'SUCCESS'
            AND wk.year >= 2020
    GROUP BY wk.year, wk.week
   ))
,
ffs_perc_orders_delivery as
(-- % of orders out late / on time for delivery FOR FFS
select b.year,b.week
,count(case when new_delivery_status in ('On Time') then 1 end) *100.0/count(*) as ontime_perc
,count(case when new_delivery_status in ('Late') then 1 end) *100.0/count(*) as late_perc
,count(case when new_delivery_status in ('Early') then 1 end)*100.0/count(*) as early_perc
from fpon_cdm.ffs_logistics_driver_trips_opm as driver
join fpon_cdm.calendar_full as b on driver.date_of_delivery = b.date
where driver.order_status in ('COMPLETED') and trip_status in ('SUCCESS')
group by b.year,b.week
)
,
------------------------------------------------
----------------- DARKSTORE --------------------
------------------------------------------------
ds_sales as
(
select
wk.year
, wk.week
,sum((sh.order_amount - sh.order_discount)/1.07) as total_sales -- ZS calculation
, count(sh.order_id) as total_orders
, count(sh.order_id) / 7 as daily_orders -- without cancel
-- for calculation of fresh pen
, sum(sh.order_amount) as order_amount
, sum(delivered_qty_with_normalised_weighted_items) as delivered_qty
, sum((sh.order_amount - sh.order_discount)/1.07) / count(sh.order_id) as aov
-- , sum(sl.gp_cost) as total_cost
-- , 1 - sum(sl.gp_cost) / (sum(sh.order_amount/1.07)) as scanned_gp_perc -- gp% calculated by 1 - cost%
, nps.nps_score as nps_response
from fpon_cdm.fpon_cdm_sales_head as sh
/*
left join
  (select order_id, sum(gp_cost) as gp_cost --,orderitems_id
  from fpon_cdm.fpon_cdm_sales_line
  group by order_id
  ) as sl
  on sh.order_id = sl.order_id
  */
LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_order
-- for calculation of nps_response: (#promoters - #detractors)/#responders
left join
(
select
  cal.year
  ,cal.week
--   ,new_delivery_type
  ,count(case when nps_response_category in ('promoters') then order_ref_number end) as promoter_cnt
  ,count(case when nps_response_category in ('detractors') then order_ref_number end) as detractor_cnt
  ,count(distinct respondent_id ) as total_cnt
,(count(case when nps_response_category in ('promoters') then order_ref_number end)-count(case when nps_response_category in ('detractors') then order_ref_number end))*100.0/count( order_ref_number ) as nps_score
from
(
 select * from nps where business_type IN ('FFS Darkstore')
) as t3
join fpon_cdm.calendar_full as cal
on date(t3.end_date)= cal.date
-- where new_delivery_type = 'FFS'
group by year
  ,week
--   ,new_delivery_type
) as nps on nps.year = wk.year and nps.week = wk.week
where
sh.order_status not in ('CANCELLED')
and order_ship_mode_level2 in ('FFS Normal')
and order_type in ('DELIVERY')
and wk.year >= 2020
group by
wk.year
, wk.week
, nps.nps_score
)
,
------------------------------------------------
------------- FRESH ----------------------------
------------------------------------------------
ffs_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , sl.order_id
  , product_sku
  , sl.delivered_qty_with_normalised_weighted_items AS delivered_qty
  , sl.order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
  --left join fpon_cdm.fpon_cdm_sales_head sh on sl.order_id = sh.order_id
  LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  sl.order_ship_mode in ('FFS')
  and sl.order_status not in ('CANCELLED')
  and sl.order_type in ('DELIVERY')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
ffs_fresh_sales as
(
select
ffs_sales.year
, ffs_sales.week
, fresh.total_orders*100.0/ ffs_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ ffs_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ ffs_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / ffs_sales.total_orders as fresh_aov
from ffs_sales
left join ffs_freshtable as fresh
on fresh.week = ffs_sales.week and fresh.year = ffs_sales.year
)
,
pfc_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items as delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('PFC_DELIVERY')
  and order_type in ('DELIVERY','B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
pfc_fresh_sales as
(
select
pfc_sales.year
, pfc_sales.week
, fresh.total_orders*100.0/ pfc_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ pfc_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ pfc_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / pfc_sales.total_orders as fresh_aov
from pfc_sales
left join pfc_freshtable as fresh
on fresh.week = pfc_sales.week and fresh.year = pfc_sales.year
)
,
overall_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items as delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('FFS','PFC_DELIVERY')
  and order_type in ('DELIVERY','B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
overall_fresh_sales as
(
select
total_sales.year
, total_sales.week
, fresh.total_orders*100.0/ total_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ total_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ total_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / total_sales.total_orders as fresh_aov
from total_sales as total_sales
left join overall_freshtable as fresh
on fresh.week = total_sales.week and fresh.year = total_sales.year
)
,
b2b_freshtable as
(
select
year
, week
, count(distinct freshsku.order_id) as total_orders
, sum(freshsku.delivered_qty) as delivered_qty
, sum(freshsku.order_item_invoiced_amount) as order_amount
from
  (
  select distinct
  wk.year
  , wk.week
  , order_id
  , product_sku
  , delivered_qty_with_normalised_weighted_items as delivered_qty
  , order_item_invoiced_amount
  , case when sl.sap_department in ('FRESH FOOD')
    and sl.sap_class IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES')
    and sap_subclass NOT LIKE '%FROZEN%'
    and sap_subclass NOT LIKE '%SURIMI%'
    and sap_subclass NOT LIKE '%PROCESSED%'
    and sap_subclass NOT LIKE '%PRESERVED%'
    and sap_subclass NOT LIKE '%DUMMY%'
    and sap_subclass NOT LIKE '%PLANTS%'
    then 1
    else 0
    end as fresh
  from fpon_cdm.fpon_cdm_sales_line sl
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sl.date_of_order
  where
  order_ship_mode in ('PFC_DELIVERY')
  and order_type in ('B2B')
  and sl.order_status not in ('CANCELLED')
  and wk.year >= 2020
            -- and wk.week >= 40
  ) as freshsku
  where fresh = 1
  group by year, week
)
,
b2b_fresh_sales as
(
select
b2b_sales.year
, b2b_sales.week
, fresh.total_orders*100.0/ b2b_sales.total_orders as fresh_pen_order
, fresh.delivered_qty*100.0/ b2b_sales.delivered_qty as fresh_pen_qty
, fresh.order_amount*100.0/ b2b_sales.order_amount as fresh_pen_sales
, (fresh.order_amount / 1.07) / b2b_sales.total_orders as fresh_aov
from b2b_sales
left join b2b_freshtable as fresh
on fresh.week = b2b_sales.week and fresh.year = b2b_sales.year
)
,

/* PFC PERC ONTIME */
-- % of orders out late / on time for delivery FOR PFC
order_deliver_history as
(
select orderid
,wk.year
,wk.week
, delivery_date
, delivery_time
, delivery_window
, DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)) as delivery_datetime
, TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC') as delivery_datetime2
, TIMESTAMP_ADD(TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC'), interval 15 minute) as calculated_15mins_ear
, DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])) as delivery_datewindow_start
, DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))) as delivery_datewindow_end
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])),'UTC') as delivery_datewindow_startx
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))),'UTC') as delivery_datewindow_endx
, distance_metres
, latitude
, longitude
, status
from
(SELECT * FROM `cds-sc-data-cloud-prod.fpg_sc_ods.fp_gls_tms_order_delivery_history` where delivery_time is not null AND REGEXP_CONTAINS(delivery_time,'[a-zA-Z0-9]') = TRUE)
 as tms
left join fpon_cdm.fpon_cdm_sales_head as sh on sh.order_ref_number = tms.orderid
LEFT JOIN fpon_cdm.calendar_full as wk -- to obtain FP weeks
  ON (tms.delivery_date) = wk.date
--sh.order_status in ('COMPLETED','DISPATCHED')
group by orderid
, year, week
, order_ship_mode, order_type
, tms.delivery_time, tms.delivery_date, tms.delivery_window, distance_metres, latitude, longitude, status
order by delivery_date desc
)
,
filter1 as
(
select
year
, week
, orderid
, distance_metres
, latitude
, longitude
, delivery_date
, delivery_datetime2
,TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)
, delivery_datewindow_startx
, delivery_datewindow_endx
,status
,case when upper(status) = 'ONTIME' and delivery_datewindow_startx >= TIMESTAMP_ADD(delivery_datetime2, INTERVAL 15 MINUTE) or (distance_metres > 500) then 'LATE'
when upper(status) = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE) then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE_EARLY
,case when status = 'ONTIME' and (distance_metres > 500) then 'LATE'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)  then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE
from order_deliver_history
)
,
filter2 as
(
select
year
, week
,count(*) as total_orders
,sum(case when LATE_EARLY = 'LATE' then 1 else 0 end) as LATE_EARLY_COUNT
,sum(case when LATE = 'LATE' then 1 else 0 end) as LATE_COUNT
from filter1
group by year, week
)
,
pfc_perc_orders_delivery as
(
select
year
, week
,total_orders
,LATE_COUNT * 100.0 / total_orders as Perc_Late_Delivered
, 100.00 - (LATE_COUNT * 100.0 / total_orders) as Perc_OnTime_Delivered
,LATE_EARLY_COUNT * 100.0 /total_orders as late_perc_applying_tooearly
from filter2
order by week
)
,
----- SUPER PERFECT DEFINITION
super_perfect_def as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty_with_normalised_weighted_items - sh.delivered_qty_with_normalised_weighted_items as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when pfc_ontime.LATE in ('ONTIME') THEN 1
  when pfc_ontime.LATE_EARLY IN ('ONTIME') then 1
  when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join fpon_cdm.zendesk_full_table_snapshot as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join filter1 as pfc_ontime on pfc_ontime.orderid = sh.order_ref_number
left join fpon_cdm.ffs_logistics_driver_trips_opm as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
--select * from perfect_def
,
overall_superperf as
(
select
super_perfect_def.year, super_perfect_def.week
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ overall_delivery.total_orders as perfect
from super_perfect_def
left join overall_delivery
on overall_delivery.year = super_perfect_def.year and overall_delivery.week = super_perfect_def.week
where order_ship_mode in ('PFC_DELIVERY','FFS')
and order_type in ('B2B','DELIVERY')
group by super_perfect_def.year, super_perfect_def.week, overall_delivery.total_orders
order by super_perfect_def.week asc
)
,
pfc_superperf as
(
select
super_perfect_def.year, super_perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ pfc_delivery.total_orders as perfect
from super_perfect_def
left join pfc_delivery
on pfc_delivery.year = super_perfect_def.year and pfc_delivery.week = super_perfect_def.week
where order_ship_mode in ('PFC_DELIVERY')
and order_type in ('DELIVERY','B2B')
group by super_perfect_def.year, super_perfect_def.week, pfc_delivery.total_orders
,order_ship_mode
order by super_perfect_def.week asc
)
,
ffs_superperf as
(
select
super_perfect_def.year, super_perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ ffs_delivery.total_orders as perfect
from super_perfect_def
left join ffs_delivery
on ffs_delivery.year = super_perfect_def.year and ffs_delivery.week = super_perfect_def.week
where order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
group by super_perfect_def.year, super_perfect_def.week, order_ship_mode, ffs_delivery.total_orders
order by super_perfect_def.week asc
)
,

---PERFECT DEFINITION
perfect_def as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty_with_normalised_weighted_items - sh.delivered_qty_with_normalised_weighted_items as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when pfc_ontime.LATE in ('ONTIME') THEN 1
  when pfc_ontime.LATE_EARLY IN ('ONTIME') then 1
  when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join (SELECT * FROM fpon_cdm.zendesk_full_table_snapshot WHERE feedback_category_new IN ('order_issues__damaged_items','feedback/suggestion__driver_attitude','order_issues__late','order_issues__no_show','order_issues__missing_items','order_issues__wrong/additional_items')) as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join filter1 as pfc_ontime on pfc_ontime.orderid = sh.order_ref_number
left join fpon_cdm.ffs_logistics_driver_trips_opm as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
--select * from perfect_def
,
overall_perf as
(
select
perfect_def.year, perfect_def.week
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ overall_delivery.total_orders as perfect
from perfect_def
left join overall_delivery
on overall_delivery.year = perfect_def.year and overall_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY','FFS')
and order_type in ('B2B','DELIVERY')
group by perfect_def.year, perfect_def.week, overall_delivery.total_orders
order by perfect_def.week asc
)
,
pfc_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ pfc_delivery.total_orders as perfect
from perfect_def
left join pfc_delivery
on pfc_delivery.year = perfect_def.year and pfc_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY')
and order_type in ('DELIVERY','B2B')
group by perfect_def.year, perfect_def.week, pfc_delivery.total_orders
,order_ship_mode
order by perfect_def.week asc
)
,
ffs_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ ffs_delivery.total_orders as perfect
from perfect_def
left join ffs_delivery
on ffs_delivery.year = perfect_def.year and ffs_delivery.week = perfect_def.week
where order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
group by perfect_def.year, perfect_def.week, order_ship_mode, ffs_delivery.total_orders
order by perfect_def.week asc
)
,
------------------------------------------------
------------- MARKETPLACE ----------------------
------------------------------------------------
pfc_marketplace_sales as
(select wk.year, wk.week, sl.*, sh.final_order_amount
from
fpon_cdm.fpon_cdm_sales_line as sl
left join (select order_ref_number, final_order_amount from fpon_cdm.fpon_cdm_sales_head) as sh on sl.order_ref_number=sh.order_ref_number
left join fpon_cdm.calendar_full as wk ON (sl.date_of_order) = wk.date
--left join fpon_cdm.fpon_cdm_product as pdt on pdt.product_id = sl.product_id
where sl.is_marketplace=1 and sl.order_ship_mode in ('PFC_DELIVERY')
and UPPER(sl.order_item_status) not in ('CANCELLED')
and sl.order_status not in ('CANCELLED')
)
,
pfc_marketplace_sl
as
( select
      mp.year
      ,mp.week
      ,count(distinct order_ref_number) as total_orders
      ,count(distinct order_ref_number)/7 as daily_orders
      ,(select count(distinct product_brand_group)
    from fpon_cdm.fpon_cdm_product
    where is_marketplace=1
      ) as sellers
      ,count(distinct orderitems_id) as total_unique_SKU
      ,sum(order_item_ordered_amount)/1.07 as total_sales
      , (sum(order_item_ordered_amount)/1.07)/ 7 as daily_sales
      ,sum(ordered_qty)/7 as daily_units -- daily_units
      ,sum(order_item_ordered_amount)/(1.07*SUM(ordered_qty)) as asp  --avg(sl.unit_price)-- ASP

      , (select count(distinct client_item_id)
        from
        (
        select distinct client_store_id
        , s.name
        , i.product_id -- zs product id
        , p.client_item_id -- sap product id
        , stock
        , case when p.client_item_id like '9_____%' then 1 else 0 end as is_marketplace
        --, trunc(sysdate) as date_updated
        from fpon_ods.zs_catalogue_service_products as p
        left join fpon_ods.zs_inventory_service_items as i on concat('1',cast(p.id as STRING)) =  cast(i.product_id as STRING)
        left join  fpon_ods.zs_organization_service_stores as s on s.id = i.store_id
        where i.status='ENABLED'
        and  p.organization_id = 2 and  p.status='ENABLED'
        and   s.organization_id = 2 and p.deleted_at is null
        and primary_category_id not in (3356) -- exclude scan&go
        and client_store_id = '004'
        -- and (unlimited_stock=1 or stock>0) IN stock
        order by client_store_id, s.name
        )
        where is_marketplace =1 )
        as skucount
  from pfc_marketplace_sales mp
    group by mp.year,mp.week
    order by mp.year, mp.week asc
)
,
pfc_marketplace_sh as
(
select
year, week,
--(sum(final_order_amount)/1.07) as sh_sales,
(sum(final_order_amount)/1.07)/count(distinct order_ref_number) as aov
from
(
select distinct year, week
, order_ref_number, final_order_amount
from pfc_marketplace_sales
)
group by year, week
)
,
pfc_marketplace_pen as (
  select pfc.year
      , pfc.week
      , mp.total_sales / pfc.total_sales as pfc_mp_gmv_penetration
      , mp.total_orders / pfc.total_orders as pfc_mp_order_penetration
  from pfc_sales pfc
  left join pfc_marketplace_sl mp
  on pfc.year = mp.year
  and pfc.week = mp.week
)

,

ffs_marketplace_sales as
(select wk.year, wk.week, sl.*, sh.final_order_amount
from
fpon_cdm.fpon_cdm_sales_line as sl
left join (select order_ref_number, final_order_amount from fpon_cdm.fpon_cdm_sales_head) as sh on sl.order_ref_number=sh.order_ref_number
left join fpon_cdm.calendar_full as wk ON (sl.date_of_order) = wk.date
--left join fpon_cdm.fpon_cdm_product as pdt on pdt.product_id = sl.product_id
where sl.is_marketplace=1 and sl.order_ship_mode_level2 in ('FFS Normal')
and UPPER(sl.order_item_status) not in ('CANCELLED')
and sl.order_status not in ('CANCELLED')
)
,

ffs_marketplace_sl as
( select
      year
      ,week
      ,count(distinct order_ref_number) as total_orders
      ,count(distinct order_ref_number)/7 as daily_orders
      ,(select count(distinct product_brand_group)
    from fpon_cdm.fpon_cdm_product
    where is_marketplace=1
      ) as sellers
      ,count(distinct orderitems_id) as total_unique_SKU
      ,sum(order_item_ordered_amount)/1.07 as total_sales
      , (sum(order_item_ordered_amount)/1.07)/ 7 as daily_sales
      ,sum(ordered_qty)/7 as daily_units -- daily_units
      ,sum(order_item_ordered_amount)/(1.07*SUM(ordered_qty)) as asp  --avg(sl.unit_price)-- ASP

      , (select count(distinct client_item_id)
        from
        (
        select distinct client_store_id
        , s.name
        , i.product_id -- zs product id
        , p.client_item_id -- sap product id
        , stock
        , case when p.client_item_id like '9_____%' then 1 else 0 end as is_marketplace
        --, trunc(sysdate) as date_updated
        from fpon_ods.zs_catalogue_service_products as p
        left join fpon_ods.zs_inventory_service_items as i on concat('1',cast(p.id as STRING)) =  cast(i.product_id as STRING)
        left join  fpon_ods.zs_organization_service_stores as s on s.id = i.store_id
        where i.status='ENABLED'
        and  p.organization_id = 2 and  p.status='ENABLED'
        and   s.organization_id = 2 and p.deleted_at is null
        and primary_category_id not in (3356) -- exclude scan&go
--         and client_store_id IN ('')
        and s.name LIKE '%FFS%'
        -- and (unlimited_stock=1 or stock>0) IN stock
        order by client_store_id, s.name
        )
        where is_marketplace =1 )
        as skucount
from
ffs_marketplace_sales
    group by year,week
    order by year, week asc
)
,
ffs_marketplace_sh as
(
select
year, week,
--(sum(final_order_amount)/1.07) as sh_sales,
(sum(final_order_amount)/1.07)/count(distinct order_ref_number) as aov
from
(
select distinct year, week
, order_ref_number, final_order_amount
from ffs_marketplace_sales
)
group by year, week
)
,
ffs_marketplace_pen as (
  select ffs.year
      , ffs.week
      , mp.total_sales / ffs.total_sales as ffs_mp_gmv_penetration
      , mp.total_orders / ffs.total_orders as ffs_mp_order_penetration
  from ffs_sales ffs
  left join ffs_marketplace_sl mp
  on ffs.year = mp.year
  and ffs.week = mp.week
)
,

ds_marketplace_sales as
(select wk.year, wk.week, sl.*, sh.final_order_amount
from
fpon_cdm.fpon_cdm_sales_line as sl
left join (select order_ref_number, final_order_amount from fpon_cdm.fpon_cdm_sales_head) as sh on sl.order_ref_number=sh.order_ref_number
left join fpon_cdm.calendar_full as wk ON (sl.date_of_order) = wk.date
--left join fpon_cdm.fpon_cdm_product as pdt on pdt.product_id = sl.product_id
where sl.is_marketplace=1 and sl.order_ship_mode_level2 in ('FFS Darkstore')
and UPPER(sl.order_item_status) not in ('CANCELLED')
and sl.order_status not in ('CANCELLED')
)
,

ds_marketplace_sl as
( select
      year
      ,week
      ,count(distinct order_ref_number) as total_orders
      ,count(distinct order_ref_number)/7 as daily_orders
      ,(select count(distinct product_brand_group)
    from fpon_cdm.fpon_cdm_product
    where is_marketplace=1
      ) as sellers
      ,count(distinct orderitems_id) as total_unique_SKU
      ,sum(order_item_ordered_amount)/1.07 as total_sales
      , (sum(order_item_ordered_amount)/1.07)/ 7 as daily_sales
      ,sum(ordered_qty)/7 as daily_units -- daily_units
      ,sum(order_item_ordered_amount)/(1.07*SUM(ordered_qty)) as asp  --avg(sl.unit_price)-- ASP

      , (select count(distinct client_item_id)
        from
        (
        select distinct client_store_id
        , s.name
        , i.product_id -- zs product id
        , p.client_item_id -- sap product id
        , stock
        , case when p.client_item_id like '9_____%' then 1 else 0 end as is_marketplace
        --, trunc(sysdate) as date_updated
        from fpon_ods.zs_catalogue_service_products as p
        left join fpon_ods.zs_inventory_service_items as i on concat('1',cast(p.id as STRING)) =  cast(i.product_id as STRING)
        left join  fpon_ods.zs_organization_service_stores as s on s.id = i.store_id
        where i.status='ENABLED'
        and  p.organization_id = 2 and  p.status='ENABLED'
        and   s.organization_id = 2 and p.deleted_at is null
        and primary_category_id not in (3356) -- exclude scan&go
        and client_store_id IN ('098')
--         and s.name LIKE '%DS%'
        -- and (unlimited_stock=1 or stock>0) IN stock
        order by client_store_id, s.name
        )
        where is_marketplace =1 )
        as skucount
from
ds_marketplace_sales
    group by year,week
    order by year, week asc
)
,
ds_marketplace_sh as
(
select
year, week,
--(sum(final_order_amount)/1.07) as sh_sales,
(sum(final_order_amount)/1.07)/count(distinct order_ref_number) as aov
from
(
select distinct year, week
, order_ref_number, final_order_amount
from ds_marketplace_sales
)
group by year, week
)
,
ds_marketplace_pen as (
  select ds.year
      , ds.week
      , mp.total_sales / ds.total_sales as ds_mp_gmv_penetration
      , mp.total_orders / ds.total_orders as ds_mp_order_penetration
  from ds_sales ds
  left join ds_marketplace_sl mp
  on ds.year = mp.year
  and ds.week = mp.week
)

,
overall_marketplace_sales as
(select wk.year, wk.week, sl.*, sh.final_order_amount
from
fpon_cdm.fpon_cdm_sales_line as sl
left join (select order_ref_number, final_order_amount from fpon_cdm.fpon_cdm_sales_head) as sh on sl.order_ref_number=sh.order_ref_number
left join fpon_cdm.calendar_full as wk ON (sl.date_of_order) = wk.date
--left join fpon_cdm.fpon_cdm_product as pdt on pdt.product_id = sl.product_id
where sl.is_marketplace=1 
-- and sl.order_ship_mode_level2 in ('FFS Darkstore')
and UPPER(sl.order_item_status) not in ('CANCELLED')
and sl.order_status not in ('CANCELLED')
)
,

overall_marketplace_sl as
( select
      year
      ,week
      ,count(distinct order_ref_number) as total_orders
      ,count(distinct order_ref_number)/7 as daily_orders
      ,(select count(distinct product_brand_group)
    from fpon_cdm.fpon_cdm_product
    where is_marketplace=1
      ) as sellers
      ,count(distinct orderitems_id) as total_unique_SKU
      ,sum(order_item_ordered_amount)/1.07 as total_sales
      , (sum(order_item_ordered_amount)/1.07)/ 7 as daily_sales
      ,sum(ordered_qty)/7 as daily_units -- daily_units
      ,sum(order_item_ordered_amount)/(1.07*SUM(ordered_qty)) as asp  --avg(sl.unit_price)-- ASP

      , (select count(distinct client_item_id)
        from
        (
        select distinct client_store_id
        , s.name
        , i.product_id -- zs product id
        , p.client_item_id -- sap product id
        , stock
        , case when p.client_item_id like '9_____%' then 1 else 0 end as is_marketplace
        --, trunc(sysdate) as date_updated
        from fpon_ods.zs_catalogue_service_products as p
        left join fpon_ods.zs_inventory_service_items as i on concat('1',cast(p.id as STRING)) =  cast(i.product_id as STRING)
        left join  fpon_ods.zs_organization_service_stores as s on s.id = i.store_id
        where i.status='ENABLED'
        and  p.organization_id = 2 and  p.status='ENABLED'
        and   s.organization_id = 2 and p.deleted_at is null
        and primary_category_id not in (3356) -- exclude scan&go
--         and client_store_id IN ('098')
--         and s.name LIKE '%DS%'
        -- and (unlimited_stock=1 or stock>0) IN stock
        order by client_store_id, s.name
        )
        where is_marketplace =1 )
        as skucount
from
overall_marketplace_sales
    group by year,week
    order by year, week asc
)
,
overall_marketplace_sh as
(
select
year, week,
--(sum(final_order_amount)/1.07) as sh_sales,
(sum(final_order_amount)/1.07)/count(distinct order_ref_number) as aov
from
(
select distinct year, week
, order_ref_number, final_order_amount
from overall_marketplace_sales
)
group by year, week
)
,
overall_marketplace_pen as (
  select overall.year
      , overall.week
      , mp.total_sales / overall.total_sales as overall_mp_gmv_penetration
      , mp.total_orders / overall.total_orders as overall_mp_order_penetration
  from total_sales overall
  left join overall_marketplace_sl mp
  on overall.year = mp.year
  and overall.week = mp.week
)


select tbl.*
 /* PFC MARKETPLACE */
    , pfc_marketplace_sl.total_sales as pfc_marketplace_total_sales
    , pfc_marketplace_sl.total_orders as pfc_marketplace_total_orders
    , pfc_marketplace_sl.daily_orders as pfc_marketplace_daily_orders
    , pfc_marketplace_sl.sellers as pfc_marketplace_sellers
    , pfc_marketplace_sh.aov as pfc_marketplace_aov
    , pfc_marketplace_sl.daily_units as pfc_marketplace_units
    , pfc_marketplace_sl.asp as pfc_marketplace_asp
    , pfc_marketplace_sl.skucount as pfc_marketplace_skucount
    , pfc_marketplace_pen.pfc_mp_gmv_penetration 
    , pfc_marketplace_pen.pfc_mp_order_penetration  
 /* FFS MARKETPLACE */
    , ffs_marketplace_sl.total_sales as ffs_marketplace_total_sales
    , ffs_marketplace_sl.total_orders as ffs_marketplace_total_orders
    , ffs_marketplace_sl.daily_orders as ffs_marketplace_daily_orders
    , ffs_marketplace_sl.sellers as ffs_marketplace_sellers
    , ffs_marketplace_sh.aov as ffs_marketplace_aov
    , ffs_marketplace_sl.daily_units as ffs_marketplace_units
    , ffs_marketplace_sl.asp as ffs_marketplace_asp
    , ffs_marketplace_sl.skucount as ffs_marketplace_skucount
    , ffs_marketplace_pen.ffs_mp_gmv_penetration 
    , ffs_marketplace_pen.ffs_mp_order_penetration
 /* DS MARKETPLACE */
    , ds_marketplace_sl.total_sales as ds_marketplace_total_sales
    , ds_marketplace_sl.total_orders as ds_marketplace_total_orders
    , ds_marketplace_sl.daily_orders as ds_marketplace_daily_orders
    , ds_marketplace_sl.sellers as ds_marketplace_sellers
    , ds_marketplace_sh.aov as ds_marketplace_aov
    , ds_marketplace_sl.daily_units as ds_marketplace_units
    , ds_marketplace_sl.asp as ds_marketplace_asp
    , ds_marketplace_sl.skucount as ds_marketplace_skucount
    , ds_marketplace_pen.ds_mp_gmv_penetration 
    , ds_marketplace_pen.ds_mp_order_penetration
/* DS MARKETPLACE */
    , overall_marketplace_sl.total_sales as overall_marketplace_total_sales
    , overall_marketplace_sl.total_orders as overall_marketplace_total_orders
    , overall_marketplace_sl.daily_orders as overall_marketplace_daily_orders
    , overall_marketplace_sl.sellers as overall_marketplace_sellers
    , overall_marketplace_sh.aov as overall_marketplace_aov
    , overall_marketplace_sl.daily_units as overall_marketplace_units
    , overall_marketplace_sl.asp as overall_marketplace_asp
    , overall_marketplace_sl.skucount as overall_marketplace_skucount
    , overall_marketplace_pen.overall_mp_gmv_penetration 
    , overall_marketplace_pen.overall_mp_order_penetration
    
    , overall_superperf.perfect as overall_superperf
    , pfc_superperf.perfect as pfc_superperf
    , ffs_superperf.perfect as ffs_superperf
FROM fpon_cdm.weeklykpi_raw_snapshot as tbl
left join overall_superperf on overall_superperf.year = tbl.year and overall_superperf.week = tbl.week
left join pfc_superperf on pfc_superperf.year = tbl.year and pfc_superperf.week = tbl.week
left join ffs_superperf on ffs_superperf.year = tbl.year and ffs_superperf.week = tbl.week
left join pfc_marketplace_sl on pfc_marketplace_sl.year = tbl.year and pfc_marketplace_sl.week = tbl.week
left join pfc_marketplace_sh on pfc_marketplace_sh.year = tbl.year and pfc_marketplace_sh.week = tbl.week
left join pfc_marketplace_pen on pfc_marketplace_pen.year = tbl.year and pfc_marketplace_pen.week = tbl.week
left join ffs_marketplace_sl on ffs_marketplace_sl.year = tbl.year and ffs_marketplace_sl.week = tbl.week
left join ffs_marketplace_sh on ffs_marketplace_sh.year = tbl.year and ffs_marketplace_sh.week = tbl.week
left join ffs_marketplace_pen on ffs_marketplace_pen.year = tbl.year and ffs_marketplace_pen.week = tbl.week
left join ds_marketplace_sl on ds_marketplace_sl.year = tbl.year and ds_marketplace_sl.week = tbl.week
left join ds_marketplace_sh on ds_marketplace_sh.year = tbl.year and ds_marketplace_sh.week = tbl.week
left join ds_marketplace_pen on ds_marketplace_pen.year = tbl.year and ds_marketplace_pen.week = tbl.week
left join overall_marketplace_sl on overall_marketplace_sl.year = tbl.year and overall_marketplace_sl.week = tbl.week
left join overall_marketplace_sh on overall_marketplace_sh.year = tbl.year and overall_marketplace_sh.week = tbl.week
left join overall_marketplace_pen on overall_marketplace_pen.year = tbl.year and overall_marketplace_pen.week = tbl.week
)