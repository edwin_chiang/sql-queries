CREATE OR REPLACE TABLE 
`fairprice-bigquery.fpon_analytics.ffs_picker_efficiency` AS 
(
    with tbl AS (
SELECT TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR) AS event_timestamp, picker_id,batch_id,order_id, CASE WHEN store_id = '0' THEN '461' ELSE store_id END AS store_id, order_pick_type
       , LAG(product_id) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS product_id
       , LAG(product_name) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS product_name
       , screen_name,event_action,event_label
       , LAG(product_location) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS product_location
       , prev_screen_name, product_count, product_qty, version, event_seq
FROM (
SELECT event_timestamp, picker_id, batch_id,order_id, store_id, product_id, order_pick_type, product_name,screen_name,event_action,event_label, product_location, product_count, product_qty,
       LAG(screen_name) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS prev_screen_name, version, ROW_NUMBER() OVER (PARTITION BY order_id, event_action, screen_name ORDER BY event_timestamp ASC) AS event_seq
FROM `fairprice-bigquery.fpon_analytics.dim_picker_app_custom_event_bq` event
WHERE version IN ('5.0.1','5.0.0','5.0.2')
AND order_pick_type = 'Single'
AND order_id IS NOT NULL
# ORDER BY order_id,event_timestamp
) 
# ORDER BY order_id,event_timestamp
)
,

multi AS (
    SELECT TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR) AS event_timestamp, 
           picker_id,
           batch_id, 
           order_id,
           store_id,
           order_pick_type,
           product_id,
           product_name,
           screen_name,
           event_action,
           event_label,
           product_location,
           CAST(null AS STRING) as prev_screen_name,
           product_count,
           product_qty, 
           version,
           rnk,
           event_seq,
          LAG(TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR)) OVER (PARTITION BY batch_id ORDER BY TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR) ASC) AS prev_event_timestamp,
           TIMESTAMP_DIFF(TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR), 
                          LAG(TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR)) OVER (PARTITION BY batch_id ORDER BY TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR) ASC),
                          SECOND) AS time_between_picks
    FROM (
        SELECT a.*,
          b.end_picking_time, 
          ROW_NUMBER() OVER (PARTITION BY a.batch_id, product_id ORDER BY event_timestamp ASC) AS rnk
        FROM (
            SELECT *,
            LAG(event_label) OVER (PARTITION BY batch_id ORDER BY event_timestamp ASC) AS event_label_lag1,
            ROW_NUMBER() OVER (PARTITION BY batch_id, screen_name, event_action ORDER BY event_timestamp ASC) AS event_seq
            FROM `fairprice-bigquery.fpon_analytics.dim_picker_app_custom_event_bq`
            WHERE 1=1
            AND order_pick_type = 'Multi'
            AND cast(replace(version,'.','') as int64) >= 503
            AND cast(replace(version,'.','') as int64) <> 510
            # AND event_label = 'Proceed'
            # AND cast(product_qty as string) LIKE '%0.%'
            # ORDER BY cast(replace(version,'.','') as int64),batch_id, event_timestamp
            ) a
        JOIN (
            SELECT batch_id, event_timestamp as end_picking_time
            FROM (
                SELECT *, ROW_NUMBER() OVER (PARTITION BY batch_id ORDER BY event_timestamp ASC) AS rnk
                FROM `fairprice-bigquery.fpon_analytics.dim_picker_app_custom_event_bq`
                WHERE 1=1
                AND order_pick_type = 'Multi'
                AND cast(replace(version,'.','') as int64) >= 503
                # AND event_label IN ('Proceed','Force Pick')
                AND event_label = 'Finish Picking'
                # AND batch_id = '342115'
                # group by batch_id
                # HAVING COUNT(*) > 1
                ORDER BY event_timestamp
            )
            WHERE rnk = 1
        ) b
        ON a.batch_id = b.batch_id
        WHERE 1=1
        AND event_timestamp < end_picking_time
        AND (event_label = 'Proceed'
        AND event_label_lag1 LIKE '%Sku%')
        OR (event_seq = 1 AND screen_name = 'Picking List' AND event_action = 'Landed')
    )
    WHERE rnk = 1
    AND product_name IS NOT NULL
)
,

int_table AS (
  SELECT *
  FROM (
    SELECT *, TIMESTAMP_DIFF(event_timestamp, prev_event_timestamp, SECOND) AS time_between_picks
    FROM (
      SELECT *, LAG(event_timestamp) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS prev_event_timestamp
      FROM (
        SELECT *, ROW_NUMBER() OVER (PARTITION BY order_id,product_id ORDER BY event_timestamp DESC) AS rnk
        FROM tbl 
        WHERE 1=1
      --   AND order_id = '60921084'
        AND (prev_screen_name = 'Product Scanner'
        AND screen_name = 'Picking List')
        OR (event_seq = 1 AND screen_name = 'Picking List' AND event_action = 'Landed')
        # ORDER BY order_id,event_timestamp
      )
      WHERE rnk = 1
    UNION ALL
    SELECT *,LAG(event_timestamp) OVER (PARTITION BY order_id ORDER BY event_timestamp) AS prev_event_timestamp
    FROM (
      SELECT TIMESTAMP_ADD(event_timestamp, INTERVAL 8 HOUR) AS event_timestamp, picker_id, batch_id, order_id, store_id, order_pick_type, product_id, product_name,screen_name,event_action,event_label,product_location,CAST(null AS STRING) as prev_screen_name, product_count, product_qty, version,rnk, event_seq
      FROM (
        SELECT *, 
           ROW_NUMBER() OVER (PARTITION BY order_id,product_id ORDER BY event_timestamp ASC) AS rnk
        FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY order_id, screen_name, event_action ORDER BY event_timestamp ASC) AS event_seq FROM `fairprice-bigquery.fpon_analytics.dim_picker_app_custom_event_bq`)
        WHERE CAST(replace(version,'.','') AS INT64) >= 503
        AND (event_label = 'Proceed'
        AND order_id IS NOT NULL)
        AND order_pick_type = 'Single'
        OR (event_seq = 1 AND screen_name = 'Picking List' AND event_action = 'Landed')
        -- AND store_id IS NOT NULL
        -- AND order_id IN ('60158203','61179637')
        # ORDER BY order_id, event_timestamp
        )
      WHERE rnk = 1
      # ORDER BY order_id, event_timestamp
      )
    # ORDER BY order_id,event_timestamp
    )
  --   WHERE prev_event_timestamp IS NOT NULL
  )
  UNION ALL 
  SELECT *
  FROM multi 
  -- ORDER BY event_timestamp DESC
)
,

# SELECT *
# FROM int_table

final_tbl AS (
  SELECT event_timestamp, order_pick_type, picker_id, batch_id, a.order_id, 
         COALESCE(b.store_client_id
         ,c.client_store_id
         ,a.store_id
         ) AS store_id,
         COALESCE(b.store_name
         ,c.name
         ) AS store_name, 
         --product_id--
         a.product_id, 
         LAG(a.product_id) OVER (PARTITION BY version,COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_product_id,
         CONCAT(LAG(a.product_id) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), '-', a.product_id) AS id_to_id,
         --product_name--
         a.product_name, 
         LAG(a.product_name) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_product_name,
         CONCAT(LAG(a.product_name) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), ' - ', a.product_name) AS name_to_name,
         --department--
         COALESCE(sap_department
         ,department_desc
         ) AS sap_department, 
         LAG(COALESCE(sap_department,department_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_dept,
         CONCAT(LAG(COALESCE(sap_department,department_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), ' - ', COALESCE(sap_department,department_desc)) AS dept_to_dept,
         --class--
         COALESCE(sap_class
         ,class_desc
         ) AS sap_class,
         LAG(COALESCE(sap_class,class_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_class,
         CONCAT(LAG(COALESCE(sap_class,class_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), ' - ', COALESCE(sap_class,class_desc)) AS class_to_class,
         --subclass--
         COALESCE(sap_subclass
         ,subclass_desc
         ) AS sap_subclass,
         LAG(COALESCE(sap_subclass,subclass_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_subclass,
         CONCAT(LAG(COALESCE(sap_subclass,subclass_desc)) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), ' - ', COALESCE(sap_subclass,subclass_desc)) AS subclass_to_subclass,
         --product location--
         product_location, 
         LAG(product_location) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp) AS prev_location,
         CONCAT(LAG(product_location) OVER (PARTITION BY version, COALESCE(a.order_id,batch_id) ORDER BY a.event_timestamp), ' - ', product_location) AS location_to_location,
         time_between_picks, 
         product_count, product_qty,
        #  percentile_disc(time_between_picks,0.5) OVER () AS median_time_between_picks_all_stores,
        #  AVG(time_between_picks) OVER() AS average_time_between_picks_all_stores,
        #  percentile_disc(time_between_picks,0.5) OVER (PARTITION BY store_client_id) AS median_time_between_picks_by_store,
        #  AVG(time_between_picks) OVER(PARTITION BY store_client_id) AS average_time_between_picks_by_store,
         version, 
         date_of_delivery, date_of_order
  -- SELECT *
  FROM int_table a
  LEFT JOIN (SELECT DISTINCT order_ref_number, product_sku, date_of_delivery,date_of_order,store_client_id, store_name, 
                            sap_department, sap_class, sap_subclass
             FROM fpon_cdm.fpon_cdm_sales_line
              WHERE order_ref_number IS NOT NULL 
              AND product_sku IS NOT NULL ) b
  ON a.order_id = b.order_ref_number
  AND a.product_id = b.product_sku
  LEFT JOIN (SELECT * 
             FROM `fairprice-bigquery.fpon_cdm.dim_fpon_store_meta_df` 
             WHERE Store_type IN ('FFS','PFC')
             AND refresh_date = CURRENT_DATE) c
  ON a.store_id = c.client_store_id
  LEFT JOIN (SELECT * FROM `fairprice-bigquery.fpon_cdm.fpon_cdm_product` WHERE deleted_at IS NULL) d
  ON a.product_id = CAST(d.client_item_id AS STRING)
  # ORDER BY a.order_id, event_timestamp
)

# SELECT *
# # SELECT batch_order_id, product_id, COUNT(*)
# from (
# select *,COALESCE(order_id, batch_id) AS batch_order_id
# FROM final_tbl
# # WHERE COALESCE(order_id, batch_id) = '62704070'
# # AND product_id = '13082985'
# )
# # GROUP BY batch_order_id,product_id
# # HAVING COUNT(*) > 1


,
ninety_fifth_percentile AS (
  SELECT COALESCE(batch_id, order_id) AS batch_order_id, product_id, percentile_disc(time_between_picks,0.95) OVER () AS ninety_fifth_percentile
  FROM final_tbl
)

SELECT a.*, 
        CASE
          WHEN a.store_id IN ('098','98') THEN 'Dark Store'
          WHEN a.store_id IN ('476','330','461','469') THEN 'Hyper'
          ELSE 'Super'
        END AS format,
       CASE 
        WHEN time_between_picks >= ninety_fifth_percentile THEN CONCAT('>=' ,CAST(ninety_fifth_percentile AS STRING))
        WHEN ninety_fifth_percentile < CEILING(time_between_picks/10) * 10 THEN CONCAT(CAST(FLOOR(time_between_picks/10) * 10 AS STRING), '-' , CAST(ninety_fifth_percentile - 1 AS STRING))
        WHEN MOD(time_between_picks, 10) = 0 THEN CONCAT(time_between_picks, '-', time_between_picks + 9)
        ELSE CONCAT(CAST(FLOOR(time_between_picks/10) * 10 AS STRING), '-' ,CAST((CEILING(time_between_picks/10) * 10) - 1 AS STRING)) 
       END AS bin,
       CASE 
        WHEN time_between_picks >= ninety_fifth_percentile THEN ninety_fifth_percentile
        WHEN ninety_fifth_percentile < CEILING(time_between_picks/10) * 10 THEN FLOOR(time_between_picks/10) * 10
        ELSE FLOOR(time_between_picks/10) * 10 
       END AS minimum,
       b.year, b.week,
       cast(concat(cast(b.year as string),case when week < 10 then '0' else '' end ,cast(b.week as string)) as int64) as year_week
FROM final_tbl a
JOIN fpon_cdm.calendar_full b
ON DATE(a.event_timestamp) = b.date
JOIN ninety_fifth_percentile c 
ON COALESCE(a.order_id, a.batch_id) = c.batch_order_id 
AND a.product_id = c.product_id
WHERE time_between_picks IS NOT NULL
AND store_name IS NOT NULL
# AND order_id = '66728821'
# ORDER BY event_timestamp ASC
)