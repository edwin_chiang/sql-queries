CREATE OR REPLACE TABLE fpon_analytics.potential_abusive_customers AS (
with sales_line AS (
  SELECT order_ref_number
      , order_status
      , customer_id
      , date_of_order
      , date_of_delivery
      , order_type
      , order_ship_mode_level2
      , product_sku
      , product_name
      , product_uom
      , order_item_ordered_amount
      , ordered_qty_with_normalised_weighted_items
      , delivered_qty_with_normalised_weighted_items
      , SUM(ordered_qty_with_normalised_weighted_items) OVER (PARTITION BY date_of_order, customer_id, product_sku, order_ref_number) AS sku_ordered_qty
      , SUM(ordered_qty_with_normalised_weighted_items) OVER (PARTITION BY date_of_order, customer_id, sap_department) AS ordered_qty_from_same_dept
      , SUM(ordered_qty_with_normalised_weighted_items) OVER (PARTITION BY date_of_order, customer_id, sap_class) AS ordered_qty_from_same_class
      , SUM(ordered_qty_with_normalised_weighted_items) OVER (PARTITION BY date_of_order, customer_id, sap_class) AS ordered_qty_from_same_subclass
      , COUNT(DISTINCT order_ref_number) OVER (PARTITION BY date_of_order, customer_id) AS no_of_orders_in_a_day
      , sap_department	
      , sap_class
      , sap_subclass
      , store_name
  FROM fpon_cdm.fpon_cdm_sales_line 
  WHERE date_of_order >= '2021-04-01'
  AND order_status <> 'CANCELLED'
  AND order_type <> 'OFFLINE'
)

SELECT date_of_order
    , customer_id
    , order_ref_number
    , order_type
    , order_status
    , store_name
    , product_sku
    , product_name
    , product_uom
    , sap_department
    , sap_class
    , sap_subclass
    , order_item_ordered_amount
    , sku_ordered_qty
    , ordered_qty_from_same_dept
    , ordered_qty_from_same_class
    , ordered_qty_from_same_subclass
    , no_of_orders_in_a_day
FROM sales_line
ORDER BY date_of_order
    , customer_id
    , sap_department
    , sap_class
    , sap_subclass
)