SELECT ROUND(AVG(order_pick_time),1) pick_time_per_order
    , ROUND(AVG(new_order_pick_time),1)  AS new_pick_time_per_order
    , ROUND(SUM(delivered_qty) / SUM(order_pick_time/60),1) AS uph
    , ROUND(SUM(delivered_qty) / SUM(new_order_pick_time/60),1) AS new_uph
FROM (
    SELECT a.*, delivered_qty
    FROM (
        SELECT batch_id,
                    SUM(CASE 
                    WHEN first_pick = 1 THEN DATETIME_DIFF(event_timestamp, start_ts, SECOND) 
                    WHEN last_pick = 1 THEN DATETIME_DIFF(finish_pick_ts, event_timestamp, SECOND) + time_between_picks
                    ELSE time_between_picks END) / 60 AS order_pick_time,
                    SUM(CASE 
                    WHEN first_pick = 1 THEN DATETIME_DIFF(event_timestamp, start_ts, SECOND) 
                    WHEN last_pick = 1 THEN DATETIME_DIFF(finish_pick_ts, event_timestamp, SECOND) + new_time_between_picks
                    ELSE new_time_between_picks END) / 60 AS new_order_pick_time
        FROM (SELECT DISTINCT a.batch_id,
--             b.order_ref_number,
            a.product_id,
            time_between_picks,
            new_time_between_picks,
            b.start_ts, 
            b.last_pick_ts, 
            b.finish_pick_ts,
            a.order_pick_type,
            DATETIME(a.event_timestamp) AS event_timestamp,
            ROW_NUMBER() OVER (PARTITION BY a.batch_id ORDER BY event_timestamp ASC) AS first_pick,
            ROW_NUMBER() OVER (PARTITION BY a.batch_id ORDER BY event_timestamp DESC) AS last_pick,
        FROM `fairprice-bigquery.fpon_analytics.ffs_picker_efficiency_impact_analysis_batch_picking` a
        LEFT JOIN (SELECT DISTINCT batch_id, start_ts, last_pick_ts, finish_pick_ts, CASE WHEN raw_order_id_list LIKE '%|%' THEN 'Multi' ELSE 'Single' END AS order_pick_type
                    FROM (SELECT *,SPLIT(REPLACE(raw_order_id_list,'|',','),',') split_order 
                          from fpon_analytics.dashboard_new_flow_performance_bq 
                          ) a
                    JOIN UNNEST(split_order) AS order_ref_number
        ) b
        ON CAST(a.batch_id AS INT64) = b.batch_id
        ORDER BY batch_id,event_timestamp ASC
        )
        GROUP BY batch_id
    ) a
    JOIN (SELECT batch_id, SUM(delivered_qty) AS delivered_qty 
          FROM `fairprice-bigquery.fpon_cdm.fpon_cdm_sales_head` a
          JOIN fpon_ods.zs_order_service_order_batches b
          ON a.order_id = b.order_id
          WHERE order_status <> 'CANCELLED'
          GROUP BY batch_id
          ORDER BY batch_id
    ) b
    ON a.batch_id = b.batch_id
    WHERE order_pick_time <> 0
)