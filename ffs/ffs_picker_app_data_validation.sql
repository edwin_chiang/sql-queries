CREATE OR REPLACE TABLE 
`fairprice-bigquery.fpon_analytics.ffs_picker_app_data_validation` AS 
(
    SELECT *, picked_orders/actual_orders AS perc_orders
FROM (
SELECT store_name,date_of_delivery, COUNT(DISTINCT order_ref_number) AS actual_orders, COUNT(DISTINCT order_id) AS picked_orders
FROM (SELECT DISTINCT a.store_name,a.date_of_delivery,a.order_ref_number,b.order_id, b.order_pick_type,b.version
FROM (SELECT * 
      FROM fpon_cdm.fpon_cdm_sales_head 
      WHERE order_status <> 'CANCELLED'
    #   AND order_payment_status NOT IN ('CANCELLED','FAILED') 
      AND order_type = 'DELIVERY'
      AND order_ship_mode = 'FFS'
    #   AND store_code = '469'
      AND date_of_delivery >= '2021-02-06'
    ) a
LEFT JOIN (SELECT * FROM `fairprice-bigquery.fpon_analytics.dim_picker_app_custom_event_bq` WHERE screen_name = 'Product Detail' AND event_label = 'Proceed' AND cast(replace(version,'.','') as int64) >= 503) b
ON a.order_ref_number = b.order_id)
GROUP BY date_of_delivery, store_name
ORDER BY date_of_delivery, store_name
)
WHERE date_of_delivery < CURRENT_DATE()
ORDER BY date_of_delivery, store_name
)