CREATE OR REPLACE TABLE fpon_analytics.uph_fp_retail_txn AS ( 
WITH store_txn AS (
  SELECT store_code, name, business_date as date, COUNT(distinct 'B'||business_date||'S'||store_code||'T'||till_code||'I'||invoice_no) AS txn_count
  FROM (
    SELECT a.*, name
    FROM `ne-fprt-data-cloud-production.fp.fp_sale_head` a
    JOIN (SELECT * FROM `fairprice-bigquery.fpon_cdm.dim_fpon_store_meta` WHERE store_type = 'FFS') b
    ON a.store_code = CAST(b.client_store_id AS INT64)
    WHERE invoice_date >= '2021-01-01' 
    AND invoice_date < current_date()
  )
  GROUP BY store_code,name, business_date
)
,

uph AS (
  SELECT picking_start_date
      , client_store_id AS store_code
      , name
      , SUM(total_delivered_quantity) AS total_delivered_qty
      , SUM(picking_packing_time_hr) AS pick_pack_time
      , SUM(picking_time_hr) AS pick_time
      , SUM(total_delivered_quantity) / SUM(picking_packing_time_hr) AS uph
      , SUM(total_delivered_quantity) / SUM(picking_time_hr) AS uph_pick
  FROM (
    SELECT a.client_store_id
        , b.name
        , raw_order_id_list
        , picking_start_date
        , total_delivered_quantity
        , picking_time_hr
        , packing_time_hr
        , picking_packing_time_hr
    FROM fpon_analytics.dashboard_new_flow_performance_bq a
    JOIN (SELECT * FROM fpon_cdm.dim_fpon_store_meta WHERE name LIKE '%FFS%') b
    ON a.client_store_id = CAST(b.client_store_id AS INT64)
  )
  GROUP BY picking_start_date, name, client_store_id
  ORDER BY picking_start_date, name, client_store_id
)

SELECT b.*
    , txn_count
FROM store_txn a
LEFT JOIN uph b
ON b.picking_start_date = a.date
AND b.store_code = a.store_code
)