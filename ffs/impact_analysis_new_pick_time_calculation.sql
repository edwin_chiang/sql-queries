SELECT ROUND(AVG(order_pick_time),1) pick_time_per_order
    , ROUND(AVG(new_order_pick_time),1)  AS new_pick_time_per_order
    , ROUND(SUM(delivered_qty) / SUM(order_pick_time/60),1) AS uph
    , ROUND(SUM(delivered_qty) / SUM(new_order_pick_time/60),1) AS new_uph
FROM (
    SELECT a.*, delivered_qty
    FROM (
        SELECT order_id,
                    SUM(CASE 
                    WHEN first_pick = 1 THEN DATETIME_DIFF(event_timestamp, start_ts, SECOND) 
                    WHEN last_pick = 1 THEN DATETIME_DIFF(finish_pick_ts, event_timestamp, SECOND) + time_between_picks
                    ELSE time_between_picks END) / 60 AS order_pick_time,
                    SUM(CASE 
                    WHEN first_pick = 1 THEN DATETIME_DIFF(event_timestamp, start_ts, SECOND) 
                    WHEN last_pick = 1 THEN DATETIME_DIFF(finish_pick_ts, event_timestamp, SECOND) + new_time_between_picks
                    ELSE new_time_between_picks END) / 60 AS new_order_pick_time
        FROM (SELECT a.order_id,
            a.product_id,
            time_between_picks,
            new_time_between_picks,
            c.start_ts, 
            c.last_pick_ts, 
            c.finish_pick_ts,
            DATETIME(a.event_timestamp) AS event_timestamp,
            ROW_NUMBER() OVER (PARTITION BY a.order_id ORDER BY event_timestamp ASC) AS first_pick,
            ROW_NUMBER() OVER (PARTITION BY a.order_id ORDER BY event_timestamp DESC) AS last_pick,
        FROM `fairprice-bigquery.fpon_analytics.ffs_picker_efficiency_impact_analysis` a
        LEFT JOIN `fairprice-bigquery.fpon_cdm.fpon_cdm_sales_head` b
        ON CAST(a.order_id AS INT64) = CAST(b.order_ref_number AS INT64)
--         LEFT JOIN (SELECT * FROM `fairprice-bigquery.zopsmart.zs_order_service_order_process_mapping` WHERE process_name = 'PICKING' AND status = 'SUCCESS') c
        LEFT JOIN (SELECT order_ref_number, start_ts, last_pick_ts, finish_pick_ts, CASE WHEN raw_order_id_list LIKE '%|%' THEN 'Multi' ELSE 'Single' END AS order_pick_type
FROM (SELECT *,SPLIT(REPLACE(raw_order_id_list,'|',','),',') split_order 
      from fpon_analytics.dashboard_new_flow_performance_bq 
      )
JOIN UNNEST(split_order) AS order_ref_number
) c
        ON b.order_ref_number = c.order_ref_number
        # WHERE a.order_id = '65159698'
        ORDER BY event_timestamp ASC
        )
        GROUP BY order_id
    ) a
    JOIN (SELECT order_ref_number, SUM(delivered_qty) AS delivered_qty 
        FROM `fairprice-bigquery.fpon_cdm.fpon_cdm_sales_line`
        WHERE order_item_status <> 'CANCELLED'
        GROUP BY order_ref_number
    ) b
    ON a.order_id = b.order_ref_number
    WHERE order_pick_time <> 0
)
