SELECT sum(case when time_between_picks >= 40 then 1 else 0 end) / count(*), avg(time_between_picks)
-- select order_id, count(*) as no_of_skus
FROM fpon_analytics.ffs_picker_efficiency_impact_analysis
WHERE order_id IN (SELECT order_id
                  FROM fpon_analytics.ffs_picker_efficiency_impact_analysis
                  GROUP BY order_id
                  HAVING COUNT(*) <= 15)