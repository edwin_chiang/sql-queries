SELECT sum(case when time_between_picks >= 40 then 1 else 0 end) / count(*) as picks_above_40_perc
    , sum(case when new_time_between_picks >= 40 then 1 else 0 end) / count(*) as picks_above_40_perc_new
    , avg(time_between_picks) as avg_time_between_picks
    , avg(new_time_between_picks) as new_avg
-- select order_id, count(*) as no_of_skus
-- SELECT distinct batch_id
FROM fpon_analytics.ffs_picker_efficiency_impact_analysis_batch_picking
WHERE batch_id NOT IN ('427369')
-- AND batch_id IN (SELECT batch_id
--                   FROM fpon_analytics.ffs_picker_efficiency_impact_analysis
--                   WHERE time_between_picks <> 0 
--                   GROUP BY order_id
--                   HAVING COUNT(*) >= 31 )