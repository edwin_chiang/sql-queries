CREATE OR REPLACE TABLE fpon_analytics.ffs_picker_efficiency_impact_analysis_batch_picking AS (
  with sample_size AS (
      SELECT *,
          PERCENT_RANK() OVER(ORDER BY time_between_picks ASC) AS percentile,
          CUME_DIST() OVER(ORDER BY time_between_picks ASC) AS cumulative_dist,
      FROM (
          SELECT *,
          PERCENTILE_CONT(time_between_picks,0.99) OVER() AS ninety_ninth_percentile,
          CASE WHEN time_between_picks > 20 THEN 1 ELSE 0 END AS more_than_20
          FROM `fairprice-bigquery.fpon_analytics.ffs_picker_efficiency`
          WHERE 1=1
          # AND prev_subclass = sap_subclass
          AND store_id IN ('461')
          AND order_pick_type = 'Multi'
          AND year_week BETWEEN 202110 AND 202113
      )
      WHERE 1=1
      AND time_between_picks < ninety_ninth_percentile
      AND more_than_20 = 1
  )
  ,

  final_tbl AS (
      SELECT *
          EXCEPT(percentile, cumulative_dist)
          ,(1 - (0.50 * cumulative_dist)) * time_between_picks AS new_time_between_picks
      FROM sample_size
      UNION ALL
      SELECT *,
          time_between_picks AS new_time_between_picks
      FROM (
          SELECT *,
          PERCENTILE_CONT(time_between_picks,0.99) OVER() AS ninety_ninth_percentile,
          CASE WHEN time_between_picks > 20 THEN 1 ELSE 0 END AS more_than_20
          FROM `fairprice-bigquery.fpon_analytics.ffs_picker_efficiency`
          WHERE 1=1
          # AND prev_subclass = sap_subclass
          AND store_id IN ('461')
          AND order_pick_type = 'Multi'
          AND year_week BETWEEN 202110 AND 202113
      )
      WHERE 1=1
      AND (time_between_picks <= 20 OR time_between_picks >= ninety_ninth_percentile)
      # AND more_than_20 = 0
  )

SELECT *
FROM final_tbl

)
