-- changed table source to sales_line
-- changed date_of_order or date_of_delivery for time when order is recognised within the month
-- 20210408 remove payment status filter
-- 20210510 change from delivered_qty to delivered_qty_with_normalised_weighted_items
-- 20210511 change source of soac from p&l to seng joo's table
-- 20210511 change storage cost naming to fixed cost per unit and change receiving and picking cost to variable cost per unit
-- 20210511 added mp commission as a cm1 component
-- 20210530 added stock writeoff for ffs (0.68% for fresh, 0.25% for non-fresh)
-- 20210530 allocate inbound log cost to only non-direct suppliers(From DC only) and include MP inbound cost
-- 20210531 changed calculation for vcpu and vcpu by taking numbers directly from p&l template instead of adding them up
-- 20210601 included MA
-- 20210603 remove passback margins
-- 20210611 fixed gp_cost double counting
-- 20210617 added 2019 sales data
-- 20210617 added claims for may 2021

CREATE OR REPLACE TABLE finance_ods.finance_master_table_new AS (
with profit_and_loss AS (
  SELECT *, 
      CASE 
        WHEN delivered_qty_dc = 0 THEN 0 
        ELSE inbound_logistic_cost / delivered_qty_dc 
      END AS inbound_logistics_cost_per_unit,
      CASE 
        WHEN delivered_qty_mp = 0 THEN 0 
        ELSE mp_inbound_logistics / delivered_qty_mp 
      END AS mp_inbound_logistics_cost_per_unit,
--        soac / delivered_qty AS soac_per_unit,
       cost_receiving / delivered_qty AS cost_receiving_per_unit,
       cost_picking / delivered_qty AS cost_picking_per_unit,
       cost_storage / delivered_qty AS cost_storage_per_unit,
       vcpu / delivered_qty AS variable_cost_per_unit,
       fcpu / delivered_qty AS fixed_cost_per_unit,
       additions_passback_margin / delivered_qty AS additions_passback_margin_per_unit
  FROM (SELECT CAST(a.year AS INT64) AS year, CAST(a.month AS INT64) AS month, a.order_ship_mode_level2, 
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(inbound_logistics_cost AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(inbound_logistics_cost AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS inbound_logistic_cost,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(soac AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(soac AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS soac,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(cost_receiving AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(cost_receiving AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS cost_receiving,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(cost_picking AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(cost_picking AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS cost_picking,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(cost_storage AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(cost_storage AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS cost_storage,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(additions_passback_margin AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(additions_passback_margin AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS additions_passback_margin,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(mp_inbound_logistics AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(mp_inbound_logistics AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS mp_inbound_logistics,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(vcpo AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(vcpo AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS vcpo,        
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(vcpu AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(vcpu AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS vcpu,
        COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(fcpu AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(fcpu AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS fcpu,
        delivered_qty,
        delivered_qty_dc,
        delivered_qty_mp
        FROM finance_ods.profit_and_loss a
        JOIN (SELECT natural_year as year
                  , month
                  , order_ship_mode_level2
                  , SUM(CASE WHEN source_supply.source_supply LIKE '__' THEN delivered_qty_with_normalised_weighted_items ELSE 0 END) AS delivered_qty_dc
                  , SUM(delivered_qty_with_normalised_weighted_items) AS delivered_qty
                  , SUM(CASE WHEN product_sku LIKE '900%' THEN delivered_qty_with_normalised_weighted_items ELSE 0 END) AS delivered_qty_mp
              FROM fpon_cdm.fpon_cdm_sales_line a
              LEFT JOIN `ne-fprt-data-cloud-production.fp.fp_source_of_supply` source_supply
              ON source_supply.product_code = a.product_sku
              AND source_supply.store_code = a.store_client_id
              JOIN fpon_cdm.calendar_full b
              ON a.date_of_delivery = b.date
              WHERE 1=1
              # WHERE payment_status NOT IN ('FAILED','CANCELLED')
              AND order_item_status <> 'CANCELLED'
              AND order_status <> 'CANCELLED'
              AND (order_ship_mode NOT IN ('OFFLINE','CNC') OR order_ship_mode IS NULL)
              AND order_type NOT IN ('OFFLINE', 'PICKUP')
              AND date_of_delivery >= '2020-01-01'
              GROUP BY natural_year, month,order_ship_mode_level2
             ) b
        ON CAST(a.year AS INT64) = b.year 
        AND CAST(a.month AS INT64) = b.month 
        AND a.order_ship_mode_level2 = b.order_ship_mode_level2
  )
),

product AS (
  select * from fpon_cdm.fpon_cdm_product where deleted_at is null
),

tbl AS ( 
  SELECT COALESCE(a.year,d.year) AS year,
         COALESCE(a.month,d.month) AS month, 
         cast(concat(cast(COALESCE(a.year,d.year) as string),case when COALESCE(a.month,d.month) < 10 then '0' else '' end ,cast(COALESCE(a.month,d.month) as string)) as int64) as year_month,
         product_sku, 
         ea_sku,
         b.product_name,
         b.department_desc AS sap_department,
         b.class_desc AS sap_class,
         b.subclass_desc AS sap_subclass,
         b.product_l1_category_name AS l1_category,
         b.product_l2_category_name AS l2_category,
         b.product_l3_category_name AS l3_category,
         b.brand_name AS product_brand_name,
         c.vendor_name AS supplier_name,
         COALESCE(c.vendor,d.vendor_code) AS vendor_code, 
         CASE 
           WHEN c.brand_desc IN ('AMMERLAND','ASPIRE','BEGA-','BEST CBD','BUDGET','CHEERS','CHEF','CHEF DELG','CHEFS PORK','CHENG','CLIPS','DOUBLE FP','FAIRPRICE','FAIRPRICEG','FORTUNE-','FP','FP MARTENS','FP-PASAR','FRESH','GLDN CHEF','GR/PAGODA','HARVEST F','HOMEPROUD','IN-STORE','INDULGENZ','JUST WINE','K.NAKARA','KIKY','LIFE','MALTAGLATI','MI E-ZEE','NATURE\'S B','OCEAN F D.','OF DELITE','OLAM SUGAR','OLITA','ORI','PASAR','PASAR ORG','PORKEE','PREMIER-','PULPPY','ROYALFEAST','S FINEST','S ORGANIC','SAFYA','SIAM PURE','SIMPLY-','SMTCHOICE','SSG','SUN BURST','TREATS','VALUE FRSH','VENUS&MARS','WHC','WILMAR MSM','XIANGXUE') OR 
                                 (c.brand_desc = 'OCEANFRESH' AND LOWER(c.article_desc) NOT LIKE '%abalone%') THEN 'OB'
           ELSE 'NB' 
         END AS is_house_brand,
         c.brand_desc,
         b.unit_of_measurement AS uom,
         order_ship_mode_level2,
         store_client_id,
         store_name,
         source_supply,
         delivered_qty,
         ordered_qty,
        -----------------------------------
        ---------------TOTAL---------------
        -----------------------------------
         gmv_no_gst_bef_promo_disc,
         gmv_no_gst_aft_promo_disc,
         cos,
         gp_margin_no_gst_bef_promo_disc,
         gp_margin_no_gst_aft_promo_disc,
         claims,
         additions_MA,
         CASE 
            WHEN order_ship_mode_level2 <> 'FFS Normal' THEN cost_writeoffs
            WHEN order_ship_mode_level2 = 'FFS Normal' 
              AND b.department_desc in ('FRESH FOOD') 
              AND b.class_desc IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES') 
              AND b.subclass_desc NOT LIKE '%FROZEN%' 
              AND b.subclass_desc NOT LIKE '%SURIMI%' 
              AND b.subclass_desc NOT LIKE '%PROCESSED%'
              AND b.subclass_desc NOT LIKE '%PRESERVED%'
              AND b.subclass_desc NOT LIKE '%DUMMY%'
              AND b.subclass_desc NOT LIKE '%PLANTS%' THEN 0.0068 * gmv_no_gst_aft_promo_disc
            ELSE 0.0025 * gmv_no_gst_aft_promo_disc
           END AS cost_writeoffs,
         inbound_logistics_cost,
         mp_commission,
         COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) * delivered_qty,soac,0) AS additions_other_income,
         gp_margin_no_gst_bef_promo_disc + claims + additions_MA - cost_writeoffs - inbound_logistics_cost + COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) * delivered_qty,soac,0) + mp_commission AS cm1_profit,
         variable_cost,
         fixed_cost,
--          additions_passback_margin,
         cost_other_charges_bc,
         cost_cust_svc,
         gmv_no_gst_bef_promo_disc 
         - cos 
         - variable_cost
         - fixed_cost
         + additions_MA 
         + claims 
         + COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) * delivered_qty,soac,0) 
         - inbound_logistics_cost 
         + mp_commission 
         -  CASE 
            WHEN order_ship_mode_level2 <> 'FFS Normal' THEN cost_writeoffs
            WHEN order_ship_mode_level2 = 'FFS Normal' 
              AND b.department_desc in ('FRESH FOOD') 
              AND b.class_desc IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES') 
              AND b.subclass_desc NOT LIKE '%FROZEN%' 
              AND b.subclass_desc NOT LIKE '%SURIMI%' 
              AND b.subclass_desc NOT LIKE '%PROCESSED%'
              AND b.subclass_desc NOT LIKE '%PRESERVED%'
              AND b.subclass_desc NOT LIKE '%DUMMY%'
              AND b.subclass_desc NOT LIKE '%PLANTS%' THEN 0.0068 * gmv_no_gst_aft_promo_disc
            ELSE 0.0025 * gmv_no_gst_aft_promo_disc
           END 
         - cost_other_charges_bc 
         - cost_cust_svc 
--          + additions_passback_margin 
         AS cm2_profit,
              -----------------------------------
              --------------PER UNIT-------------
              -----------------------------------         
         unit_price_no_gst,
         avg_selling_price,
         cos_per_unit,
         gp_margin_per_unit_bef_disc,
         gp_margin_per_unit_aft_disc, 
         claims_per_unit,
         additions_MA_per_unit,
         CASE 
            WHEN order_ship_mode_level2 <> 'FFS Normal' THEN cost_writeoffs_per_unit_actual
            WHEN order_ship_mode_level2 = 'FFS Normal' 
              AND b.department_desc in ('FRESH FOOD') 
              AND b.class_desc IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES') 
              AND b.subclass_desc NOT LIKE '%FROZEN%' 
              AND b.subclass_desc NOT LIKE '%SURIMI%' 
              AND b.subclass_desc NOT LIKE '%PROCESSED%'
              AND b.subclass_desc NOT LIKE '%PRESERVED%'
              AND b.subclass_desc NOT LIKE '%DUMMY%'
              AND b.subclass_desc NOT LIKE '%PLANTS%' THEN 0.0068 * avg_selling_price
            ELSE 0.0025 * avg_selling_price
           END AS cost_writeoffs_per_unit_actual,
         inbound_logistics_cost_per_unit,
         mp_commission_per_unit,
         COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) ,soac,0) AS additions_other_income_per_unit,
         gp_margin_per_unit_bef_disc + claims_per_unit + additions_MA_per_unit - cost_writeoffs_per_unit_actual - 
         inbound_logistics_cost_per_unit + COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) ,soac,0) + mp_commission_per_unit AS cm1_profit_per_unit,
         variable_cost_per_unit,
         fixed_cost_per_unit,
--          additions_passback_margin_per_unit,
         cost_other_charges_bc_per_unit,
         cost_cust_svc_per_unit,
         unit_price_no_gst 
         - cos_per_unit 
         - variable_cost_per_unit
         - fixed_cost_per_unit
         + additions_MA_per_unit 
         + claims_per_unit 
         + COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY c.vendor, a.year, a.month)) ,soac,0) 
         - inbound_logistics_cost_per_unit 
         + mp_commission_per_unit 
         - CASE 
            WHEN order_ship_mode_level2 <> 'FFS Normal' THEN cost_writeoffs_per_unit_actual
            WHEN order_ship_mode_level2 = 'FFS Normal' 
              AND b.department_desc in ('FRESH FOOD') 
              AND b.class_desc IN ('PORK', 'POULTRY', 'OTHER FRESH MEAT', 'FRESH FISH/ SEAFOOD', 'TROPICAL FRUITS', 'EXOTIC FRUITS', 'TEMPERATE FRUITS', 'VEGETABLES') 
              AND b.subclass_desc NOT LIKE '%FROZEN%' 
              AND b.subclass_desc NOT LIKE '%SURIMI%' 
              AND b.subclass_desc NOT LIKE '%PROCESSED%'
              AND b.subclass_desc NOT LIKE '%PRESERVED%'
              AND b.subclass_desc NOT LIKE '%DUMMY%'
              AND b.subclass_desc NOT LIKE '%PLANTS%' THEN 0.0068 * avg_selling_price
            ELSE 0.0025 * avg_selling_price
           END 
         - cost_cust_svc_per_unit 
         - cost_other_charges_bc_per_unit 
--          + additions_passback_margin_per_unit
         AS cm2_profit_per_unit,
         CURRENT_DATE() as run_date
  FROM (
    SELECT COALESCE(a.natural_year,d.year,e.year) AS year , 
           COALESCE(a.month,d.month,e.month) AS month,
           COALESCE(CAST(a.product_sku AS INT64),CAST(d.material AS INT64)) AS product_sku, 
           COALESCE(CAST(a.ea_sku AS INT64),CAST(e.material AS INT64)) AS ea_sku,
           COALESCE(CASE WHEN a.store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE a.order_ship_mode_level2 END,d.order_ship_mode_level2) AS order_ship_mode_level2,
           COALESCE(store_client_id, CASE WHEN d.order_ship_mode_level2 = 'PFC_DELIVERY' THEN '004' WHEN d.order_ship_mode_level2 = 'FFS Darkstore' THEN '098' END, e.store_code) AS store_client_id,
           COALESCE(a.store_name,CASE WHEN d.order_ship_mode_level2 = 'PFC_DELIVERY' THEN 'Fairprice PFC' WHEN d.order_ship_mode_level2 = 'FFS Darkstore' THEN 'Orchid Country Club (DS)' END, e.store_name) AS store_name,
           source_supply.source_supply,
              -----------------------------------
              ---------------TOTAL---------------
              -----------------------------------
           COALESCE(SUM(a.delivered_qty_with_normalised_weighted_items),0) AS delivered_qty,
           COALESCE(SUM(a.ordered_qty_with_normalised_weighted_items),0) AS ordered_qty,
           SUM(COALESCE(unit_price * a.delivered_qty_with_normalised_weighted_items,0)) / 1.07 AS gmv_no_gst_bef_promo_disc,
           SUM(COALESCE(order_item_invoiced_amount,0))/1.07 AS gmv_no_gst_aft_promo_disc,
           CASE 
            WHEN CAST(a.product_sku AS STRING) LIKE '900%' THEN SUM(COALESCE(order_item_invoiced_amount,0))/1.07
            ELSE SUM(COALESCE(gp_cost, 0)) 
           END AS cos,      
           SUM((COALESCE(unit_price*a.delivered_qty_with_normalised_weighted_items,0))/1.07) - COALESCE(SUM(gp_cost),0) AS gp_margin_no_gst_bef_promo_disc,
           SUM(COALESCE(order_item_invoiced_amount,0)/1.07)- COALESCE(SUM(gp_cost),0) AS gp_margin_no_gst_aft_promo_disc,
           SUM(COALESCE(claims, 0)) AS claims,
           CASE 
            WHEN source_supply.source_supply NOT LIKE '__' THEN 0 
            WHEN a.product_sku LIKE '900%' THEN COALESCE(SUM(c.mp_inbound_logistics_cost_per_unit * a.delivered_qty_with_normalised_weighted_items),0) 
            ELSE COALESCE(SUM(c.inbound_logistics_cost_per_unit * a.delivered_qty_with_normalised_weighted_items),0)             
           END AS inbound_logistics_cost,
           COALESCE(SUM(a.mp_commission), 0) AS mp_commission,
           COALESCE(SUM(c.cost_receiving_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS cost_receiving,
           COALESCE(SUM(c.cost_picking_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS cost_picking,
           COALESCE(SUM(c.cost_storage_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS cost_storage,
           COALESCE(SUM(c.variable_cost_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS variable_cost,
           COALESCE(SUM(c.fixed_cost_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS fixed_cost,
--            COALESCE(SUM(c.additions_passback_margin_per_unit * a.delivered_qty_with_normalised_weighted_items),0) AS additions_passback_margin,
           COALESCE(- writeoffs,0) AS cost_writeoffs,
           COALESCE(SUM(ma_amt), 0) AS additions_MA,
           SUM(COALESCE(order_item_invoiced_amount,0))/1.07 * 0.010 AS cost_other_charges_bc,
           SUM(COALESCE(order_item_invoiced_amount,0))/1.07 * 0.006 AS cost_cust_svc,
              -----------------------------------
              --------------PER UNIT-------------
              -----------------------------------
           COALESCE(AVG(unit_price/1.07),0) AS unit_price_no_gst,
           COALESCE(SAFE_DIVIDE((SUM(order_item_invoiced_amount)/ 1.07),SUM(a.delivered_qty_with_normalised_weighted_items)),0) AS avg_selling_price,
           CASE 
            WHEN CAST(a.product_sku AS STRING) LIKE '900%' THEN COALESCE(SAFE_DIVIDE((SUM(order_item_invoiced_amount)/ 1.07),SUM(a.delivered_qty_with_normalised_weighted_items)),0)
            ELSE COALESCE(SAFE_DIVIDE(SUM(gp_cost),SUM(a.delivered_qty_with_normalised_weighted_items)),0) 
           END AS cos_per_unit,
           COALESCE(AVG(unit_price/1.07),0) - COALESCE(SAFE_DIVIDE(SUM(gp_cost),SUM(a.delivered_qty_with_normalised_weighted_items)), 0) AS gp_margin_per_unit_bef_disc,
           COALESCE(SAFE_DIVIDE((SUM(order_item_invoiced_amount)/ 1.07),SUM(a.delivered_qty_with_normalised_weighted_items)),0) - COALESCE(SAFE_DIVIDE(SUM(gp_cost),SUM(a.delivered_qty_with_normalised_weighted_items)),0) AS gp_margin_per_unit_aft_disc,
           COALESCE(SAFE_DIVIDE(SUM(claims), SUM(a.delivered_qty_with_normalised_weighted_items)),0) AS claims_per_unit,
           CASE 
            WHEN source_supply.source_supply NOT LIKE '__' THEN 0 
            WHEN a.product_sku LIKE '900%' THEN COALESCE(c.mp_inbound_logistics_cost_per_unit,0) 
            ELSE COALESCE(c.inbound_logistics_cost_per_unit,0) 
           END AS inbound_logistics_cost_per_unit,
           COALESCE(SUM(mp_commission_per_unit),0) AS mp_commission_per_unit,
           COALESCE(c.cost_receiving_per_unit,0) AS cost_receiving_per_unit,
           COALESCE(c.cost_picking_per_unit,0) AS cost_picking_per_unit,
           COALESCE(c.cost_storage_per_unit,0) AS cost_storage_per_unit,
           COALESCE(c.variable_cost_per_unit,0) AS variable_cost_per_unit,
           COALESCE(c.fixed_cost_per_unit,0) AS fixed_cost_per_unit,
--            COALESCE(c.additions_passback_margin_per_unit,0) AS additions_passback_margin_per_unit,
           COALESCE(SAFE_DIVIDE(-writeoffs , SUM(a.delivered_qty_with_normalised_weighted_items)),0) AS cost_writeoffs_per_unit_actual,
           COALESCE(SAFE_DIVIDE(SUM(ma_amt), SUM(a.delivered_qty_with_normalised_weighted_items)), 0) AS additions_MA_per_unit,
           COALESCE(SAFE_DIVIDE((SUM(order_item_invoiced_amount)/ 1.07),SUM(a.delivered_qty_with_normalised_weighted_items)),0) * 0.010 AS cost_other_charges_bc_per_unit,
           COALESCE(SAFE_DIVIDE((SUM(order_item_invoiced_amount)/ 1.07),SUM(a.delivered_qty_with_normalised_weighted_items)),0) * 0.006 AS cost_cust_svc_per_unit
    FROM (SELECT natural_year
              , month
              , cast(concat(cast(natural_year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64) as year_month
              , a.product_sku
              , a.sap_unit_of_measurement
              , a.ea_sku
              , store_client_id
              , store_name
              , CASE 
                  WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'FFS' THEN 'FFS Normal' 
                  WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'BULK' THEN 'PFC_DELIVERY'
                  ELSE COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) 
                END AS order_ship_mode_level2
              , SUM(unit_price) / COUNT(*) AS unit_price
              , SUM(COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty)) AS delivered_qty_with_normalised_weighted_items
              , SUM(COALESCE(ordered_qty_with_normalised_weighted_items, ordered_qty)) AS ordered_qty_with_normalised_weighted_items
              , SUM(order_item_invoiced_amount) AS order_item_invoiced_amount
              , SUM(COALESCE(cost_of_sales.cos_per_unit, 0) * COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty,0)) AS gp_cost
              , SUM(gp_cost) AS old_cos
              , SUM(COALESCE(mp_commission.mp_cos_per_unit,0) * COALESCE(a.delivered_qty_with_normalised_weighted_items, delivered_qty, 0)) AS mp_cos
              , SUM(COALESCE(mp_cos_per_unit,0)) AS mp_cos_per_unit
              , SUM(COALESCE(commission_per_unit,0) * COALESCE(a.delivered_qty_with_normalised_weighted_items, delivered_qty,0)) AS mp_commission
              , SUM(COALESCE(commission_per_unit,0)) AS mp_commission_per_unit
          FROM (SELECT * 
                FROM fpon_cdm.fpon_cdm_sales_line 
                WHERE invoice_number <> 'INVTest'
                AND order_item_status <> 'CANCELLED'
                AND order_status <> 'CANCELLED'
                AND (order_ship_mode NOT IN ('OFFLINE','CNC') OR order_ship_mode IS NULL)
                AND order_type NOT IN ('OFFLINE', 'PICKUP')
                and date_of_delivery >= "2019-01-01" 
                AND store_name <> 'Boys Brigade (Donation)'
                AND source = 'ZopSmart'
                AND order_ship_mode <> 'BOY'
                
                UNION ALL
                
                SELECT *
                FROM fpon_cdm.fpon_cdm_sales_line
                WHERE invoice_number IS NOT NULL
                AND date_of_delivery >= '2019-01-01'
                and source = 'WCS'
                AND order_ship_mode <> 'BOY'
                ) a
          LEFT JOIN (
              SELECT *, COALESCE(ROUND(SAFE_DIVIDE(cost , billed_quantity),2),0) AS cos_per_unit
              FROM fpon_ods.sap_orderline_gp
                ) cost_of_sales
          ON CAST(a.invoice_number AS INT64) = CAST(cost_of_sales.bill_doc AS INT64)
          AND CAST(a.ea_sku AS INT64) = CAST(cost_of_sales.article AS INT64)
          AND CASE WHEN cost_of_sales.su IN ('EA') THEN 'EA' WHEN cost_of_sales.su IN ('CAR','KAR') THEN 'KAR' ELSE cost_of_sales.su END = a.sap_unit_of_measurement
          LEFT JOIN (
              select sap_barcode AS product_sku
                , rsp_with_gst
                , cost_price
                , commission / 100 AS commission_rate
                , round((rsp_with_gst / 1.07) * (100 -  commission) / 100,2) as mp_cos_per_unit
                , round((rsp_with_gst / 1.07) * commission / 100,2) as commission_per_unit
                , load_date
              from fpon_ods.sap_sku_extract
              where 1=1
              and regexp_contains(sap_barcode,'[0-9]') IS TRUE
                ) mp_commission
          ON a.product_sku = mp_commission.product_sku
          AND a.date_of_delivery = mp_commission.load_date
          JOIN fpon_cdm.calendar_full cal
          ON a.date_of_delivery = cal.date
          GROUP BY natural_year
              , month
              , product_sku
              , ea_sku
              , store_client_id
              , store_name
              , order_ship_mode_level2
              , sap_unit_of_measurement
          ) a
    LEFT JOIN profit_and_loss c
    ON a.natural_year = c.year AND a.month = c.month AND a.order_ship_mode_level2 = c.order_ship_mode_level2
    LEFT JOIN finance_ods.finance_ma ma
    ON a.year_month = CAST(ma.year_month AS INT64)
    AND CAST(a.product_sku AS INT64) = ma.product_sku
    AND a.store_client_id = ma.store_code
    LEFT JOIN `ne-fprt-data-cloud-production.fp.fp_source_of_supply` source_supply
    ON source_supply.product_code = a.product_sku
    AND source_supply.store_code = a.store_client_id
    FULL OUTER JOIN (SELECT * FROM fpon_ods.finance_writeoffs) d
    ON a.natural_year = d.year
    AND a.month = d.month
    AND CAST(a.product_sku AS INT64) = CAST(d.material AS INT64)
    AND a.order_ship_mode_level2 = d.order_ship_mode_level2
    FULL OUTER JOIN finance_ods.finance_claims e
    ON a.natural_year = e.year
    AND a.month = e.month
    AND CAST(a.ea_sku AS INT64) = CAST(e.material AS INT64)
    AND CASE WHEN e.uom IN ('EA') THEN 'EA' WHEN e.uom IN ('CAR','KAR') THEN 'KAR' ELSE e.uom END = a.sap_unit_of_measurement
    AND a.store_client_id = e.store_code
    GROUP BY COALESCE(a.natural_year,d.year,e.year)
        , COALESCE(CAST(a.ea_sku AS INT64),CAST(e.material AS INT64))
        , month
        , product_sku
        , a.product_sku
        , order_ship_mode_level2
        , store_client_id
        , store_name
        , c.inbound_logistics_cost_per_unit
        , c.cost_receiving_per_unit
        , c.cost_picking_per_unit
        , c.cost_storage_per_unit
        , c.variable_cost_per_unit
        , c.fixed_cost_per_unit
        , additions_passback_margin_per_unit
        , writeoffs
        , source_supply
        , mp_inbound_logistics_cost_per_unit
  ) a
  LEFT JOIN product b
  ON SAFE_CAST(a.product_sku AS int64) = SAFE_CAST(b.client_item_id AS int64)
  LEFT JOIN fpon_ods.cls_product c
  ON SAFE_CAST(b.client_item_id AS int64) = SAFE_CAST(c.article AS INT64)
  FULL OUTER JOIN (SELECT year, month, vendor_code, SUM(CAST(soac AS FLOAT64)) AS soac FROM finance_ods.soac_by_month WHERE month <= 12 GROUP BY year, month, vendor_code) d
  ON c.vendor = CAST(d.vendor_code AS INT64)
  AND a.year = CAST(d.year AS INT64)
  AND a.month = CAST(d.month AS INT64)
)

SELECT DISTINCT * EXCEPT(source_supply)
FROM tbl 
WHERE year_month <= cast(concat(EXTRACT(year FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)),case when EXTRACT(month FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) < 10 then '0' else '' end ,cast(EXTRACT(month FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) as string)) as int64)
-- AND vendor_code = CAST(source_supply AS INT64)
)

