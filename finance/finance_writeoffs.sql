CREATE OR REPLACE TABLE finance_ods.finance_writeoffs AS (

select extract(year from posting_date) as year, 
       extract(month from posting_date) as month,
       Material, 
       sum(writeoffs) as writeoffs,
       order_ship_mode_level2
from (
select posting_date, Material, 
      sum( Amt_in_loc_cur_ ) as writeoffs, 
    'PFC_DELIVERY' AS order_ship_mode_level2
  from fpon_analytics.writeoff_test a
  where 1=1
  and cost_center='F004'
  group by posting_date, material
  
UNION ALL

select posting_date, Material, 
      sum( Amt_in_loc_cur_ ) as writeoffs, 
    'FFS Darkstore' AS order_ship_mode_level2,
  from finance_ods.aug_ds_writeoff a
  where 1=1
  and cost_center='F098'
  group by posting_date, material
  
UNION ALL

select posting_date, Material, 
        sum( Amt_in_loc_cur_ ) as writeoffs, 
    'PFC_DELIVERY' AS order_ship_mode_level2,
  from finance_ods.writeoff_sept_oct2020_temp a
  where 1=1
  and cost_center='F004'
  group by posting_date, material
  
UNION ALL 

select posting_date, Material,
      sum( Amt_in_loc_cur_ ) as writeoffs, 
    'FFS Darkstore' AS order_ship_mode_level2,
  from finance_ods.writeoff_sept_oct2020_temp a
  where 1=1
  and cost_center='F098'
  group by posting_date, material
  
UNION ALL 

SELECT posting_date, Material,
    sum( amtin_loccur ) as writeoffs, 
    CASE WHEN plant = '004' THEN 'PFC_DELIVERY' WHEN plant = '098' THEN 'FFS Darkstore' END AS order_ship_mode_level2,
FROM (
select 
      company_code_1
      ,plant
      ,'' as name
      ,storage_location
      ,document_date
      ,posting_date
      ,original_line_item_1
      ,cast(material as int64) as material
      ,'' as product_description
      ,qty_in_unit_of_entry
      ,unit_of_entry
      ,case when debit_credit_ind ='H' then -1*amtin_loccur else amtin_loccur end as amtin_loccur -- H credit, S is debit
      ,sales_value_w_o_vat
      ,'' cost_center
      ,movement_type_1
      ,'' as movement_type_text
      ,'' reason_for_movement
      ,material_document_1
      ,document_header_text
      ,entry_date
      ,user_name
      ,purchase_order
      ,debit_credit_ind
      from `ne-fprt-data-cloud-production.fp_sap.fp_sap_matdoc`
      where 
      1=1 
      and kostl IN ('F004','F098')
      and posting_date >= '2020-11-01' 
      and safe_cast(movement_type_1 as int64) in ( 551,552 ,951,952)
) a
group by posting_date, material,  plant
) 
group by extract(year from posting_date),extract(month from posting_date),order_ship_mode_level2,Material


)