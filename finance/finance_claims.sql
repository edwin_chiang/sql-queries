CREATE OR REPLACE TABLE finance_ods.finance_claims AS (
SELECT cast(concat(cast(year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64) AS year_month
    , year
    , month
    , supplier
    , material
    , store_code
    , uom
    , SUM(quantity) AS total_quantity
    , SUM(est_settlement) AS claims
    , SUM(est_settlement) / SUM(quantity) AS claims_per_unit
FROM (
  SELECT settlement_date
      , EXTRACT(year FROM PARSE_DATE('%Y%m%d',CAST(settlement_date AS STRING))) AS year
      , EXTRACT(month FROM PARSE_DATE('%Y%m%d',CAST(settlement_date AS STRING))) AS month
      , supplier
      , material
      , RIGHT(profit_ctr, 3) AS store_code
      , est_settlement
      , quantity
      , uom
  FROM finance_ods.claims_202105
)
GROUP BY year
    , month
    , supplier
    , material
    , store_code
    , uom
    , cast(concat(cast(year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64)
)