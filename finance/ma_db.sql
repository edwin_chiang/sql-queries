SELECT spmon AS year_month
    , CAST(matnr AS INT64) AS product_sku
    , CASE WHEN vsnmr_v IS NULL THEN '004' ELSE vsnmr_v END AS store_code
    , SUM(ma_amt) AS ma_amt
FROM `ne-fprt-data-cloud-production.test_fp_sap_rpt.dwd_fp_macalculation_db` 
GROUP BY spmon
      , matnr
      , vsnmr_v