--20210617 included 2019 sales data


CREATE OR REPLACE TABLE finance_ods.finance_sales_line AS (
  with sales_line AS (
    SELECT date_of_delivery
        ,order_ref_number
        , product_sku
        , CASE 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'FFS' THEN 'FFS Normal' 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'BULK' THEN 'PFC_DELIVERY'
            ELSE COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) 
          END AS order_ship_mode_level2
        , store_name
        , SUM(COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty)) AS delivered_qty
    FROM fpon_cdm.fpon_cdm_sales_line
    WHERE 1=1
    AND order_item_status <> 'CANCELLED'
    AND order_status <> 'CANCELLED'
    AND (order_ship_mode NOT IN ('OFFLINE','CNC') OR order_ship_mode IS NULL)
    AND order_type NOT IN ('OFFLINE', 'PICKUP')
    AND date_of_delivery >= '2019-01-01' 
    AND invoice_number <> 'INVTest'
    AND source = 'ZopSmart'
    AND store_name <> 'Boys Brigade (Donation)'
    AND order_ship_mode <> 'BOY'
    GROUP BY order_ref_number
        , product_sku
        , order_ship_mode_level2
        , store_name
        , date_of_delivery
        
    UNION ALL
    
    SELECT date_of_delivery
        ,order_ref_number
        , product_sku
        , CASE 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'FFS' THEN 'FFS Normal' 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'BULK' THEN 'PFC_DELIVERY'
            ELSE COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) 
          END AS order_ship_mode_level2       
        , store_name
        , SUM(COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty)) AS delivered_qty
    FROM fpon_cdm.fpon_cdm_sales_line
    WHERE 1=1
    AND date_of_delivery >= '2019-01-01'
    AND invoice_number IS NOT NULL
    AND source = 'WCS'
    AND invoice_number <> 'INVTest'
    AND order_ship_mode <> 'BOY'
    GROUP BY order_ref_number
        , product_sku
        , order_ship_mode_level2
        , store_name
        , date_of_delivery
  )

  SELECT a.date_of_delivery
      , a.order_ref_number
      , a.product_sku 
      , b.product_name
      , a.store_name 
      , a.order_ship_mode_level2 
      , a.delivered_qty 
      , b.sap_department
      , b.sap_class
      , b.sap_subclass
      , b.l1_category
      , b.l2_category
      , b.l3_category
      , supplier_name
      , vendor_code
      , is_house_brand
      , uom
      , unit_price_no_gst * a.delivered_qty AS gmv_bef_promo
      , avg_selling_price * a.delivered_qty AS gmv_aft_promo
      , cos_per_unit * a.delivered_qty AS cost_of_sales
      , gp_margin_per_unit_bef_disc * a.delivered_qty AS gross_profit_bef_promo
      , gp_margin_per_unit_aft_disc * a.delivered_qty AS gross_profit_aft_promo
      , claims_per_unit * a.delivered_qty AS claims
      , additions_ma_per_unit * a.delivered_qty AS additions_ma
      , cost_writeoffs_per_unit_actual * a.delivered_qty AS cost_writeoffs
      , inbound_logistics_cost_per_unit	 * a.delivered_qty AS inbound_logistics_cost 
      , additions_other_income_per_unit * a.delivered_qty AS additions_other_income
      , mp_commission_per_unit * a.delivered_qty AS mp_commission
      , cm1_profit_per_unit * a.delivered_qty AS cm1_profit
      , variable_cost_per_unit * a.delivered_qty AS variable_cost
      , fixed_cost_per_unit * a.delivered_qty AS fixed_cost
--       , additions_passback_margin_per_unit * a.delivered_qty AS additions_passback_margin 
      , cost_other_charges_bc_per_unit * a.delivered_qty AS cost_other_charges_bc
      , cost_cust_svc_per_unit * a.delivered_qty AS cost_cust_svc
      , cm2_profit_per_unit * a.delivered_qty AS cm2_profit
  FROM sales_line a
  LEFT JOIN finance_ods.finance_master_table_new b
  ON CAST(a.product_sku AS INT64) = b.product_sku
  AND a.order_ship_mode_level2 = b.order_ship_mode_level2
  AND a.store_name = b.store_name
  AND EXTRACT(year FROM a.date_of_delivery) = b.year
  AND EXTRACT(month FROM a.date_of_delivery) = b.month
)