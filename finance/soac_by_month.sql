--20210617 included 2019 soac

CREATE OR REPLACE TABLE finance_ods.soac_by_month AS (
with soac_tbl AS (
  SELECT * FROM finance_ods.soac_2020 WHERE vendor_code IS NOT NULL
  UNION ALL
  SELECT * FROM finance_ods.soac_2021 WHERE vendor_code IS NOT NULL
  UNION ALL 
  SELECT vendor_code
      , FORMAT_DATE('%d.%m.%Y',DATE(promo_start_date)) AS promo_start_date  
      , FORMAT_DATE('%d.%m.%Y',DATE(promo_end_date)) AS promo_end_date 
      , soac
  FROM finance_ods.soac_2019
  WHERE vendor_code IS NOT NULL
  AND promo_start_date IS NOT NULL 
  AND promo_end_date IS NOT NULL 
  AND soac IS NOT NULL
  AND promo_start_date < promo_end_date
)
,
tbl AS (
  SELECT *, soac / days_of_promo AS soac_per_day
  FROM (
    SELECT vendor_code,
           PARSE_DATE('%d.%m.%Y',promo_start_date) AS start_date, 
           PARSE_DATE('%d.%m.%Y',promo_end_date) AS end_date, 
           day,
--            CAST(REPLACE(REPLACE(soac, '$', ''),',','') as numeric) AS soac, 
           soac,
           CASE
            WHEN PARSE_DATE('%d.%m.%Y',promo_end_date) < PARSE_DATE('%d.%m.%Y',promo_start_date) THEN  DATE_DIFF(PARSE_DATE('%d.%m.%Y',promo_start_date) ,PARSE_DATE('%d.%m.%Y',promo_end_date), DAY) + 1 
            ELSE DATE_DIFF(PARSE_DATE('%d.%m.%Y',promo_end_date) ,PARSE_DATE('%d.%m.%Y',promo_start_date), DAY) + 1
           END AS days_of_promo,
    FROM soac_tbl CROSS JOIN UNNEST(GENERATE_DATE_ARRAY(PARSE_DATE('%d.%m.%Y',promo_start_date), PARSE_DATE('%d.%m.%Y',promo_end_date), INTERVAL 1 DAY)) AS day
  )
--   WHERE same_month = 0
ORDER BY start_date DESC
)

SELECT *
FROM (
  SELECT year, month, vendor_code, SUM(soac_per_day) AS soac
  FROM( 
    select *, EXTRACT(month FROM day) AS month, EXTRACT(year FROM day) AS year
    from tbl 
  )
  GROUP BY vendor_code,month,year
  ORDER BY year, month, vendor_code
)
-- WHERE vendor_code = 70529

)