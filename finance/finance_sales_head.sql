-- 20210607 include membership fees

-- 20210617 included 2019 sales data

CREATE OR REPLACE TABLE finance_ods.finance_sales_head AS (
  with sales_head AS (
    SELECT date_of_delivery
        , order_ref_number
        , customer_id
        , CASE 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'FFS' THEN 'FFS Normal' 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'BULK' THEN 'PFC_DELIVERY'
            ELSE COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) 
          END AS order_ship_mode_level2        , store_name
        , SUM(COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty)) AS delivered_qty
        , SUM(order_shipping) AS order_shipping
        , SUM(service_fee) AS service_fee
    FROM fpon_cdm.fpon_cdm_sales_head
    WHERE 1=1
    AND order_status <> 'CANCELLED'
    AND (order_ship_mode NOT IN ('OFFLINE','CNC') OR order_ship_mode IS NULL)
    AND order_type NOT IN ('OFFLINE', 'PICKUP')
    AND date_of_delivery >= '2019-01-01' 
    AND store_name <> 'Boys Brigade (Donation)'
    AND invoice_number <> 'INVTest'
    AND source = 'ZopSmart'
    GROUP BY order_ref_number
        , order_ship_mode_level2
        , store_name
        , date_of_delivery
        , customer_id
        
    UNION ALL 
    
        SELECT date_of_delivery
        , order_ref_number
        , customer_id
        , CASE 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'FFS' THEN 'FFS Normal' 
            WHEN COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) = 'BULK' THEN 'PFC_DELIVERY'
            ELSE COALESCE(CASE WHEN store_name = 'Ang Mo Kio 712 (FFS)' THEN 'FFS Normal' ELSE order_ship_mode_level2 END, order_ship_mode) 
          END AS order_ship_mode_level2        , store_name
        , SUM(COALESCE(delivered_qty_with_normalised_weighted_items, delivered_qty)) AS delivered_qty
        , SUM(order_shipping) AS order_shipping
        , SUM(service_fee) AS service_fee
    FROM fpon_cdm.fpon_cdm_sales_head
    WHERE 1=1
    AND date_of_delivery >= '2019-01-01'
    AND invoice_number IS NOT NULL
    AND source = 'WCS'
    AND invoice_number <> 'INVTest'
    AND order_ship_mode <> 'BOY'
    GROUP BY order_ref_number
        , order_ship_mode_level2
        , store_name
        , date_of_delivery
        , customer_id
    
    
  )
,

vcpo AS (
  SELECT *
      , vcpo / order_count AS variable_cost_per_order
  FROM (SELECT a.year
      , a.month
      , a.order_ship_mode_level2
      , COALESCE(CAST(CASE WHEN REGEXP_REPLACE(TRIM(CAST(vcpo AS STRING), '-'),',','') = '' THEN 0 ELSE CAST(REGEXP_REPLACE(TRIM(CAST(vcpo AS STRING), '-'),',','') AS FLOAT64) END AS FLOAT64),0) AS vcpo
      , order_count
  FROM finance_ods.profit_and_loss a
  JOIN (SELECT natural_year as year
            , month
            , order_ship_mode_level2
            , COUNT(DISTINCT order_ref_number) AS order_count
              FROM fpon_cdm.fpon_cdm_sales_head a
              JOIN fpon_cdm.calendar_full b
              ON a.date_of_delivery = b.date
              WHERE 1=1
              # WHERE payment_status NOT IN ('FAILED','CANCELLED')
              AND order_status <> 'CANCELLED'
              AND (order_ship_mode NOT IN ('OFFLINE','CNC') OR order_ship_mode IS NULL)
              AND order_type NOT IN ('OFFLINE', 'PICKUP')
              AND date_of_delivery >= '2019-01-01'
              GROUP BY natural_year, month,order_ship_mode_level2
             ) b
        ON CAST(a.year AS INT64) = b.year 
        AND CAST(a.month AS INT64) = b.month 
        AND a.order_ship_mode_level2 = b.order_ship_mode_level2
  )
)

  SELECT customer_id
      ,a.date_of_delivery
      , a.order_ref_number
      , a.order_ship_mode_level2
      , a.store_name
      , a.delivered_qty
      , a.order_shipping
      , a.service_fee
--       , dc.amount AS membership_fee
--       , COUNT(DISTINCT a.order_ref_number) OVER (PARTITION BY customer_id, EXTRACT(year FROM a.date_of_delivery), EXTRACT(month FROM a.date_of_delivery)) AS order_count
      , dc.amount / COUNT(DISTINCT a.order_ref_number) OVER (PARTITION BY customer_id, EXTRACT(year FROM a.date_of_delivery), EXTRACT(month FROM a.date_of_delivery)) AS membership_fee
      , b.* EXCEPT(date_of_delivery,order_ref_number,order_ship_mode_level2,store_name,delivered_qty,cm2_profit)
      , b.cm2_profit 
      - variable_cost_per_order 
      + a.order_shipping 
      + a.service_fee 
      + dc.amount / COUNT(DISTINCT a.order_ref_number) OVER (PARTITION BY customer_id, EXTRACT(year FROM a.date_of_delivery), EXTRACT(month FROM a.date_of_delivery))
      AS cm2_profit
  FROM sales_head a
  LEFT JOIN (
  SELECT date_of_delivery
    , order_ref_number
    , store_name
    , sales_line.order_ship_mode_level2
    , SUM(delivered_qty) AS delivered_qty
    , SUM(gmv_bef_promo) AS gmv_bef_promo
    , SUM(gmv_aft_promo) AS gmv_aft_promo
    , SUM(cost_of_sales) AS cost_of_sales
    , SUM(gross_profit_bef_promo) AS gross_profit_bef_promo
    , SUM(gross_profit_aft_promo) AS gross_profit_aft_promo
    , SUM(claims) AS claims
    , SUM(additions_ma) AS additions_ma
    , SUM(cost_writeoffs) AS cost_writeoffs
    , SUM(sales_line.inbound_logistics_cost) AS inbound_logistics_cost
    , SUM(additions_other_income) AS additions_other_income
    , SUM(mp_commission) AS mp_commission
    , SUM(cm1_profit) AS cm1_profit
    , SUM(variable_cost) AS variable_cost
    , SUM(fixed_cost) AS fixed_cost
--     , SUM(sales_line.additions_passback_margin) AS additions_passback_margin
    , SUM(cost_other_charges_bc) AS cost_other_charges_bc
    , SUM(cost_cust_svc) AS cost_cust_svc
    , SUM(cm2_profit) AS cm2_profit
    , variable_cost_per_order
  FROM finance_ods.finance_sales_line sales_line
  JOIN vcpo profit_and_loss
  ON EXTRACT(year FROM sales_line.date_of_delivery) = profit_and_loss.year
  AND EXTRACT(month FROM sales_line.date_of_delivery) = profit_and_loss.month
  AND sales_line.order_ship_mode_level2 = profit_and_loss.order_ship_mode_level2
  -- WHERE variable_cost_per_order <> 0
  GROUP BY date_of_delivery
      , order_ref_number
      , store_name
      , order_ship_mode_level2
      , variable_cost_per_order
  ) 
  b
  ON a.order_ref_number = b.order_ref_number
  LEFT JOIN (select *
        from fpon_analytics.digital_club_membership
        where refund_amount = 0
        order by fp_id 
        ) dc
  ON a.customer_id = CAST(dc.fp_id AS STRING)
--   AND a.date_of_delivery BETWEEN DATE(start_date) AND DATE_SUB(DATE(end_date), INTERVAL 1 DAY)
  AND EXTRACT(year FROM a.date_of_delivery) = EXTRACT(year FROM dc.start_date)
  AND EXTRACT(month FROM a.date_of_delivery) = EXTRACT(month FROM dc.start_date)
)