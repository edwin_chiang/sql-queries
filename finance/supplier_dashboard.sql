CREATE OR REPLACE TABLE fpon_analytics.supplier_dashboard AS (
  with tbl AS (SELECT DISTINCT cast(concat(cast(year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64) as year_month, year, month, COALESCE(order_ship_mode_level2) AS channel ,vendor_code, COALESCE(a.supplier_name,b.vendor_name) AS supplier_name, sap_department, sap_class, sap_subclass, unique_skus, delivered_qty, gmv_aft_promo, cos, claims,ma,
         soac, cos - claims - ma - soac AS net_to_supplier, CASE WHEN cos = 0 THEN 0 ELSE CAST(soac AS NUMERIC) / CAST(cos AS NUMERIC) END AS perc_soac_of_cos,
  FROM (
    SELECT COALESCE(a.year,b.year) AS year,
           COALESCE(a.month,b.month) AS month,  
           COALESCE(a.vendor_code,CAST(b.vendor_code AS INT64)) AS vendor_code,
           supplier_name,
           a.order_ship_mode_level2, 
           sap_department, sap_class, sap_subclass, unique_skus, 
           delivered_qty, 
           COALESCE(gmv_aft_promo,0) AS gmv_aft_promo,
           COALESCE(claims,0) AS claims,
           COALESCE(ma,0) AS ma,
           COALESCE(cos,0) AS cos,
           COALESCE(SAFE_DIVIDE(soac , SUM(delivered_qty) OVER (PARTITION BY a.vendor_code, a.year, a.month)) * delivered_qty,soac,0) AS soac
    FROM (SELECT year
              , month
              , vendor_code
              , order_ship_mode_level2
              , supplier_name, sap_department
              , sap_class, sap_subclass
              , COUNT(DISTINCT product_sku) AS unique_skus
              , SUM(delivered_qty) AS delivered_qty
              , SUM(gmv_no_gst_aft_promo_disc) AS gmv_aft_promo
              , SUM(claims) AS claims
              , SUM(cos) AS cos
              , SUM(CASE WHEN CAST(product_sku AS STRING) LIKE '900%' THEN gmv_no_gst_aft_promo_disc ELSE cos END) AS cos_soac
              , SUM(additions_MA) AS ma
           FROM finance_ods.finance_master_table_new 
  --          WHERE order_ship_mode_level2 = 'PFC_DELIVERY'
           GROUP BY year
                , month
                , vendor_code
                , supplier_name
                , sap_department
                , sap_class
                , sap_subclass
                ,order_ship_mode_level2
            ) a 
    FULL OUTER JOIN (SELECT year, month, vendor_code, SUM(CAST(soac AS FLOAT64)) AS soac FROM finance_ods.soac_by_month WHERE month <= 12 GROUP BY year, month, vendor_code) b
    ON a.vendor_code = CAST(b.vendor_code AS INT64)
    AND a.year = CAST(b.year AS INT64)
    AND a.month = CAST(b.month AS INT64)
--     AND a.order_ship_mode_level2 = 'PFC_DELIVERY'
  -- GROUP BY year, month, vendor_code, supplier_name, sap_department, sap_class, sap_subclass
  ) a
  LEFT JOIN fpon_ods.cls_product b
  ON SAFE_CAST(a.vendor_code AS int64) = SAFE_CAST(b.vendor AS INT64)
  )

  SELECT b.date AS first_day_of_month,
        c.date AS last_day_of_month,
        a.*
  FROM tbl a
  JOIN (SELECT * FROM fpon_cdm.calendar_full WHERE day = 1) b
  ON a.year = b.natural_year 
  AND a.month = b.month
  JOIN (SELECT * FROM (SELECT *, row_number() over (partition by natural_year, month ORDER BY day DESC) as rnk FROM fpon_cdm.calendar_full) WHERE rnk = 1 ) c
  ON a.year = c.natural_year 
  AND a.month = c.month
  WHERE year_month <= cast(concat(EXTRACT(year FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)),case when EXTRACT(month FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) < 10 then '0' else '' end ,cast(EXTRACT(month FROM DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)) as string)) as int64)
)