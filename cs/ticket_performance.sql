CREATE OR REPLACE TABLE fpon_analytics.ticket_performance AS (
with tickets AS (
  SELECT *
  FROM fpon_cdm.zendesk_full_table_snapshot
  WHERE ticket_created_date BETWEEN '2021-01-01' AND '2021-03-31'
)
,

tbl AS (
  SELECT tickets.*
      , reply_time_in_minutes 
      , first_resolution_time_in_minutes 
      , full_resolution_time_in_minutes
      , agent_wait_time_in_minutes
      , requester_wait_time_in_minutes
      , on_hold_time_in_minutes
      , requester_wait_time_in_minutes - on_hold_time_in_minutes AS agent_work_time_in_minutes
      , sla.assignee_id AS sla_assignee_id
      , sla.sla_agent_work_time
      , sla.sla_requester_wait_time
      , csat.assignee_id AS csat_assignee_id
      , csat.ticket_csat_score
      , scorecard.csat_score
      , scorecard.csat_tier
      , scorecard.two_touch
      , scorecard.two_touch_tier
      , scorecard.sla_agent_work_time AS sla_agent_work_time_perc
      , scorecard.sla_agent_work_time_tier
      , scorecard.sla_requester_wait_time AS sla_requester_wait_time_perc
      , scorecard.sla_requester_wait_time_tier 
  FROM tickets tickets
  LEFT JOIN zendesk.zendesk_ticket_metrics metrics
  ON tickets.ticket_id = metrics.ticket_id
  LEFT JOIN fpon_analytics.sla_extract sla
  ON tickets.ticket_id = sla.ticket_id
  LEFT JOIN fpon_analytics.csat_extract csat
  ON tickets.ticket_id = csat.ticket_id
  LEFT JOIN fpon_analytics.agent_scorecard_extract scorecard
  ON sla.assignee_id = scorecard.assignee_id
)
,

performance AS (
  SELECT customer_id
      , ticket_id
      , order_ref_number
      , date_of_delivery
      , feedback_category_new
      , ticket_created_date
      , ticket_solved_date
      , channel
      , ticket_satisfaction_score
      , one_two_touch
      , COALESCE(sla_assignee_id, csat_assignee_id) AS assignee_id
      , sla_agent_work_time
      , sla_requester_wait_time
      , ticket_csat_score
      , csat_score AS agent_csat_score
      , csat_tier AS agent_csat_tier
      , two_touch AS agent_two_touch
      , two_touch_tier AS agent_two_touch_tier
      , sla_agent_work_time_perc 
      , sla_agent_work_time_tier
      , sla_requester_wait_time_perc
      , sla_requester_wait_time_tier
      , agent_wait_time_in_minutes / 60 AS agent_wait_time_in_hrs
      , requester_wait_time_in_minutes / 60 AS requester_wait_time_in_hrs
      , on_hold_time_in_minutes / 60 AS on_hold_time_in_hrs
      , agent_work_time_in_minutes / 60 AS agent_work_time_in_hrs
  FROM tbl 
  WHERE customer_id IS NOT NULL
  AND sla_assignee_id IS NOT NULL
)
,
sales_head AS (
  SELECT *
  FROM fpon_cdm.fpon_cdm_sales_head
  WHERE order_status <> 'CANCELLED'
  AND order_type <> 'OFFLINE'
  AND order_ship_mode <> 'CNC'
)
,
after_ticket_sales AS (
  SELECT customer_id
      , ticket_id
      , ticket_created_date
      , COALESCE(SUM(final_order_amount), 0) AS total_spend_aft_ticket
      , COALESCE(SUM(order_item_count), 0) AS total_unique_items_ordered_aft_ticket
      , COALESCE(SUM(ordered_qty_with_normalised_weighted_items), 0) AS total_items_ordered_aft_ticket
      , COUNT(DISTINCT order_ref_number) AS no_of_orders_aft_ticket
      , CASE 
              WHEN COUNT(DISTINCT order_ref_number) = 0 THEN 0 
              ELSE COALESCE(SUM(ordered_qty_with_normalised_weighted_items),0) / COUNT(DISTINCT order_ref_number) 
        END AS aoq_aft_ticket
      , CASE 
              WHEN COUNT(DISTINCT order_ref_number) = 0 THEN 0 
              ELSE COALESCE(SUM(final_order_amount),0) / COUNT(DISTINCT order_ref_number)
        END AS aov_aft_ticket
  FROM (
  SELECT p.customer_id
      , p.ticket_id
      , p.ticket_created_date
      , aft_sh.order_ref_number
      , aft_sh.store_name
      , aft_sh.final_order_amount 
      , aft_sh.order_item_count
      , aft_sh.ordered_qty_with_normalised_weighted_items
      , aft_sh.date_of_order
      , DATE_DIFF(aft_sh.date_of_order,p.ticket_created_date, DAY) AS days_between_ticket_created_date_and_date_of_order
  FROM performance p
  LEFT JOIN sales_head aft_sh
  ON p.customer_id = aft_sh.customer_id
  AND p.ticket_created_date < aft_sh.date_of_order
  AND p.order_ref_number <> aft_sh.order_ref_number
  AND DATE_DIFF(aft_sh.date_of_order,p.ticket_created_date, DAY) <= 30
  )
  GROUP BY customer_id, ticket_id, ticket_created_date
)
, 
before_ticket_sales AS (
    SELECT customer_id
        , ticket_id
        , ticket_created_date
        , COALESCE(SUM(final_order_amount),0) AS total_spend_bef_ticket
        , COALESCE(SUM(order_item_count),0) AS total_unique_items_ordered_bef_ticket
        , COALESCE(SUM(ordered_qty_with_normalised_weighted_items),0) AS total_items_ordered_bef_ticket
        , COUNT(DISTINCT order_ref_number) AS no_of_orders_bef_ticket
        , CASE 
              WHEN COUNT(DISTINCT order_ref_number) = 0 THEN 0 
              ELSE COALESCE(SUM(ordered_qty_with_normalised_weighted_items),0) / COUNT(DISTINCT order_ref_number) 
          END AS aoq_bef_ticket
        , CASE 
              WHEN COUNT(DISTINCT order_ref_number) = 0 THEN 0 
              ELSE COALESCE(SUM(final_order_amount),0) / COUNT(DISTINCT order_ref_number)
          END AS aov_bef_ticket
    FROM (
    SELECT p.customer_id
        , p.ticket_id
        , p.ticket_created_date
        , bef_sh.order_ref_number
        , bef_sh.store_name
        , bef_sh.final_order_amount 
        , bef_sh.order_item_count
        , bef_sh.ordered_qty_with_normalised_weighted_items
        , bef_sh.date_of_order
        , DATE_DIFF(p.ticket_created_date,bef_sh.date_of_order, DAY) AS days_between_ticket_created_date_and_date_of_order
    FROM performance p
    LEFT JOIN sales_head bef_sh
    ON p.customer_id = bef_sh.customer_id
    AND p.ticket_created_date > bef_sh.date_of_order
    AND p.order_ref_number <> bef_sh.order_ref_number
    AND DATE_DIFF(p.ticket_created_date,bef_sh.date_of_order, DAY) <= 30
  )
  GROUP BY customer_id, ticket_id, ticket_created_date
)

select bef.*
    , aft.* except(ticket_id, ticket_created_date, customer_id)
    , p.* except(ticket_id, ticket_created_date, customer_id)
from before_ticket_sales bef
join after_ticket_sales aft
on bef.ticket_id = aft.ticket_id
and bef.customer_id = aft.customer_id
and bef.ticket_created_date = aft.ticket_created_date
join performance p
on bef.customer_id = p.customer_id
and bef.ticket_id = p.ticket_id
and bef.ticket_created_date = p.ticket_created_date

)