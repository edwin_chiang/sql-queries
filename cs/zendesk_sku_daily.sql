with full_table as 
(select * from
(
 SELECT zd.*,
        cal.cs_year as ticket_created_year,
        cal.month as ticket_created_month,
        cal.cs_week as ticket_created_week,
        ROW_NUMBER() OVER (PARTITION BY zd.ticket_id ORDER BY zd.snapshot_date DESC) AS rk
 FROM fpon_cdm.zendesk_tickets_weekly_snapshot as zd
 left join fpon_cdm.calendar_full as cal on cal.date =  zd.ticket_created_date
where snapshot_date = CURRENT_DATE()
)
where rk=1 -- select only those rk  =1 for most updated status
AND (brand_id = 114094936051 OR brand_id IS NULL) 
and ticket_tags NOT LIKE '%closed_by_merge%'
)
,
zendesk_full_table as
(
SELECT 
  sh.customer_id
  , sh.order_id
  , sh.order_ref_number
  , sh.order_status
  , sh.date_of_order
  , sh.date_of_delivery
  , sh.store_code
  , sh.store_name
  , sh.order_ship_mode
  , sh.order_ship_mode_level2
  , sh.order_type
  , sh.final_order_amount
  , case when sh.ordered_qty - sh.delivered_qty = 0 then 0 else 1 end as dropoff
  , full_table.ticket_id
  , full_table.ticket_status
  , full_table.refund_amount as refund_amount_new
  , full_table.pdt_type
  , full_table.damage_cause
  , full_table.feedback_category_new
  , full_table.sku
  , delivery_method
  , negligent_party
  , cancellation_reason
  , ticket_created_date
  , ticket_created_year
  , ticket_created_month
  , ticket_created_week
  , refund_amount
  , ticket_requester_domain
  , ticket_subject
  , ticket_solved_date
  , channel
  , ticket_satisfaction_score
  , no_of_tickets
  , order_issues_data
  , ticket_tags
  FROM full_table
  left join fpon_cdm.fpon_cdm_sales_head as sh 
  on cast(sh.order_ref_number as string) = cast(full_table.order_reference_number as string)
  order by ticket_created_year desc, ticket_created_week desc
)
,
extract_sku as
(
select 
  -- ticket information
  ticket_id
  , ticket_status
  , refund_amount
  , ticket_created_date
  , ticket_created_year
  , ticket_created_month
  , ticket_created_week
  , feedback_category_new
  , negligent_party
  , sku
  , cancellation_reason
  , damage_cause
  , pdt_type
  , channel
  -- order information
  , customer_id
  , order_id
  , order_ref_number
  , order_status
  , date_of_order
  , date_of_delivery
  , final_order_amount
  -- store information
  , store_code
  , store_name
  , order_ship_mode
  , order_ship_mode_level2
  , order_type
  , order_issues_data
  , ticket_tags
  , REGEXP_EXTRACT_ALL(sku, '[0-9]{6,}') as product_sku_array
from zendesk_full_table
--where feedback_category_new in ('order_issues__damaged_items','order_issues__missing_items','changes_to_orders__add/remove_items')
order by sku
)
,

order_issues AS (
    SELECT *,      
       JSON_EXTRACT_SCALAR(split_items,'$.id') AS sku_new, 
       JSON_EXTRACT_SCALAR(split_items,'$.name') AS product_name_new,
       JSON_EXTRACT_SCALAR(split_items,'$.packaging') AS uom,
       JSON_EXTRACT_SCALAR(split_items,'$.affected') AS affected,
       JSON_EXTRACT_SCALAR(split_items,'$.collections')AS collections,
       JSON_EXTRACT_SCALAR(split_items,'$.deliveries') AS deliveries
    FROM (
        SELECT * 
        FROM zendesk_full_table
        LEFT JOIN UNNEST(JSON_EXTRACT_ARRAY(order_issues_data)) split_items
    )
    # WHERE order_ref_number = '64187723'
    WHERE order_issues_data IS NOT NULL
    AND order_issues_data LIKE '%{%'
    ORDER BY date_of_delivery DESC, ticket_id

)
,

product_list as
(
select DISTINCT ticket.*
      #  b.sku_new, b.product_name_new , b.uom, 
      #  CAST(b.collections AS NUMERIC) AS collections, 
      #  CAST(b.deliveries AS NUMERIC) as deliveries
--, pdt.product_code
from
(
select
  ticket_id
  , ticket_status
  , refund_amount
  , ticket_created_date
  , ticket_created_year
  , ticket_created_month
  , ticket_created_week
  , feedback_category_new
  , negligent_party
  , cancellation_reason
  , damage_cause
  , pdt_type
  , channel
  -- order information
  , customer_id
  , order_id
  , order_ref_number
  , order_status
  , date_of_order
  , date_of_delivery
  , final_order_amount
  -- store information
  , store_code
  , store_name
  , order_ship_mode
  , order_ship_mode_level2
  , order_type
  , product_sku
  , ticket_tags
  , order_issues_data
  , null as sku_new
  , null as product_name_new
  , null as uom
  , null as affected
  , null as collections
  , null as deliveries
from extract_sku left join
unnest(product_sku_array) product_sku
UNION ALL 
SELECT ticket_id
  , ticket_status
  , refund_amount
  , ticket_created_date
  , ticket_created_year
  , ticket_created_month
  , ticket_created_week
  , feedback_category_new
  , negligent_party
  , cancellation_reason
  , damage_cause
  , pdt_type
  , channel
  -- order information
  , customer_id
  , order_id
  , order_ref_number
  , order_status
  , date_of_order
  , date_of_delivery
  , final_order_amount
  -- store information
  , store_code
  , store_name
  , order_ship_mode
  , order_ship_mode_level2
  , order_type
  , NULL AS product_sku
  , ticket_tags
  , order_issues_data
  , JSON_EXTRACT_SCALAR(split_items,'$.id') AS sku_new, 
    JSON_EXTRACT_SCALAR(split_items,'$.name') AS product_name_new,
    JSON_EXTRACT_SCALAR(split_items,'$.packaging') AS uom,
    JSON_EXTRACT_SCALAR(split_items,'$.affected') AS affected,
    JSON_EXTRACT_SCALAR(split_items,'$.collections')AS collections,
    JSON_EXTRACT_SCALAR(split_items,'$.deliveries') AS deliveries
 FROM order_issues
order by ticket_id
) as ticket
)
,

final_tbl AS (
SELECT ticket_id,ticket_status,refund_amount,ticket_created_date,ticket_created_year,ticket_created_month,ticket_created_week,feedback_category_new,negligent_party,cancellation_reason,
  damage_cause,pdt_type,channel,ticket.customer_id,ticket.order_id,ticket.order_ref_number,ticket.order_status,ticket.date_of_order,ticket.date_of_delivery,final_order_amount,store_code,
  ticket.store_name,ticket.order_ship_mode,ticket.order_ship_mode_level2,ticket.order_type,
  ticket_tags,
  uom,
  refunded,
  collections,
  deliveries
, pdt.department_desc
, class_desc
, subclass_desc
, COALESCE(ticket.product_sku,sku_new) AS product_sku
, COALESCE(pdt.product_name, product_name_new) as product_name
, pdt.is_marketplace
, sl.order_item_invoiced_amount
, sl.order_item_ordered_amount
, sl.ordered_qty 
, sl.delivered_qty
, sl.substituted_item_id
, sub.product_sku AS substituted_sku
, sub.product_name AS substituted_item_name
FROM (
  SELECT ticket_id,ticket_status,refund_amount,ticket_created_date,ticket_created_year,ticket_created_month,ticket_created_week,feedback_category_new,negligent_party,cancellation_reason,
  damage_cause,pdt_type,channel,customer_id,order_id,order_ref_number,order_status,date_of_order,date_of_delivery,final_order_amount,store_code,store_name,order_ship_mode,order_ship_mode_level2,order_type,
  ticket_tags,
  product_sku,
  sku_new,
  product_name_new,
  uom,
  CASE WHEN REGEXP_CONTAINS(affected, '[0-9]') = FALSE THEN 0 WHEN affected IS NULL THEN 0 ELSE CAST(affected AS NUMERIC) END AS refunded, 
  CASE WHEN REGEXP_CONTAINS(collections, '[0-9]') = FALSE THEN 0 WHEN collections IS NULL THEN 0 ELSE CAST(collections AS NUMERIC) END AS collections,
  CASE WHEN REGEXP_CONTAINS(deliveries, '[0-9]') = FALSE THEN 0 WHEN deliveries IS NULL THEN 0 ELSE CAST(deliveries AS NUMERIC) END AS deliveries,
  rnk,
  order_issues_data
  FROM (
    select product_list.*, ROW_NUMBER() OVER (PARTITION BY order_ref_number, COALESCE(product_sku,sku_new) ORDER BY ticket_id, uom DESC) AS rnk
    from product_list
    where 1=1
    and COALESCE(product_sku,sku_new) IS NOT NULL
    order by ticket_id
  )
) ticket
left join (SELECT * FROM fpon_cdm.fpon_cdm_product WHERE deleted_at IS NULL) as pdt on cast(pdt.product_code as string) = COALESCE(ticket.product_sku, ticket.sku_new)
left join (select order_ref_number, product_sku, product_id, orderitems_id,
           SUM(order_item_invoiced_amount) AS order_item_invoiced_amount, 
           SUM(order_item_ordered_amount) AS order_item_ordered_amount, 
           SUM(ordered_qty) AS ordered_qty, 
           SUM(delivered_qty) AS delivered_qty,
           substituted_item_id 
           from fpon_cdm.fpon_cdm_sales_line
           GROUP BY order_ref_number, product_sku, product_id, substituted_item_id,orderitems_id) as sl 
  on sl.order_ref_number = ticket.order_ref_number and cast(sl.product_sku as string)= COALESCE(ticket.product_sku, ticket.sku_new)
left join (select DISTINCT order_ref_number, product_sku,product_name,substituted_item_id from fpon_cdm.fpon_cdm_sales_line where substituted_item_id is not null) sub
  on sl.order_ref_number = sub.order_ref_number and cast(sl.orderitems_id as int64) = sub.substituted_item_id
where rnk = 1
and ticket_id IS NOT NULL
# and uom IS NOT NULL
# # and ticket_id = 1003831
# # AND department_desc is not null
ORDER BY ticket_created_date DESC
)

SELECT ticket_id,ticket_status,refund_amount,ticket_created_date,ticket_created_year,ticket_created_month,ticket_created_week,feedback_category_new,negligent_party,cancellation_reason,
  damage_cause,pdt_type,channel,customer_id,order_id,order_ref_number,order_status,date_of_order,date_of_delivery,final_order_amount,store_code,
  store_name,order_ship_mode,order_ship_mode_level2,order_type,
  ticket_tags,
  uom,
  refunded,
  collections,
  deliveries
, department_desc
, class_desc
, subclass_desc
, product_sku
, product_name
, is_marketplace
, SUM(order_item_invoiced_amount) AS order_item_invoiced_amount
, SUM(order_item_ordered_amount) AS order_item_ordered_amount
, SUM(ordered_qty) AS ordered_qty
, SUM(delivered_qty) AS delivered_qty
, substituted_item_id
, substituted_sku
, substituted_item_name
FROM final_tbl
GROUP BY ticket_id,ticket_status,refund_amount,ticket_created_date,ticket_created_year,ticket_created_month,ticket_created_week,feedback_category_new,negligent_party,cancellation_reason,
  damage_cause,pdt_type,channel,customer_id,order_id,order_ref_number,order_status,date_of_order,date_of_delivery,final_order_amount,store_code,
  store_name,order_ship_mode,order_ship_mode_level2,order_type,
  ticket_tags,
  uom,
  refunded,
  collections,
  deliveries
, department_desc
, class_desc
, subclass_desc
, product_sku
, product_name
, is_marketplace
, substituted_item_id
, substituted_sku
, substituted_item_name

