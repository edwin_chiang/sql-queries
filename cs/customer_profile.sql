-- Change on 100121: Condition for frequent refund requestor from >2 in past 2 month to >3 in past 2 months. Change takes effect on 110121

------------------------------------------------
--------- ABUSERS D - MULTIPLE REFUNDS ---------
------------------------------------------------
with no_auto_reply as (
  select distinct customer_id , 'Frequent refund requestor' as profile, 'no_auto_refund' as reason, null as refunds_requested
  from (
      select *
      from fpon_cdm.zendesk_full_table_snapshot
      where ticket_tags like ('%no_auto_refund%')
      and feedback_category_new in ('order_issues__damaged_items', 'order_issues__missing_items')
      and order_type in ('DELIVERY')
      and customer_id is not null
  )
)
,
allrefund as (
  select *
  from fpon_cdm.zendesk_full_table_snapshot
  where customer_id is not null
  and refund_amount is not null
  and refund_amount not in (0,0.00)
  and order_ref_number is not null
  and feedback_category_new in ('order_issues__damaged_items', 'order_issues__missing_items')
  and ticket_created_date between DATE_SUB(DATE_SUB(CURRENT_DATE(),INTERVAL 1 DAY), INTERVAL 61 DAY) and DATE_SUB(CURRENT_DATE(),INTERVAL 1 DAY)
  --and date_of_order between '2020-01-04' and '2020-03-04'
)
, 
repeat_refund as (
  select customer_id, count(distinct ticket_id) as refunds_requested
  from allrefund
  group by customer_id
  order by refunds_requested desc
)
,
final_abusers_list as (
  select distinct customer_id 
         , 'Frequent refund requestor' as profile
         , 'over limit' as reason
         , refunds_requested
  from
  (
    select * 
    from repeat_refund where refunds_requested > 3
  )
)
------------------------------------------------
----------------- HIGH VALUE  ------------------
------------------------------------------------
,

final_highvalue_list as (
  select distinct online_id as customer_id
  , 'Highvalue' as profile
  , 'Golden Nuggets' as reason
  , null as refund_requested
  from `fp-marketing-tech-production.martech.fpon_rfm_salesforce_integration` 
  where rfm_segment = 'Golden Nuggets'
  and CAST(online_id AS STRING) NOT IN (select customer_id from final_abusers_list) 
  and CAST(online_id AS STRING) NOT IN (select customer_id from no_auto_reply) 
)

select cast(customer_id as string) as customer_id, profile, 'no_auto_refund' as reason, null as refunds_requested
from no_auto_reply

UNION ALL

select * 
from final_abusers_list

UNION ALL

select cast(customer_id as string) as customer_id, profile, 'Golden Nuggets' as reason , null as refund_requested
from final_highvalue_list


