with full_table as 
(select * from
(
 SELECT zd.*,
        cal.cs_year as ticket_created_year,
        cal.month as ticket_created_month,
        cal.cs_week as ticket_created_week,
        ROW_NUMBER() OVER (PARTITION BY zd.ticket_id ORDER BY zd.snapshot_date DESC) AS rk
 FROM fpon_cdm.zendesk_tickets_weekly_snapshot as zd
 left join fpon_cdm.calendar_full as cal on cal.date =  zd.ticket_created_date
--where snapshot_date = '2020-05-27'
where snapshot_date = CURRENT_DATE()  --weekly wednesday snapshot
)
where rk=1 -- select only those rk  =1 for most updated status
and brand_id = 114094936051
)

SELECT 
  sh.customer_id
  , sh.order_id
  , sh.order_ref_number
  , sh.order_status
  , sh.date_of_order
  , sh.date_of_delivery
  , sh.store_code
  , sh.store_name
  , sh.order_ship_mode
  , sh.order_type
  , sh.final_order_amount
  , case when sh.ordered_qty_with_normalised_weighted_items - sh.delivered_qty_with_normalised_weighted_items = 0 then 0 else 1 end as dropoff
  , full_table.ticket_id
  , full_table.ticket_status
  , full_table.refund_amount as refund_amount_new
  , ticket_tags
  , full_table.pdt_type
  , full_table.damage_cause
  , full_table.feedback_category_new
  , full_table.sku
  , delivery_method
  , negligent_party
  , cancellation_reason
  , ticket_created_date
  , ticket_created_year
  , ticket_created_month
  , ticket_created_week
  , refund_amount
  , refund_status
  , ticket_requester_domain
  , ticket_subject
  , ticket_solved_date
  , channel
  , ticket_satisfaction_score
  , CASE 
       WHEN full_table.ticket_tags NOT LIKE '%sms_sent_via_api%' AND full_table.ticket_tags NOT LIKE '%send_bulk_sms%' AND replies <= 1 THEN 'one-touch' 
       WHEN full_table.ticket_tags NOT LIKE '%sms_sent_via_api%' AND full_table.ticket_tags NOT LIKE '%send_bulk_sms%' AND replies <= 2 THEN 'two-touch' 
       WHEN full_table.ticket_tags NOT LIKE '%sms_sent_via_api%' AND full_table.ticket_tags NOT LIKE '%send_bulk_sms%' AND replies > 2 THEN 'more than two-touch'
       ELSE NULL
       END AS one_two_touch
  , no_of_tickets
  , order_issues_data
  FROM full_table
  left join fpon_cdm.fpon_cdm_sales_head as sh 
  on cast(sh.order_ref_number as string) = cast(full_table.order_reference_number as string)
  left join zendesk.zendesk_ticket_metrics metrics
  on full_table.ticket_id = metrics.ticket_id
  order by ticket_created_year desc, ticket_created_week desc