CREATE OR REPLACE TABLE fpon_analytics.loyalty_tickets AS (
WITH loyalty_tickets AS (
    SELECT * EXCEPT(channel)
        , CASE WHEN ticket_tags LIKE '%voicecall%' THEN 'call' ELSE channel END AS channel
    FROM (
        SELECT *, ROW_NUMBER() OVER (PARTITION BY ticket_id ORDER BY load_date ASC) AS rnk
        FROM fpon_cdm.zendesk_tickets_weekly_snapshot
        WHERE ticket_tags LIKE '%cr_digital_loyalty___earn_/_redeem_linkpoints%'
        OR ticket_tags LIKE '%cr_digital_loyalty___fpapp_pay_issues%'
        OR ticket_tags LIKE '%cr_group_loyalty__earn_bonus_linkpoints%'
        OR ticket_tags LIKE '%cr_group_loyalty__earn_linkpoints%'
        OR ticket_tags LIKE '%cr_group_loyalty__link_-_others%'
        OR ticket_tags LIKE '%cr_group_loyalty__linkpoints_revaluation%'
        OR ticket_tags LIKE '%cr_group_loyalty__payment_cards%'
        OR ticket_tags LIKE '%group_loyalty__linkpoints_revaluation%'
        OR ticket_tags LIKE '%group_loyalty__earn_bonus_linkpoints%'
        OR ticket_tags LIKE '%group_loyalty__earn_linkpoints%'
        OR ticket_tags LIKE '%group_loyalty__payment_cards%'
        OR ticket_tags LIKE '%group_loyalty__link_-_others%'
        OR ticket_tags LIKE '%digital_loyalty___earn_/_redeem_linkpoints%'
        OR ticket_tags LIKE '%digital_loyalty___fpapp_pay_issues%'
    )
    WHERE rnk = 1
)

SELECT tickets.ticket_id
    , ticket_status
    , ticket_created_date
    , feedback_category_new
    , created_at AS comment_created_at
    , ticket_subject
    , body
FROM loyalty_tickets tickets
LEFT JOIN zendesk.zendesk_ticket_comments comments
ON tickets.ticket_id = comments.ticket_id
ORDER BY tickets.ticket_id, comments.created_at ASC
)