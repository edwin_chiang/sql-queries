create temp table run_date
(
current_date date,
year int64,
week int64
)
;
INSERT INTO run_date
select current_date,
case when wk.week=1 then wk.year-1 else wk.year end as year,
case when wk.week=1 then 52 else wk.week-1 end as week --- change to 53 if leap year, 52 for normal years
from fpon_cdm.calendar as wk -- to obtain FP weeks
where current_date = wk.date
;

INSERT INTO fpon_cdm.zendesk_tickets_feature_snapshot

-- total orders delivered
-- amendments
-- delivery on time %
-- add/missing/wrong
-- damaged
-- other zendesk tickets
-- super perfect order def

----- FFS STORES -----
-- HJEM 52/469
-- HCBP 45/461
-- HSPH 17/476
-- SUN 13/355
-- PGOA 66/351
-- AMK712 42/458
---------------------------------------
------------ OVERALL ------------------
---------------------------------------
with overall_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY','FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('B2B','DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
-- dropoff = 0 and nps_response_category = detractors
overall_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select
total.year, total.week
, 100.00 - (perf.count * 100.0 / total.total_orders) as perc_amendments
, nps_detractors * 100.0 / total.total_orders as perc_detractors
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
, sum(case when sh.nps_response_category = 'detractors' then 1 else 0 end) as nps_detractors
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY', 'FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('B2B', 'DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty - delivered_qty) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('PFC_DELIVERY', 'FFS')
    and order_type in ('B2B', 'DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff = 1
    group by p1.year, p1.week
    ) as perf on perf.year = total.year and perf.week = total.week
)
,
---------------------------------------
---------------- FFS ------------------
---------------------------------------
ffs_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, sum(order_shipping) + sum(surcharge) as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week
)
,
ffs_amendments as
(
select total.year, total.week
, 100.00 - (perf.count * 100.0 / total.total_orders) as perc_amendments
, nps_detractors * 100.0 / total.total_orders as perc_detractors
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
, sum(case when sh.nps_response_category = 'detractors' then 1 else 0 end) as nps_detractors
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty - delivered_qty) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('FFS')
    and order_type in ('DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff = 1
    group by p1.year, p1.week
    ) as perf on perf.year = total.year and perf.week = total.week
)
,
ffs_perc_orders_delivery as
(-- % of orders out late / on time for delivery FOR FFS
select b.year,b.week
,count(case when new_delivery_status in ('On Time') then 1 end) *100.0/count(*) as ontime_perc
,count(case when new_delivery_status in ('Late') then 1 end) *100.0/count(*) as late_perc
,count(case when new_delivery_status in ('Early') then 1 end)*100.0/count(*) as early_perc
from fpon_cdm.ffs_logistics_driver_trips_opm as driver
join fpon_cdm.calendar_full as b on driver.date_of_delivery = b.date
where driver.order_status in ('COMPLETED') and trip_status in ('SUCCESS')
group by b.year,b.week
)
,
---------------------------------------
---------------- PFC ------------------
---------------------------------------
pfc_delivery
as
(
select
wk.year, wk.week
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, sum(order_shipping) + sum(surcharge) as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY','B2B')
  and wk.year >= 2020
  group by wk.year, wk.week
  )
,
pfc_amendments as -- # orders (ordered_qty = delivered_qty) / total # of orders
(
select total.year, total.week
, 100.00 - (perf.count * 100.0 / total.total_orders) as perc_amendments
, nps_detractors * 100.0 / total.total_orders as perc_detractors
from
(select
wk.year, wk.week
, count(sh.order_ref_number) as total_orders
, sum(case when sh.nps_response_category = 'detractors' then 1 else 0 end) as nps_detractors
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('PFC_DELIVERY')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week ) as total
left join
  (
  select distinct p1.year, p1.week, count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    ,order_ref_number
    ,case when (ordered_qty - delivered_qty) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('PFC_DELIVERY')
    and order_type in ('DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff = 1
    group by p1.year, p1.week
    ) as perf on perf.year = total.year and perf.week = total.week
)
,
/* PFC PERC ONTIME */
order_deliver_history as
(
select orderid
,wk.year
,wk.week
, delivery_date
, delivery_time
, delivery_window
, DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)) as delivery_datetime
, TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC') as delivery_datetime2
, TIMESTAMP_ADD(TIMESTAMP(DATETIME(delivery_date, PARSE_TIME("%R",delivery_time)), 'UTC'), interval 15 minute) as calculated_15mins_ear
, DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])) as delivery_datewindow_start
, DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))) as delivery_datewindow_end
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",split(delivery_window, '-')[OFFSET(0)])),'UTC') as delivery_datewindow_startx
, TIMESTAMP(DATETIME(delivery_date,PARSE_TIME("%R",reverse(SPLIT(reverse(delivery_window),'-')[OFFSET(0)]))),'UTC') as delivery_datewindow_endx
, distance_metres
, latitude
, longitude
, status
from
(SELECT * FROM `cds-sc-data-cloud-prod.fpg_sc_ods.fp_gls_tms_order_delivery_history` where delivery_time is not null AND REGEXP_CONTAINS(delivery_time,'[a-zA-Z0-9]') = TRUE) as tms
left join fpon_cdm.fpon_cdm_sales_head as sh on sh.order_ref_number = tms.orderid
LEFT JOIN fpon_cdm.calendar_full as wk -- to obtain FP weeks
  ON (tms.delivery_date) = wk.date
--sh.order_status in ('COMPLETED','DISPATCHED')
group by orderid
, year, week
, order_ship_mode, order_type
, tms.delivery_time, tms.delivery_date, tms.delivery_window, distance_metres, latitude, longitude, status
order by delivery_date desc
)
,
filter1 as
(
select
year
, week
, orderid
, distance_metres
, latitude
, longitude
, delivery_date
, delivery_datetime2
,TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)
, delivery_datewindow_startx
, delivery_datewindow_endx
,status
,case when upper(status) = 'ONTIME' and delivery_datewindow_startx >= TIMESTAMP_ADD(delivery_datetime2, INTERVAL 15 MINUTE) or (distance_metres > 500) then 'LATE'
when upper(status) = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE) then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE_EARLY
,case when status = 'ONTIME' and (distance_metres > 500) then 'LATE'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) and (distance_metres < 500) or distance_metres is null then 'ONTIME'
when status = 'UNSUCCESSFUL' and delivery_datewindow_endx < TIMESTAMP_SUB(delivery_datetime2, INTERVAL 10 MINUTE) or (distance_metres > 500) then 'LATE'
when status = 'LATE' and delivery_datewindow_endx > TIMESTAMP_SUB(delivery_datetime2, INTERVAL 15 MINUTE)  then 'ONTIME'
when status = 'EXPECTED LATE' then 'LATE'
else status end as LATE
from order_deliver_history
)
,
filter2 as
(
select
year
, week
,count(*) as total_orders
,sum(case when LATE_EARLY = 'LATE' then 1 else 0 end) as LATE_EARLY_COUNT
,sum(case when LATE = 'LATE' then 1 else 0 end) as LATE_COUNT
from filter1
group by year, week
)
,
pfc_perc_orders_delivery as
(
select
year
, week
,total_orders
,LATE_COUNT * 100.0 / total_orders as late_perc
, 100.00 - (LATE_COUNT * 100.0 / total_orders) as ontime_perc
,LATE_EARLY_COUNT * 100.0 /total_orders as late_perc_applying_tooearly
from filter2
order by week
)
,
-----------------------------------------------
------------- CONTACT RATIO -------------------
-----------------------------------------------
overall_cr as
(
select
cal.year, cal.week
, count(distinct ticket_id) as total_ticket
, count(distinct ticket_id)*100.0 / overall_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join overall_delivery on overall_delivery.year = cal.year and overall_delivery.week = cal.week
  where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
group by cal.year, cal.week, overall_delivery.total_orders
order by cal.year, cal.week
)
,
overall_feedback as
(
select
cal.year, cal.week
, feedback_category_new
, count(distinct order_ref_number) as total_ticket
, overall_delivery.total_orders
, count(distinct order_ref_number)*100.0 / overall_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join overall_delivery on overall_delivery.year = cal.year and overall_delivery.week = cal.week
where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new in ('order_issues__damaged_items',
'order_issues__late',
'order_issues__missing_items',
'order_issues__no_show',
'order_issues__wrong/additional_items',
'changes_to_orders__delivery_address_amendment',
'fairprice_on__changes_to_orders__amendment_enquiry',
'feedback/suggestion__driver_attitude')
group by cal.year, cal.week, overall_delivery.total_orders, feedback_category_new
order by cal.year, cal.week, feedback_category_new
)
,
-- FFS CONTACT RATIO --
ffs_cr as
(
select
cal.year, cal.week, order_ship_mode
, count(distinct ticket_id) as total_ticket
, count(distinct ticket_id)*100.0 / ffs_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join ffs_delivery on ffs_delivery.year = cal.year and ffs_delivery.week = cal.week
where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and zd.order_ship_mode in ('FFS')
group by cal.year, cal.week, order_ship_mode, ffs_delivery.total_orders
order by cal.year, cal.week, order_ship_mode desc
)
,
ffs_feedback as
(
select
cal.year, cal.week, order_ship_mode, feedback_category_new
, count(distinct order_ref_number) as total_ticket
, count(distinct order_ref_number) *100.0 / ffs_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join ffs_delivery on ffs_delivery.year = cal.year and ffs_delivery.week = cal.week
  where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and feedback_category_new in ('order_issues__damaged_items',
'order_issues__late',
'order_issues__missing_items',
'order_issues__no_show',
'order_issues__wrong/additional_items',
'changes_to_orders__delivery_address_amendment',
'fairprice_on__changes_to_orders__amendment_enquiry',
'feedback/suggestion__driver_attitude')
and order_ship_mode in ('FFS')
group by cal.year, cal.week, order_ship_mode, feedback_category_new,ffs_delivery.total_orders
)
,
-- PFC CONTACT RATIO --
pfc_cr as
(
select
cal.year, cal.week, order_ship_mode
, count(distinct ticket_id) as total_ticket
, count(distinct ticket_id)*100.0 / pfc_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join pfc_delivery on pfc_delivery.year = cal.year and pfc_delivery.week = cal.week
  where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and zd.order_ship_mode in ('PFC_DELIVERY')
group by cal.year, cal.week, order_ship_mode, pfc_delivery.total_orders
)
,
pfc_feedback as -- pfc damaged items, missing, wrong/addtl items
(
select
cal.year, cal.week, order_ship_mode, feedback_category_new
, count(distinct order_ref_number) as total_ticket
, count(distinct order_ref_number) *100.0 / pfc_delivery.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join pfc_delivery on pfc_delivery.year = cal.year and pfc_delivery.week = cal.week
  where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and feedback_category_new in ('order_issues__damaged_items',
'order_issues__late',
'order_issues__missing_items',
'order_issues__no_show',
'order_issues__wrong/additional_items',
'changes_to_orders__delivery_address_amendment',
'fairprice_on__changes_to_orders__amendment_enquiry',
'feedback/suggestion__driver_attitude')
and order_ship_mode in ('PFC_DELIVERY')
group by cal.year, cal.week, order_ship_mode, feedback_category_new,pfc_delivery.total_orders
)
,
---------------------------------------
-------- PERFECT DEFINITION -----------
---------------------------------------
perfect_def as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty - sh.delivered_qty as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when pfc_ontime.LATE in ('ONTIME') THEN 1
  when pfc_ontime.LATE_EARLY IN ('ONTIME') then 1
  when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join fpon_cdm.zendesk_full_table_snapshot as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join filter1 as pfc_ontime on pfc_ontime.orderid = sh.order_ref_number
left join fpon_cdm.ffs_logistics_driver_trips_opm as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('PFC_DELIVERY','FFS')
  and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
,
overall_perf as
(
select
perfect_def.year, perfect_def.week
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ overall_delivery.total_orders as perfect
from perfect_def
left join overall_delivery
on overall_delivery.year = perfect_def.year and overall_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY','FFS')
and order_type in ('B2B','DELIVERY')
group by perfect_def.year, perfect_def.week, overall_delivery.total_orders
order by perfect_def.week asc
)
,
pfc_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ pfc_delivery.total_orders as perfect
from perfect_def
left join pfc_delivery
on pfc_delivery.year = perfect_def.year and pfc_delivery.week = perfect_def.week
where order_ship_mode in ('PFC_DELIVERY')
and order_type in ('DELIVERY')
group by perfect_def.year, perfect_def.week, pfc_delivery.total_orders
,order_ship_mode
order by perfect_def.week asc
)
,
ffs_perf as
(
select
perfect_def.year, perfect_def.week
,order_ship_mode
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ ffs_delivery.total_orders as perfect
from perfect_def
left join ffs_delivery
on ffs_delivery.year = perfect_def.year and ffs_delivery.week = perfect_def.week
where order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
group by perfect_def.year, perfect_def.week, order_ship_mode, ffs_delivery.total_orders
order by perfect_def.week asc
)
,
---------------------------------------
----------- BY FFS STORES -------------
---------------------------------------
ffs_delivery_store
as
(
select
wk.year, wk.week
, store_name
, store_code
, count(distinct sh.order_ref_number) as total_orders
, count(sh.order_ref_number) / 7 as daily_orders
, sum(sh.order_amount - sh.order_discount)/1.07 as total_sales
, sum(order_shipping) + sum(surcharge) as extra_fees
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
    ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week, sh.store_name,store_code
  )
,
ffs_amendments_store as
(
select total.year, total.week, total.store_name
, total.store_code
, 100.00 - (perf.count * 100.0 / total.total_orders) as perc_amendments
, nps_detractors * 100.0 / total.total_orders as perc_detractors
from
(select distinct
wk.year, wk.week, store_name,store_code
, count(sh.order_ref_number) as total_orders
, sum(case when sh.nps_response_category = 'detractors' then 1 else 0 end) as nps_detractors
from fpon_cdm.fpon_cdm_sales_head as sh
LEFT JOIN fpon_cdm.calendar_full as wk
      ON sh.date_of_delivery = wk.date
    where order_ship_mode in ('FFS')
    and order_status not in ('CANCELLED')
    and order_type in ('DELIVERY')
  and wk.year >= 2020
  group by wk.year, wk.week, store_name,store_code) as total
left join
  (
  select distinct p1.year, p1.week, store_name, store_code,count(distinct order_ref_number) as count
  from
    (select
    wk.year
    ,wk.week
    , store_name, store_code
    ,order_ref_number
    ,case when (ordered_qty - delivered_qty) = 0 then 1 else null end as no_dropoff
    from fpon_cdm.fpon_cdm_sales_head as sh
    LEFT JOIN fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
    where order_ship_mode in ('FFS')
    and order_type in ('DELIVERY')
    and order_status not in ('CANCELLED')
  and wk.year >= 2020
    ) as p1
    where no_dropoff = 1
    group by p1.year, p1.week, store_name,store_code
    ) as perf on perf.year = total.year and perf.week = total.week and perf.store_code = total.store_code
)
,
ffs_perc_orders_delivery_store as
(-- % of orders out late / on time for delivery FOR FFS
select b.year,b.week, store_name, store_code as store_code
,count(case when new_delivery_status in ('On Time') then 1 end) *100.0/count(*) as ontime_perc
,count(case when new_delivery_status in ('Late') then 1 end) *100.0/count(*) as late_perc
,count(case when new_delivery_status in ('Early') then 1 end)*100.0/count(*) as early_perc
from fpon_cdm.ffs_logistics_driver_trips_opm as driver
join fpon_cdm.calendar_full as b on driver.date_of_delivery = b.date
where driver.order_status in ('COMPLETED') and trip_status in ('SUCCESS')
group by b.year,b.week, store_name,store_code
)
,
ffs_cr_store as
(
select
cal.year, cal.week, order_ship_mode, ffs_delivery_store.store_name, ffs_delivery_store.store_code
, count(distinct ticket_id) as total_ticket
, count(distinct ticket_id)*100.0 / ffs_delivery_store.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join ffs_delivery_store on ffs_delivery_store.year = cal.year and ffs_delivery_store.week = cal.week
  and ffs_delivery_store.store_name = zd.store_name
where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and zd.order_ship_mode in ('FFS')
group by cal.year, cal.week, order_ship_mode, ffs_delivery_store.store_name, ffs_delivery_store.store_code, ffs_delivery_store.total_orders
order by cal.year, cal.week, order_ship_mode, ffs_delivery_store.store_name desc
)
,
ffs_feedback_store as -- ffs damaged, missing, late, no show, wrong/addtl items
(
select
cal.year, cal.week, order_ship_mode, ffs_delivery_store.store_name, ffs_delivery_store.store_code
,feedback_category_new
, count(distinct order_ref_number) as total_ticket
, count(distinct order_ref_number) *100.0 / ffs_delivery_store.total_orders as contact_ratio
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_cdm.calendar_full as cal on cal.date = zd.ticket_created_date
left join ffs_delivery_store on ffs_delivery_store.year = cal.year and ffs_delivery_store.week = cal.week
  and ffs_delivery_store.store_name = zd.store_name
where ticket_status in ('solved', 'hold', 'open', 'pending', 'new')
and feedback_category_new is not null
and feedback_category_new in ('order_issues__damaged_items',
'order_issues__late',
'order_issues__missing_items',
'order_issues__no_show',
'order_issues__wrong/additional_items',
'changes_to_orders__delivery_address_amendment',
'fairprice_on__changes_to_orders__amendment_enquiry',
'feedback/suggestion__driver_attitude')
and order_ship_mode in ('FFS')
group by cal.year, cal.week, order_ship_mode, ffs_delivery_store.store_name, ffs_delivery_store.store_code, feedback_category_new,ffs_delivery_store.total_orders
)
,
perfect_def_store as (
select * from
(
select
wk.year, wk.week
, sh.order_ref_number
, sh.order_ship_mode
, sh.store_name
, sh.store_code
, sh.order_type
--, case when sh.nps_response_category = 'detractors' then 1 else 0 end as nps_response_ind
, sh.ordered_qty - sh.delivered_qty as dropoff
, zd.ticket_id
, zd.feedback_category_new
, case when ffs_ontime.delivery_status in ('On Time') then 1
  else 0 end as ontime
from fpon_cdm.fpon_cdm_sales_head as sh
left join fpon_cdm.calendar_full as wk on wk.date = sh.date_of_delivery
left join fpon_cdm.zendesk_full_table_snapshot as zd on cast(zd.order_ref_number as string) = sh.order_ref_number
left join fpon_cdm.dim_ffs_logistics_driver_trips as ffs_ontime
on ffs_ontime.order_ref_number = sh.order_ref_number
where
sh.order_status not in ('CANCELLED')
and sh.order_ship_mode in ('FFS')
and wk.year >= 2020
)
where ontime = 1 and dropoff = 0 and ticket_id is null
)
,
ffs_perf_store as
(
select
perfect_def_store.year, perfect_def_store.week
,order_ship_mode
, ffs_delivery_store.store_name
, ffs_delivery_store.store_code
, count(order_ref_number) as perf_order
, count(order_ref_number) * 100.00/ ffs_delivery_store.total_orders as perfect
from perfect_def_store
left join ffs_delivery_store
on ffs_delivery_store.year = perfect_def_store.year and ffs_delivery_store.week = perfect_def_store.week and ffs_delivery_store.store_code = perfect_def_store.store_code
where order_ship_mode in ('FFS')
and order_type in ('DELIVERY')
group by perfect_def_store.year, perfect_def_store.week, order_ship_mode, ffs_delivery_store.store_name, ffs_delivery_store.store_code,ffs_delivery_store.total_orders
order by perfect_def_store.week, ffs_delivery_store.store_name asc
)

select * from
(
select
concat('Actuals',cast(overall_delivery.year as string), cast(overall_delivery.week as string), 'TOTAL_ONLINE') as col_id
,overall_delivery.year
, overall_delivery.week
, 'TOTAL_ONLINE' as group_name
, null as store_name
, overall_delivery.total_orders as orders_delivered
, overall_cr.total_ticket as total_ticket
, ((ffs_perc_orders_delivery.late_perc*ffs_delivery.total_orders) + (pfc_perc_orders_delivery.late_perc*pfc_delivery.total_orders))/overall_delivery.total_orders as late_delivery_tms
, overall_cr.contact_ratio as contact_ratio
, overall_cs.amendment_enq_tickets
, overall_cs.amendment_enq_cr
, overall_cs.damage_tickets
, overall_cs.damage_cr
, overall_cs.delivery_add_tickets
, overall_cs.delivery_add_cr
, overall_cs.late_tickets
, overall_cs.late_cr
, overall_cs.missing_tickets
, overall_cs.missing_cr
, overall_cs.noshow_tickets
, overall_cs.noshow_cr
, overall_cs.wrongadd_tickets
, overall_cs.wrongadd_cr
, overall_cs.driver_attitude_tickets
, overall_cs.driver_attitude_cr
, overall_cs.missing_tickets + overall_cs.wrongadd_tickets as addmissingwrong_tickets
, overall_cs.missing_cr + overall_cs.wrongadd_cr as addmissingwrong_cr
, overall_cs.damage_tickets + overall_cs.missing_tickets + overall_cs.wrongadd_tickets + overall_cs.driver_attitude_tickets + overall_cs.noshow_tickets + overall_cs.late_tickets AS customer_tickets
, overall_cs.damage_cr + overall_cs.missing_cr + overall_cs.wrongadd_cr + overall_cs.driver_attitude_cr + overall_cs.noshow_cr + overall_cs.late_cr AS customer_cr
, overall_cr.total_ticket - (overall_cs.damage_tickets + overall_cs.missing_tickets + overall_cs.wrongadd_tickets) as other_tickets
, overall_cr.contact_ratio - (overall_cs.damage_cr + overall_cs.missing_cr + overall_cs.wrongadd_cr) as other_tickets_cr
, overall_cr.total_ticket - (overall_cs.damage_tickets + overall_cs.missing_tickets + overall_cs.wrongadd_tickets + overall_cs.driver_attitude_tickets + overall_cs.noshow_tickets + overall_cs.late_tickets) as other_tickets_mbr
, overall_cr.contact_ratio - (overall_cs.damage_cr + overall_cs.missing_cr + overall_cs.wrongadd_cr + overall_cs.driver_attitude_cr + overall_cs.noshow_cr + overall_cs.late_cr) as other_tickets_cr_mbr
from overall_delivery
left join ffs_delivery on overall_delivery.year = ffs_delivery.year and overall_delivery.week = ffs_delivery.week
left join pfc_delivery on overall_delivery.year = pfc_delivery.year and overall_delivery.week = pfc_delivery.week
left join ffs_perc_orders_delivery on overall_delivery.year = ffs_perc_orders_delivery.year and overall_delivery.week = ffs_perc_orders_delivery.week
left join pfc_perc_orders_delivery on overall_delivery.year = pfc_perc_orders_delivery.year and overall_delivery.week = pfc_perc_orders_delivery.week
left join overall_cr on overall_delivery.year = overall_cr.year and overall_delivery.week = overall_cr.week
left join
    (select feedback.year, feedback.week
      , feedback.total_ticket as amendment_enq_tickets
      , feedback.contact_ratio as amendment_enq_cr
      , damage.total_ticket as damage_tickets
      , damage.contact_ratio as damage_cr
      , delivery_add.total_ticket as delivery_add_tickets
      , delivery_add.contact_ratio as delivery_add_cr
      , late.total_ticket as late_tickets
      , late.contact_ratio as late_cr
      , missing.total_ticket as missing_tickets
      , missing.contact_ratio as missing_cr
      , noshow.total_ticket as noshow_tickets
      , noshow.contact_ratio as noshow_cr
      , wrongadd.total_ticket as wrongadd_tickets
      , wrongadd.contact_ratio as wrongadd_cr
      , driver_attitude.total_ticket as driver_attitude_tickets
      , driver_attitude.contact_ratio as driver_attitude_cr
    from overall_feedback as feedback
    left join (select * from overall_feedback where feedback_category_new = 'order_issues__damaged_items') as damage on feedback.year = damage.year and feedback.week = damage.week
    left join (select * from overall_feedback where feedback_category_new = 'changes_to_orders__delivery_address_amendment') as delivery_add on feedback.year = delivery_add.year and feedback.week = delivery_add.week
    left join (select * from overall_feedback where feedback_category_new = 'order_issues__late') as late on feedback.year = late.year and feedback.week = late.week
    left join (select * from overall_feedback where feedback_category_new = 'order_issues__missing_items') as missing on feedback.year = missing.year and feedback.week = missing.week
    left join (select * from overall_feedback where feedback_category_new = 'order_issues__no_show') as noshow on feedback.year = noshow.year and feedback.week = noshow.week
    left join (select * from overall_feedback where feedback_category_new = 'order_issues__wrong/additional_items') as wrongadd on feedback.year = wrongadd.year and feedback.week = wrongadd.week
    left join (select * from overall_feedback where feedback_category_new = 'feedback/suggestion__driver_attitude') as driver_attitude on feedback.year = driver_attitude.year and feedback.week = driver_attitude.week
    where feedback.feedback_category_new = 'fairprice_on__changes_to_orders__amendment_enquiry'
    ) as overall_cs on overall_cs.year = overall_delivery.year and overall_cs.week = overall_delivery.week

UNION ALL

select
concat('Actuals',cast(pfc_delivery.year as string), cast(pfc_delivery.week as string), 'PFC') as col_id
,pfc_delivery.year
,  pfc_delivery.week
, 'PFC' as group_name
, null as store_name
, pfc_delivery.total_orders as orders_delivered
, pfc_cr.total_ticket as total_ticket
, pfc_perc_orders_delivery.late_perc as late_delivery_tms
, pfc_cr.contact_ratio as contact_ratio
, pfc_cs.amendment_enq_tickets
, pfc_cs.amendment_enq_cr
, pfc_cs.damage_tickets
, pfc_cs.damage_cr
, pfc_cs.delivery_add_tickets
, pfc_cs.delivery_add_cr
, pfc_cs.late_tickets
, pfc_cs.late_cr
, pfc_cs.missing_tickets
, pfc_cs.missing_cr
, pfc_cs.noshow_tickets
, pfc_cs.noshow_cr
, pfc_cs.wrongadd_tickets
, pfc_cs.wrongadd_cr
, pfc_cs.driver_attitude_tickets
, pfc_cs.driver_attitude_cr
, pfc_cs.missing_tickets + pfc_cs.wrongadd_tickets as addmissingwrong_tickets
, pfc_cs.missing_cr + pfc_cs.wrongadd_cr as addmissingwrong_cr
, pfc_cs.damage_tickets + pfc_cs.missing_tickets + pfc_cs.wrongadd_tickets + pfc_cs.driver_attitude_tickets + pfc_cs.noshow_tickets + pfc_cs.late_tickets AS customer_tickets
, pfc_cs.damage_cr + pfc_cs.missing_cr + pfc_cs.wrongadd_cr + pfc_cs.driver_attitude_cr + pfc_cs.noshow_cr + pfc_cs.late_cr AS customer_cr
, pfc_cr.total_ticket - (pfc_cs.damage_tickets + pfc_cs.missing_tickets + pfc_cs.wrongadd_tickets) as other_tickets
, pfc_cr.contact_ratio - (pfc_cs.damage_cr + pfc_cs.missing_cr + pfc_cs.wrongadd_cr) as other_tickets_cr
, pfc_cr.total_ticket - (pfc_cs.damage_tickets + pfc_cs.missing_tickets + pfc_cs.wrongadd_tickets + pfc_cs.driver_attitude_tickets + pfc_cs.noshow_tickets + pfc_cs.late_tickets) as other_tickets_mbr
, pfc_cr.contact_ratio - (pfc_cs.damage_cr + pfc_cs.missing_cr + pfc_cs.wrongadd_cr + pfc_cs.driver_attitude_cr + pfc_cs.noshow_cr + pfc_cs.late_cr) as other_tickets_cr_mbr
from pfc_delivery
left join pfc_perc_orders_delivery on pfc_delivery.year = pfc_perc_orders_delivery.year and pfc_delivery.week = pfc_perc_orders_delivery.week
left join pfc_cr on pfc_delivery.year = pfc_cr.year and pfc_delivery.week = pfc_cr.week
left join
    (select feedback.year, feedback.week
      , feedback.total_ticket as amendment_enq_tickets
      , feedback.contact_ratio as amendment_enq_cr
      , damage.total_ticket as damage_tickets
      , damage.contact_ratio as damage_cr
      , delivery_add.total_ticket as delivery_add_tickets
      , delivery_add.contact_ratio as delivery_add_cr
      , late.total_ticket as late_tickets
      , late.contact_ratio as late_cr
      , missing.total_ticket as missing_tickets
      , missing.contact_ratio as missing_cr
      , noshow.total_ticket as noshow_tickets
      , noshow.contact_ratio as noshow_cr
      , wrongadd.total_ticket as wrongadd_tickets
      , wrongadd.contact_ratio as wrongadd_cr
      , driver_attitude.total_ticket as driver_attitude_tickets
      , driver_attitude.contact_ratio as driver_attitude_cr
    from pfc_feedback as feedback
    left join (select * from pfc_feedback where feedback_category_new = 'order_issues__damaged_items') as damage on feedback.year = damage.year and feedback.week = damage.week
    left join (select * from pfc_feedback where feedback_category_new = 'changes_to_orders__delivery_address_amendment') as delivery_add on feedback.year = delivery_add.year and feedback.week = delivery_add.week
    left join (select * from pfc_feedback where feedback_category_new = 'order_issues__late') as late on feedback.year = late.year and feedback.week = late.week
    left join (select * from pfc_feedback where feedback_category_new = 'order_issues__missing_items') as missing on feedback.year = missing.year and feedback.week = missing.week
    left join (select * from pfc_feedback where feedback_category_new = 'order_issues__no_show') as noshow on feedback.year = noshow.year and feedback.week = noshow.week
    left join (select * from pfc_feedback where feedback_category_new = 'order_issues__wrong/additional_items') as wrongadd on feedback.year = wrongadd.year and feedback.week = wrongadd.week
    left join (select * from pfc_feedback where feedback_category_new = 'feedback/suggestion__driver_attitude') as driver_attitude on feedback.year = driver_attitude.year and feedback.week = driver_attitude.week
    where feedback.feedback_category_new = 'fairprice_on__changes_to_orders__amendment_enquiry'
    ) as pfc_cs on pfc_cs.year = pfc_delivery.year and pfc_cs.week = pfc_delivery.week

UNION ALL

select
concat('Actuals',cast(ffs_delivery.year as string), cast(ffs_delivery.week as string), 'FFS') as col_id
,ffs_delivery.year
, ffs_delivery.week
, 'FFS' as group_name
, null as store_name
, ffs_delivery.total_orders as orders_delivered
, ffs_cr.total_ticket as total_ticket
, ffs_perc_orders_delivery.late_perc as late_delivery_tms
, ffs_cr.contact_ratio as contact_ratio
, ffs_cs.amendment_enq_tickets
, ffs_cs.amendment_enq_cr
, ffs_cs.damage_tickets
, ffs_cs.damage_cr
, ffs_cs.delivery_add_tickets
, ffs_cs.delivery_add_cr
, ffs_cs.late_tickets
, ffs_cs.late_cr
, ffs_cs.missing_tickets
, ffs_cs.missing_cr
, ffs_cs.noshow_tickets
, ffs_cs.noshow_cr
, ffs_cs.wrongadd_tickets
, ffs_cs.wrongadd_cr
, ffs_cs.driver_attitude_tickets
, ffs_cs.driver_attitude_cr
, ffs_cs.missing_tickets + ffs_cs.wrongadd_tickets as addmissingwrong_tickets
, ffs_cs.missing_cr + ffs_cs.wrongadd_cr as addmissingwrong_cr
, ffs_cs.damage_tickets + ffs_cs.missing_tickets + ffs_cs.wrongadd_tickets + ffs_cs.driver_attitude_tickets + ffs_cs.noshow_tickets + ffs_cs.late_tickets AS customer_tickets
, ffs_cs.damage_cr + ffs_cs.missing_cr + ffs_cs.wrongadd_cr + ffs_cs.driver_attitude_cr + ffs_cs.noshow_cr + ffs_cs.late_cr AS customer_cr
, ffs_cr.total_ticket - (ffs_cs.damage_tickets + ffs_cs.missing_tickets + ffs_cs.wrongadd_tickets) as other_tickets
, ffs_cr.contact_ratio - (ffs_cs.damage_cr + ffs_cs.missing_cr + ffs_cs.wrongadd_cr) as other_tickets_cr
, ffs_cr.total_ticket - (ffs_cs.damage_tickets + ffs_cs.missing_tickets + ffs_cs.wrongadd_tickets + ffs_cs.driver_attitude_tickets + ffs_cs.noshow_tickets + ffs_cs.late_tickets) as other_tickets_mbr
, ffs_cr.contact_ratio - (ffs_cs.damage_cr + ffs_cs.missing_cr + ffs_cs.wrongadd_cr + ffs_cs.driver_attitude_cr + ffs_cs.noshow_cr + ffs_cs.late_cr) as other_tickets_cr_mbr
from ffs_delivery
left join ffs_perc_orders_delivery on ffs_delivery.year = ffs_perc_orders_delivery.year and ffs_delivery.week = ffs_perc_orders_delivery.week
left join ffs_cr on ffs_delivery.year = ffs_cr.year and ffs_delivery.week = ffs_cr.week
left join
    (select feedback.year, feedback.week
      , feedback.total_ticket as amendment_enq_tickets
      , feedback.contact_ratio as amendment_enq_cr
      , damage.total_ticket as damage_tickets
      , damage.contact_ratio as damage_cr
      , delivery_add.total_ticket as delivery_add_tickets
      , delivery_add.contact_ratio as delivery_add_cr
      , late.total_ticket as late_tickets
      , late.contact_ratio as late_cr
      , missing.total_ticket as missing_tickets
      , missing.contact_ratio as missing_cr
      , noshow.total_ticket as noshow_tickets
      , noshow.contact_ratio as noshow_cr
      , wrongadd.total_ticket as wrongadd_tickets
      , wrongadd.contact_ratio as wrongadd_cr
      , driver_attitude.total_ticket as driver_attitude_tickets
      , driver_attitude.contact_ratio as driver_attitude_cr
    from ffs_feedback as feedback
    left join (select * from ffs_feedback where feedback_category_new = 'order_issues__damaged_items') as damage on feedback.year = damage.year and feedback.week = damage.week
    left join (select * from ffs_feedback where feedback_category_new = 'changes_to_orders__delivery_address_amendment') as delivery_add on feedback.year = delivery_add.year and feedback.week = delivery_add.week
    left join (select * from ffs_feedback where feedback_category_new = 'order_issues__late') as late on feedback.year = late.year and feedback.week = late.week
    left join (select * from ffs_feedback where feedback_category_new = 'order_issues__missing_items') as missing on feedback.year = missing.year and feedback.week = missing.week
    left join (select * from ffs_feedback where feedback_category_new = 'order_issues__no_show') as noshow on feedback.year = noshow.year and feedback.week = noshow.week
    left join (select * from ffs_feedback where feedback_category_new = 'order_issues__wrong/additional_items') as wrongadd on feedback.year = wrongadd.year and feedback.week = wrongadd.week
    left join (select * from ffs_feedback where feedback_category_new = 'feedback/suggestion__driver_attitude') as driver_attitude on feedback.year = driver_attitude.year and feedback.week = driver_attitude.week
    where feedback.feedback_category_new = 'fairprice_on__changes_to_orders__amendment_enquiry'
    ) as ffs_cs on ffs_cs.year = ffs_delivery.year and ffs_cs.week = ffs_delivery.week

UNION ALL

select
distinct concat('Actuals',cast(ffs_delivery_store.year as string), cast(ffs_delivery_store.week as string), UPPER(ffs_delivery_store.store_code)) as col_id
,ffs_delivery_store.year
, ffs_delivery_store.week
, UPPER(ffs_delivery_store.store_code) as group_name
, ffs_delivery_store.store_name
, ffs_delivery_store.total_orders as orders_delivered
, ffs_cr_store.total_ticket as total_ticket
, ffs_perc_orders_delivery_store.late_perc as late_delivery_tms
, ffs_cr_store.contact_ratio as contact_ratio
, ffs_cs_store.amendment_enq_tickets
, ffs_cs_store.amendment_enq_cr
, ffs_cs_store.damage_tickets
, ffs_cs_store.damage_cr
, ffs_cs_store.delivery_add_tickets
, ffs_cs_store.delivery_add_cr
, ffs_cs_store.late_tickets
, ffs_cs_store.late_cr
, ffs_cs_store.missing_tickets
, ffs_cs_store.missing_cr
, ffs_cs_store.noshow_tickets
, ffs_cs_store.noshow_cr
, ffs_cs_store.wrongadd_tickets
, ffs_cs_store.wrongadd_cr
, ffs_cs_store.driver_attitude_tickets
, ffs_cs_store.driver_attitude_cr
, ffs_cs_store.missing_tickets + ffs_cs_store.wrongadd_tickets as addmissingwrong_tickets
, ffs_cs_store.missing_cr + ffs_cs_store.wrongadd_cr as addmissingwrong_cr
, ffs_cs_store.damage_tickets + ffs_cs_store.missing_tickets + ffs_cs_store.wrongadd_tickets + ffs_cs_store.driver_attitude_tickets + ffs_cs_store.noshow_tickets + ffs_cs_store.late_tickets AS customer_tickets
, ffs_cs_store.damage_cr + ffs_cs_store.missing_cr + ffs_cs_store.wrongadd_cr + ffs_cs_store.driver_attitude_cr + ffs_cs_store.noshow_cr + ffs_cs_store.late_cr AS customer_cr
, ffs_cr_store.total_ticket - (ffs_cs_store.damage_tickets + ffs_cs_store.missing_tickets + ffs_cs_store.wrongadd_tickets) as other_tickets
, ffs_cr_store.contact_ratio - (ffs_cs_store.damage_cr + ffs_cs_store.missing_cr + ffs_cs_store.wrongadd_cr) as other_tickets_cr
, ffs_cr_store.total_ticket - (ffs_cs_store.damage_tickets + ffs_cs_store.missing_tickets + ffs_cs_store.wrongadd_tickets + ffs_cs_store.driver_attitude_tickets + ffs_cs_store.noshow_tickets + ffs_cs_store.late_tickets) as other_tickets_mbr
, ffs_cr_store.contact_ratio - (ffs_cs_store.damage_cr + ffs_cs_store.missing_cr + ffs_cs_store.wrongadd_cr + ffs_cs_store.driver_attitude_cr + ffs_cs_store.noshow_cr + ffs_cs_store.late_cr) as other_tickets_cr_mbr
from ffs_delivery_store
left join ffs_perc_orders_delivery_store on ffs_delivery_store.year = ffs_perc_orders_delivery_store.year and ffs_delivery_store.week = ffs_perc_orders_delivery_store.week and ffs_delivery_store.store_code = ffs_perc_orders_delivery_store.store_code
left join ffs_cr_store on ffs_delivery_store.year = ffs_cr_store.year and ffs_delivery_store.week = ffs_cr_store.week and ffs_delivery_store.store_code = ffs_cr_store.store_code
left join
    (select feedback.year, feedback.week, feedback.store_name, feedback.store_code
      , COALESCE(amendment.total_ticket,0) as amendment_enq_tickets
      , COALESCE(amendment.contact_ratio,0) as amendment_enq_cr
      , COALESCE(damage.total_ticket,0) as damage_tickets
      , COALESCE(damage.contact_ratio,0) as damage_cr
      , COALESCE(delivery_add.total_ticket,0) as delivery_add_tickets
      , COALESCE(delivery_add.contact_ratio,0) as delivery_add_cr
      , COALESCE(late.total_ticket,0) as late_tickets
      , COALESCE(late.contact_ratio,0) as late_cr
      , COALESCE(missing.total_ticket,0) as missing_tickets
      , COALESCE(missing.contact_ratio,0) as missing_cr
      , COALESCE(noshow.total_ticket,0) as noshow_tickets
      , COALESCE(noshow.contact_ratio,0) as noshow_cr
      , COALESCE(wrongadd.total_ticket,0) as wrongadd_tickets
      , COALESCE(wrongadd.contact_ratio,0) as wrongadd_cr
      , COALESCE(driver_attitude.total_ticket,0) as driver_attitude_tickets
      , COALESCE(driver_attitude.contact_ratio,0) as driver_attitude_cr
    from ffs_feedback_store as feedback
    left join (select * from ffs_feedback_store where feedback_category_new = 'order_issues__damaged_items') as damage on feedback.year = damage.year and feedback.week = damage.week and feedback.store_code = damage.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'changes_to_orders__delivery_address_amendment') as delivery_add on feedback.year = delivery_add.year and feedback.week = delivery_add.week and feedback.store_code = delivery_add.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'order_issues__late') as late on feedback.year = late.year and feedback.week = late.week and feedback.store_code = late.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'order_issues__missing_items') as missing on feedback.year = missing.year and feedback.week = missing.week and feedback.store_code = missing.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'order_issues__no_show') as noshow on feedback.year = noshow.year and feedback.week = noshow.week and feedback.store_code = noshow.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'order_issues__wrong/additional_items') as wrongadd on feedback.year = wrongadd.year and feedback.week = wrongadd.week and feedback.store_code = wrongadd.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'fairprice_on__changes_to_orders__amendment_enquiry') as amendment on feedback.year = amendment.year and feedback.week = amendment.week and feedback.store_code = amendment.store_code
    left join (select * from ffs_feedback_store where feedback_category_new = 'feedback/suggestion__driver_attitude') as driver_attitude on feedback.year = driver_attitude.year and feedback.week = driver_attitude.week and feedback.store_code = driver_attitude.store_code    
    ) as ffs_cs_store on ffs_cs_store.year = ffs_delivery_store.year and ffs_cs_store.week = ffs_delivery_store.week and ffs_cs_store.store_code = ffs_delivery_store.store_code
)
where year in (select year from run_date) and week in (select week from run_date)

order by year, week, group_name