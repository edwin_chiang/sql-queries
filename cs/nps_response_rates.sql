CREATE OR REPLACE TABLE fpon_analytics.nps_response_rates AS (
with total_responses AS (
  SELECT year, week, business_type, store_name, COUNT(DISTINCT order_ref_number) AS no_of_respondents
  FROM fpon_cdm.dim_fpon_cx_nps_detail
  WHERE store_name IS NOT NULL
  AND status = 'Complete'
  GROUP BY year, week, business_type, store_name
  ORDER BY year, week, business_type, store_name
),

zs_null AS (
	SELECT order_ref_number
	FROM fpon_cdm.fpon_cdm_sales_head
	WHERE order_ship_mode_level2 IS NULL 
	AND source = 'ZopSmart'
),

total_orders AS (
  SELECT year, week, 
         CASE 
	   	     WHEN order_type = 'B2B' then 'B2B' 
	   	     WHEN (order_type = 'DELIVERY' or order_type = 'PICKUP') and order_ship_mode_level2 = 'PFC_DELIVERY' THEN 'PFC'
	   	     WHEN order_ship_mode_level2 = 'FFS Normal' THEN 'FFS Normal'
	   	     WHEN order_ship_mode_level2 = 'FFS Darkstore' THEN 'FFS Darkstore'
		       WHEN order_ship_mode = 'PFC_DELIVERY' AND order_ship_mode_level2 IS NULL THEN 'PFC'
		       WHEN order_ship_mode = 'FFS' AND order_ship_mode_level2 IS NULL THEN 'FFS Normal'
           WHEN order_ship_mode = 'BULK' AND order_ship_mode_level2 IS NULL THEN 'PFC'
         END AS business_type,
         store_name, COUNT(DISTINCT order_id) AS no_of_orders
  FROM (
    SELECT year, week, order_type, order_ship_mode, order_ship_mode_level2, order_id, store_name 
    FROM fpon_cdm.fpon_cdm_sales_head a 
    JOIN fpon_cdm.calendar b
    ON a.date_of_delivery = b.date
    WHERE order_status not in ('CANCELLED')
    AND store_name IS NOT NULL
  )
  GROUP BY year, week, business_type, store_name
)

SELECT a.year, a.week, a.business_type, a.store_name, no_of_respondents, no_of_orders
FROM total_orders a 
JOIN total_responses b
ON a.year = b.year AND a.week = b.week AND a.business_type = b.business_type AND a.store_name = b.store_name
WHERE b.business_type IS NOT NULL
order by a.year desc, a.week desc
)