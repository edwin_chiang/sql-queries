CREATE OR REPLACE TABLE fpon_analytics.zendesk_sku_reported AS (
WITH items AS (
  SELECT natural_year, year,week, month,feedback_category_new,date_of_delivery,negligent_party, damage_cause, order_ship_mode,order_ship_mode_level2, order_type, store_code ,dept_name, class_name, subclass_name, product_sku, product_name, 
  sum(delivered_qty) AS no_of_products, 
  count(distinct ticket_id) as ticket_count
  FROM (
  SELECT natural_year,year,week,month,date_of_delivery,ticket_id, feedback_category_new,ticket_created_date, negligent_party, damage_cause, pdt_type, store_code, order_ship_mode, order_type, product_sku,
         department_desc AS dept_name, class_desc AS class_name, subclass_desc AS subclass_name, a.product_name, is_marketplace, delivered_qty, order_ship_mode_level2
  FROM fpon_cdm.zendesk_sku_daily a
  JOIN fpon_cdm.calendar_full b 
  ON a.date_of_delivery = b.date
  # WHERE LOWER(feedback_category_new) LIKE '%damaged%'
  WHERE 1=1
  AND ticket_created_date >= '2020-01-01'
  ORDER BY ticket_id
  )
  WHERE product_name IS NOT NULL AND store_code IS NOT NULL
  GROUP BY product_name, dept_name, class_name, subclass_name, order_ship_mode, order_type, store_code,negligent_party, damage_cause, year, week,product_sku,month,date_of_delivery,natural_year,feedback_category_new,order_ship_mode_level2
  ORDER BY no_of_products DESC
),

products_sold_by_day AS (
  SELECT date_of_delivery, order_ship_mode_level2, store_client_id, product_sku, sap_department, sap_class, sap_subclass
      , product_l1_category_name
      , product_l2_category_name
      , product_l3_category_name
      , SUM(delivered_qty) AS delivered_qty 
  FROM `fairprice-bigquery.fpon_cdm.fpon_cdm_sales_line`
  WHERE date_of_delivery >= '2020-01-01'
  AND payment_status NOT IN ('CANCELLED','FAILED')
  AND order_item_status <> 'CANCELLED'
  AND order_status <> 'CANCELLED'
  AND order_ship_mode NOT IN ('OFFLINE','CNC') 
  AND order_type <> 'OFFLINE' 
  GROUP BY date_of_delivery, order_ship_mode_level2, product_sku, sap_department, sap_class, sap_subclass,store_client_id      
      , product_l1_category_name
      , product_l2_category_name
      , product_l3_category_name
)

SELECT a.*
    , product_l1_category_name
    , product_l2_category_name
    , product_l3_category_name
    , CASE WHEN b.product_sku LIKE '900%' THEN 1 ELSE 0 END AS is_marketplace
    , b.delivered_qty 
FROM (
  SELECT natural_year,year,week,month,feedback_category_new,date_of_delivery,store_code, order_ship_mode_level2, product_sku,name
        , cast(concat(cast(year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week
        , cast(concat(cast(natural_year as string),case when month < 10 then '0' else '' end ,cast(month as string)) as int64) as year_month,
         CASE 
          WHEN UPPER(name) LIKE '%DS%' THEN 'FFS Darkstore'
          WHEN UPPER(name) LIKE '%PFC%' THEN 'PFC'
          WHEN UPPER(name) LIKE '%FFS%' THEN 'FFS Normal'
         END AS fulfillment
        , order_type, dept_name, class_name, subclass_name, product_name, negligent_party, damage_cause, no_of_products, ticket_count
  FROM items a
  JOIN fpon_cdm.dim_fpon_store_meta b
  ON a.store_code = b.client_store_id
  AND name NOT IN ('VivoCity','PUNGGOL DRIVE OASIS','FairPrice Xtra JEM')
) a 
JOIN products_sold_by_day b
ON a.date_of_delivery = b.date_of_delivery
AND a.order_ship_mode_level2 = b.order_ship_mode_level2
AND a.product_sku = b.product_sku
AND a.store_code = b.store_client_id
WHERE a.date_of_delivery <= CURRENT_DATE()
ORDER BY year, week, no_of_products DESC
)