SELECT a.*,created_at,b.body as first_comment
FROM (
  SELECT *, ROW_NUMBER() OVER (PARTITION BY ticket_id ORDER BY load_hour DESC) AS rnk
  FROM fpon_cdm.zendesk_tickets_weekly_snapshot 
  WHERE snapshot_date = CURRENT_DATE()
  AND feedback_category_new LIKE '%digital%'
  AND digital_club_sub_nature is not null 
  AND digital_club_sub_nature <> ''
  ) a
JOIN (
  SELECT *, RANK() OVER (PARTITION BY ticket_id ORDER BY created_at ASC) AS rnk1 
  FROM zendesk.zendesk_ticket_comments b 
  WHERE LOWER(b.body) NOT LIKE '%call from%'
  ) b
ON a.ticket_id = b.ticket_id
WHERE rnk = 1
AND (channel = 'chat' AND b.body LIKE '%Customer Service%')
OR (channel <> 'chat' AND rnk1 = 1)