CREATE OR REPLACE TABLE fpon_cdm.dim_fpon_cx_nps_detail AS (
with pfc_ffs as (
select 
  year
  ,week
  ,cast(concat(cast(year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week
  ,'Complete' AS status
  ,t3.respondent_id
  ,t3.collector_id
  ,DATETIME(t3.start_date) AS start_date
  ,DATETIME(t3.end_date) AS end_date
  ,t3.ip_address
  ,t3.response
  ,t3.mobile_app
  ,t3.website
  ,t3.product_price
  ,t3.product_assortment
  ,t3.product_quality
  ,t3.stock_availability
  ,t3.delivery_collection_experience
  ,t3.delivery_timeliness_collection_waiting_time
  ,t3.payment_options
  ,t3.customer_service
  ,t3.loyalty_program
  ,t3.others as comments
  ,CAST(NULL AS STRING) as feel_free_to_tell_us_more
  ,t3.ab_testing
  ,t3.order_key
  ,t3.open_ended_response
  ,b.date_of_delivery
  ,b.final_order_amount
  ,b.delivered_qty / b.ordered_qty AS fulfil_perc
  ,b.order_ship_mode 
  ,b.order_ship_mode_level2 
  ,b.order_type
  ,b.store_code
  ,b.store_name
  ,t3.new_order_id as order_id
  ,b.order_ref_number
  ,b.source
  ,CAST(NULL AS STRING) AS user_agent
  ,CAST(NULL AS STRING) AS device
  ,CAST(NULL AS STRING) AS email_or_app
  ,CASE WHEN response <= 6 THEN 'detractors'
                  WHEN response >= 7 AND response <= 8 THEN 'neutral'
                  WHEN response > 8 THEN 'promoters'
              END AS nps_response_category
  , CAST(NULL AS INT64) AS is_substituted
  , CAST(NULL AS STRING) AS substitution_service_rating
  , CAST(NULL AS INT64) AS substitution_suggestion_selection
  , CAST(NULL AS INT64) AS substitution_suggestion_range_of_products
  , CAST(NULL AS INT64) AS substitution_suggestion_did_not_know
  , CAST(NULL AS INT64) AS substitution_suggestion_not_highlighted
  , CAST(NULL AS INT64) AS substitution_suggestion_bad_shape
  , CAST(NULL AS STRING) AS substitution_easy_to_use
  , CAST(NULL AS STRING) AS substitution_easy_to_understand
FROM 
(
    select *
    from 
    (
        select 
           *,row_number() over (partition by new_order_id order by end_date asc) as rk
        from 
        (
          select 
            *
            ,CASE WHEN response <= 6 THEN 'detractors'
                  WHEN response >= 7 AND response <= 8 THEN 'neutral'
                  WHEN response > 8 THEN 'promoters'
              END AS nps_response_category
            , case when order_id is null then respondent_id else order_id end as new_order_id
          from fpon_ods.nps_detail as a
          order by end_date desc

        ) as t
         order by end_date desc
    ) t2
    where rk=1
    order by end_date desc
) as t3 
join fpon_cdm.calendar as cal 
on date(t3.end_date) =cal.date
left join fpon_cdm.fpon_cdm_sales_head as b 
on cast(t3.order_id as string) = cast(b.order_ref_number as string)
--where order_ship_mode_level2 <> 'BULK'
WHERE order_status not in ('CANCELLED')
-- and nps_flg = 'Y'
order by end_date desc
),
b2b as (
select 
  year
  ,week
  ,cast(concat(cast(year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week  
  ,'Completed' AS status
  ,t3.respondent_id
  ,t3.collector_id
  ,DATETIME(t3.start_date) AS start_date
  ,DATETIME(t3.end_date) AS end_date
  ,t3.ip_address
  ,t3.response
  ,t3.mobile_app
  ,t3.website
  ,t3.product_price
  ,t3.product_assortment
  ,t3.product_quality
  ,t3.stock_availability
  ,t3.delivery_collection_experience
  ,t3.delivery_timeliness_collection_waiting_time
  ,t3.payment_options
  ,t3.customer_service
  ,t3.loyalty_program
  ,t3.others as comments
  ,CAST(NULL AS STRING) as feel_free_to_tell_us_more
  ,t3.ab_testing
  ,t3.order_key
  ,t3.open_ended_response
  ,b.date_of_delivery
  ,b.final_order_amount
  ,b.delivered_qty_with_normalised_weighted_items / b.ordered_qty_with_normalised_weighted_items AS fulfil_perc
  ,b.order_ship_mode 
  ,b.order_ship_mode_level2 
  ,b.order_type
  ,b.store_code
  ,b.store_name
  ,t3.new_order_id as order_id
  ,b.order_ref_number
  ,b.source
  ,CAST(NULL AS STRING) AS user_agent
  ,CAST(NULL AS STRING) AS device
  ,CAST(NULL AS STRING) AS email_or_app
  ,CASE WHEN response <= 6 THEN 'detractors'
                  WHEN response >= 7 AND response <= 8 THEN 'neutral'
                  WHEN response > 8 THEN 'promoters'
              END AS nps_response_category
  , CAST(NULL AS INT64) AS is_substituted
  , CAST(NULL AS STRING) AS substitution_service_rating
  , CAST(NULL AS INT64) AS substitution_suggestion_selection
  , CAST(NULL AS INT64) AS substitution_suggestion_range_of_products
  , CAST(NULL AS INT64) AS substitution_suggestion_did_not_know
  , CAST(NULL AS INT64) AS substitution_suggestion_not_highlighted
  , CAST(NULL AS INT64) AS substitution_suggestion_bad_shape
  , CAST(NULL AS STRING) AS substitution_easy_to_use
  , CAST(NULL AS STRING) AS substitution_easy_to_understand
FROM 
(
    select *
    from 
    (
        select 
           *,row_number() over (partition by new_order_id order by end_date asc) as rk
        from 
        (
          select 
            *
            ,CASE WHEN response <= 6 THEN 'detractors'
                  WHEN response >= 7 AND response <= 8 THEN 'neutral'
                  WHEN response > 8 THEN 'promoters'
              END AS nps_response_category
            , case when order_id is null then respondent_id else order_id end as new_order_id
          from fpon_ods.nps_detail as a
        ) as t
    ) t2
    where rk=1
) as t3 
join fpon_cdm.calendar as cal 
on date(t3.end_date) =cal.date
left join fpon_cdm.fpon_cdm_sales_head as b 
on cast(t3.order_key as string) = cast(b.external_order_id as string)
--where order_ship_mode_level2 <> 'BULK'
-- where nps_flg = 'Y'
and order_status not in ('CANCELLED')),
final_table as (
	select * from pfc_ffs
	union all
	select * from b2b
),
zs_null as (
	SELECT order_ref_number
	FROM final_table
	WHERE order_ship_mode_level2 IS NULL 
	AND source = 'ZopSmart'
),

tbl AS (select *, 
	   case 
      when order_type = 'OFFLINE' or order_ship_mode = 'OFFLINE' THEN 'OFFLINE'
	   	when order_type = 'B2B' then 'B2B' 
	   	when (order_type = 'DELIVERY' or order_type = 'PICKUP') and order_ship_mode_level2 = 'PFC_DELIVERY' THEN 'PFC'
	   	when order_ship_mode_level2 = 'FFS Normal' THEN 'FFS Normal'
	   	when order_ship_mode_level2 = 'FFS Darkstore' THEN 'FFS Darkstore'
		  when order_ship_mode = 'PFC_DELIVERY' AND order_ship_mode_level2 IS NULL THEN 'PFC'
		  when order_ship_mode = 'FFS' AND order_ship_mode_level2 IS NULL THEN 'FFS Normal'
      when order_ship_mode = 'BULK' AND order_ship_mode_level2 IS NULL THEN 'PFC'
    end as business_type,
    'survey monkey' AS survey_source
from final_table
WHERE order_ref_number NOT IN (SELECT order_ref_number FROM zs_null)
-- ORDER BY end_date DESC
UNION ALL
SELECT b.year, b.week, cast(concat(cast(year as string),case when week < 10 then '0' else '' end ,cast(week as string)) as int64) as year_week,status,
       null as respondent_id, null as collector_id, 
       start_date,
       end_date,
       null as ip_address,
       CAST(nps_answer_how_likely AS INT64) AS response,
       mobile_app,
       website, 
       product_price,
       product_assortment,
       product_quality,
       stock_availability,
       delivery_collection_experience,
       delivery_timeliness_collection_waiting_time,
       payment_options,
       customer_service,
       loyalty_program,
       others AS comments,
       feedback_text as feel_free_to_tell_us_more,
       null as ab_testing,
       a.order_id as order_key,
       delivery_type AS open_ended_response,
       date_of_delivery,
       final_order_amount,
       c.delivered_qty_with_normalised_weighted_items  / c.ordered_qty_with_normalised_weighted_items AS fulfil_perc,
       order_ship_mode,
       order_ship_mode_level2,
       order_type,
       store_code,
       store_name,
       c.order_id,
--        CASE WHEN external_order_id IS NOT NULL THEN a.order_id ELSE a.order_ref_number END AS order_ref_number,
       original_order_ref_number AS order_ref_number,
       c.source,
       user_agent,
        CASE
          WHEN LOWER(user_agent) LIKE '%android%' THEN 'mobile'
          WHEN LOWER(user_agent) LIKE '%iphone%' OR LOWER(user_agent) LIKE '%ipod%' OR LOWER(user_agent) LIKE '%ipad%' THEN 'mobile'
          WHEN LOWER(user_agent) LIKE '%macintosh%' THEN 'desktop'
          WHEN LOWER(user_agent) LIKE '%windows%' THEN 'desktop'
          WHEN LOWER(user_agent) LIKE '%linux%' THEN 'desktop'
          WHEN LOWER(user_agent) LIKE '%cros%' THEN 'desktop'
         END AS device,
       CASE WHEN response IS NULL AND is_substituted IS NULL THEN 'app' ELSE 'email' END AS email_or_app,
       CASE 
          WHEN CAST(nps_answer_how_likely AS INT64) <= 6 THEN 'detractors'
          WHEN CAST(nps_answer_how_likely AS INT64) >= 7 AND CAST (nps_answer_how_likely AS INT64)<= 8 THEN 'neutral'
          WHEN CAST(nps_answer_how_likely AS INT64) > 8 THEN 'promoters'
       END AS nps_response_category,
       CASE WHEN is_substituted = true THEN 1 WHEN is_substituted = false THEN 0 END AS is_substituted,
       substitution_service_rating,
       substitution_suggestion_selection,
       substitution_suggestion_range_of_products,
       substitution_suggestion_did_not_know,
       substitution_suggestion_not_highlighted,
       substitution_suggestion_bad_shape,
       substitution_easy_to_use,
       substitution_easy_to_understand,
       case 
          when order_type = 'B2B' then 'B2B' 
          when order_type = 'OFFLINE' or order_ship_mode = 'OFFLINE' THEN 'OFFLINE'
          when (order_type = 'DELIVERY' or order_type = 'PICKUP') and order_ship_mode_level2 = 'PFC_DELIVERY' THEN 'PFC'
          when order_ship_mode_level2 = 'FFS Normal' THEN 'FFS Normal'
          when order_ship_mode_level2 = 'FFS Darkstore' THEN 'FFS Darkstore'
          when order_ship_mode = 'PFC_DELIVERY' AND order_ship_mode_level2 IS NULL THEN 'PFC'
          when order_ship_mode = 'FFS' AND order_ship_mode_level2 IS NULL THEN 'FFS Normal'
          when order_ship_mode = 'BULK' AND order_ship_mode_level2 IS NULL THEN 'PFC'
        end as business_type,
       'surveygizmo' as survey_source
       
FROM (
  SELECT * 
      , REGEXP_EXTRACT(order_id,'[0-9]{4,}') AS order_ref_number
  FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY order_id ORDER BY end_date ASC) AS rnk
    FROM fpon_ods.nps_detail_survey_gizmo
    WHERE id >= 77
    # AND status = 'Complete'
    AND (order_id <> '123456' OR order_id <> '12345')
    ORDER BY id ASC
  )
  WHERE rnk = 1
) a 
JOIN fpon_cdm.calendar_full b
ON date(end_date) = b.date
LEFT JOIN (SELECT date_of_delivery
          , order_type
          , order_id
          , CASE WHEN order_type = 'B2B' THEN COALESCE(REGEXP_EXTRACT(external_order_id,'[0-9]{4,}'), order_ref_number) ELSE order_ref_number END AS order_ref_number
          , order_ref_number AS original_order_ref_number
          , external_order_id
          , final_order_amount
          , delivered_qty_with_normalised_weighted_items
          , ordered_qty_with_normalised_weighted_items
          , order_ship_mode
          , order_ship_mode_level2
          , store_code
          , store_name
          , source
      FROM fpon_cdm.fpon_cdm_sales_head 
      WHERE order_status <> 'CANCELLED') c 
ON a.order_ref_number = c.order_ref_number
)

SELECT *, CASE WHEN have_comments = 1 OR have_feel_free_to_tell_us_more = 1 THEN 1 ELSE 0 END AS have_remarks
FROM (
  SELECT *, 
         CASE WHEN REGEXP_CONTAINS(comments,'[a-zA-Z0-9]') = TRUE THEN 1 ELSE 0 END AS have_comments, 
         CASE WHEN REGEXP_CONTAINS(feel_free_to_tell_us_more,'[a-zA-Z0-9]') = TRUE THEN 1 ELSE 0 END AS have_feel_free_to_tell_us_more
  FROM tbl
)
)