select 
distinct
profile, feedback_category_new
, cast(round(percentile_cont(refund_amount, 0.85) over (partition by profile, feedback_category_new),2) as string) as refund_amt_85
, cast(round(percentile_cont(refund_perc, 0.85) over (partition by profile, feedback_category_new),2) as string) as refund_perc_85
, cast(round(percentile_cont(refund_amount, 0.9) over (partition by profile, feedback_category_new),2) as string) as refund_amt_90
, cast(round(percentile_cont(refund_perc, 0.9) over (partition by profile, feedback_category_new),2) as string) as refund_perc_90
, cast(round(percentile_cont(refund_amount, 0.95) over (partition by profile, feedback_category_new),2) as string) as refund_amt_95
, cast(round(percentile_cont(refund_perc, 0.95) over (partition by profile, feedback_category_new),2) as string) as refund_perc_95
from
(
select 
ticket_created_date
, ticket_id
, cast(refund_amount_new as numeric) as refund_amount
, order_ref_number
, final_order_amount
, feedback_category_new
, cast(refund_amount_new as numeric) * 100.00/final_order_amount as refund_perc
, case when profile is null then 'Others' else profile end as profile
from fpon_cdm.zendesk_full_table_snapshot as zd
left join fpon_analytics.customer_profile as cust on cust.customer_id = zd.customer_id
where 
--new_ticket_created_date >= '2019-10-01'
ticket_created_date >= DATE_SUB(CURRENT_DATE(), INTERVAL 180 DAY)
and feedback_category_new in ('order_issues__damaged_items','order_issues__missing_items')
and safe_cast(refund_amount_new as numeric) is not null
and refund_amount not in (0,0.00)
and final_order_amount is not null
and final_order_amount > 0
) 
group by profile, feedback_category_new, refund_amount, refund_perc
order by feedback_category_new,profile
;