INSERT INTO `fairprice-bigquery.fpon_analytics.pfc_drop_off_offset` (run_date, store_code, customer_id, total_orders, total_order_days, order_number)

WITH last_promo_code_expiry AS (
  SELECT customer_id, DATE_ADD(run_date, INTERVAL 4 DAY) AS last_edm_blast, DATE_SUB(DATE_ADD(DATE_ADD(run_date, INTERVAL 4 DAY), INTERVAL 1 MONTH), INTERVAL 1 DAY) AS last_promo_code_expiry
  FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY run_date DESC) as rnk
    FROM fpon_analytics.pfc_drop_off_offset
  )
  WHERE rnk = 1
  ORDER BY last_edm_blast ASC
),

pfc_drop_off_orders AS (
  SELECT a.customer_id, order_id, last_edm_blast, last_promo_code_expiry,order_placed_time, order_completion_date, store_code, ordered_qty, delivered_qty, order_ship_mode_level2, order_type
  FROM (
    SELECT order_ref_number AS order_id, order_placed_time, customer_id, order_completion_date, store_code, ordered_qty, delivered_qty, order_ship_mode_level2, order_type, 
    FROM fpon_cdm.fpon_cdm_sales_head
    WHERE delivered_qty_with_normalised_weighted_items < ordered_qty_with_normalised_weighted_items
    AND order_completion_date BETWEEN  DATE_ADD(CURRENT_DATE(), INTERVAL -4 WEEK) AND DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
    AND order_status = 'COMPLETED' 
    AND order_ref_number NOT IN (SELECT DISTINCT ZEN.order_reference_number
                                 FROM fpon_cdm.zendesk_tickets_weekly_snapshot  ZEN --fpon_cdm.zendesk_tickets_weekly ZEN Changed on 12/Jun/2020 after the original table is not updated anymore.
                                 WHERE ZEN.order_reference_number = order_ref_number 
                                 AND ZEN.feedback_category_new = 'changes_to_orders__add/remove_items' --'FairPrice On::Changes to Orders::Add/Remove Items' old value
                                 AND ticket_created_date  <= order_completion_date)
    AND CAST(zs_store_id AS INT64) = 165
    AND order_type <> 'B2B'
--     AND customer_id NOT IN (SELECT DISTINCT customer_id 
--                             FROM fpon_analytics.pfc_drop_off_offset Ofst
--                             WHERE DATE_DIFF(current_date(), DATE_ADD(Ofst.run_date, INTERVAL 4 DAY), DAY) / 30 < 1
--                             AND Ofst.customer_id = customer_id) 
    ) a
  LEFT JOIN last_promo_code_expiry b
  ON CAST(a.customer_id AS INT64) = b.customer_id
),

final_table AS (
  SELECT *
  FROM pfc_drop_off_orders
  WHERE last_promo_code_expiry < DATE(order_placed_time) 
  OR last_promo_code_expiry IS NULL
  ORDER BY last_promo_code_expiry DESC
)


SELECT *
FROM (
  SELECT current_date() run_date, 
         CAST(STRING_AGG(DISTINCT store_code, ';') AS INT64) AS store_code,
         CAST(a.customer_id AS INT64) AS customer_id,
         COUNT(order_id) AS total_orders,
         COUNT(DISTINCT EXTRACT(DATE FROM order_placed_time)) AS total_order_days,
         STRING_AGG(order_id,';') AS order_number 
  FROM final_table a
  GROUP BY customer_id
  HAVING COUNT(order_id) > 1
)